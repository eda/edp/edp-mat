classdef StabilityFrequencySweepAnalysis < Analysis
  %STABILITYFREQUENCYSWEEPANALYSIS Reference of an Stability-Analysis
  
  methods (Access = private)
    function this = StabilityFrequencySweepAnalysis(javaObj)
      this.javaObj = javaObj;
    end
  end

  methods (Access = public)

    function  net = getGround(this)
      % GETGROUND

      javaRetval = javaMethod('getGround', this.javaObj);

      if  isjava(javaRetval) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.simulation.datastruct.Net', javaRetval)
        net = Net.init(javaRetval);
      else
        net = [];
      end
    end

    function  tf = setGround(this, net)
      % SETGROUND

      if isa(net, 'Net')

        javaRetval = javaMethod('setGround', this.javaObj, net.javaObj);

        if  isjava(javaRetval) && ...
            javaMethod('isInstanceOf',...
              'edlab.eda.edp.simulation.datastruct.Net', javaRetval)
          tf = true;
        else
          tf = false;
        end
      else
        error('Provided parameter is not a net');
      end
    end

    function  inst = getProbe(this)
      % GETPROBE

      javaRetval = javaMethod('getProbe', this.javaObj);

      if  isjava(javaRetval) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.simulation.datastruct.Instance', javaRetval)
        inst = Instance.init(javaRetval);
      else
        inst = [];
      end
    end

    function  tf = setProbe(this, probe)
      % SETPROBE

      if isa(probe, 'Instance')

        javaRetval = javaMethod('setProbe', this.javaObj, probe.javaObj);

        if  isjava(javaRetval) && ...
            javaMethod('isInstanceOf',...
              'edlab.eda.edp.simulation.datastruct.Instance', javaRetval)
          tf = true;
        else
          tf = false;
        end
      else
        error('Provided parameter is not an instance');
      end
    end

    function values = getValues(this)
      % GETVALUES

      javaArray = javaMethod('getValues', this.javaObj);

      values = zeros(1, numel(javaArray));

      for i=1:numel(javaArray)
        values(i) = javaMethod('doubleValue', javaArray(i));
      end
    end

    function tf = setValues(this, values)
      % SETVALUES

      if isreal(values)

        javaValuesArray = javaArray('java.math.BigDecimal', numel(values));

        for i=1:numel(javaValuesArray)
          javaValuesArray(i) = javaObject('java.math.BigDecimal', values(i));
        end

        tf = javaMethod('setValues', this.javaObj, javaValuesArray);
      else
        error('No array of values is provided');
      end
    end
  end

  methods (Static)
    function obj = init(javaObj)
      %INIT Do not call this function by yourself.
      if  isjava(javaObj) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.simulation.analysis.StabilityFrequencySweepAnalysis', javaObj)
        obj = StabilityFrequencySweepAnalysis(javaObj);
      else
        obj=[];
      end
    end
  end
end