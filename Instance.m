classdef Instance < ExpertDesignPlanWrapper
  % INSTANCE Reference of an instance in a Test in the Expert Design Plan

  % Copyright 2023 Reutlingen University, Electronics & Drives

  methods (Access = private)
    function this = Instance(javaObj)
      this.javaObj = javaObj;
    end
  end

  methods (Access = public)

    function instance = getDatabaseInstance(this)
      % GETDATABSEINSTANCE  Get the associated database instance

      instance = [];

      instanceJavaObj = javaMethod('getDatabaseInstance',this.javaObj);

      if ~isempty(instanceJavaObj)
        instance = DatabaseInstance.init(instanceJavaObj);
      end
    end

    function net = getNet(this, name)
      % GETNET Get the reference to a net in the toplevel testbench.
      %  net = test.GETNET(name) returns the reference to
      %  a net if available, [] otherwise.

      net = [];

      if ischar(name)

        javaNet = javaMethod('getNet', this.javaObj, ...
          javaObject('java.lang.String',name));

        if ~isempty(javaNet)
          net = Net.init(javaNet);
        end

      else
         error('Provided parameter name is not a character array');
      end
    end

    function name = getName(this)
      % GETNAME  Get the name of the instance
      name = handleJavaString(javaMethod('getInstanceName',this.javaObj));
    end

    function instance = getParent(this)
      % GETNAME  Get parent instance

      instance = [];

      instanceJavaObj = javaMethod('getParent',this.javaObj);

      if ~isempty(instanceJavaObj)
        instance = Instance.init(instanceJavaObj);
      end
    end

    function test = getTest(this)
      % GETTEST  Get the test where the instance is defined

      javaTestObj = javaMethod('getTest',this.javaObj);
      test = Test.init(javaTestObj);
    end

    function property = getTechProperty(this, name)
      % GETTECHPROPERTY Get a technology property from the technology database
      % property = instance.GETPROPERTY(name) returns the
      % value of a property if available, [] otherwise.

      property = [];

      if ischar(name)

        javaBigDecimal = javaMethod('getTechProperty', this.javaObj, ...
          javaObject('java.lang.String', name));

        if ~isempty(javaBigDecimal)
          property = javaMethod('doubleValue', javaBigDecimal);
        end

      else
         error('Provided parameter ''name'' is not a character array');
      end
    end

    function property = getProperty(this, name)
      % GETPROPERTY Get a property of the instance
      %  property = instance.GETPROPERTY(name) returns the
      %  value of a property if available, [] otherwise.

      property = [];

      if ischar(name)

        javaBigDecimal = javaMethod('getProperty', this.javaObj, ...
          javaObject('java.lang.String', name));

        if ~isempty(javaBigDecimal)
          property = javaMethod('doubleValue', javaBigDecimal);
        end

      else
         error('Provided parameter ''name'' is not a character array');
      end
    end

    function property = setProperty(this, name, value)
      % SETPROPERTY Set a property of the instance
      %  property = instance.SETPROPERTY(name, value) will set the property 
      %  with the identifier 'name' to the value 'value'
      %   
      %  Example:
      %   property = inst.SETPROPERTY('gmid', 10.0) 
      %

      property = [];

      if ischar(name)

        if isreal(value) && numel(value) ==1

          javaBigDecimal = javaMethod('setProperty', this.javaObj, ...
            javaObject('java.lang.String', name), ...
            javaObject('java.math.BigDecimal', value));

          if ~isempty(javaBigDecimal)
            property = javaMethod('doubleValue', javaBigDecimal);
          end
        else
           error('Provided parameter ''value'' is not a real value');
        end
      else
         error('Provided parameter ''name'' is not a character array');
      end
    end

    function instanceNames = getInstanceNames(this)
      %GETINSTANCENAMES Get a cell array of all instance names that are defined
      %   within the instance. 
      %   instanceNames = test.GETINSTANCENAMES() returns a cell array of all
      %   instance names.


      javaArray = javaMethod('getInstanceNames', this.javaObj);

      instanceNames = cell(1, numel(javaArray));

      for i=1:numel(javaArray)
        instanceNames{i} = handleJavaString(javaArray(i));
      end
    end

    function instance = getInstance(this, name)
      %GETINSTANCE Get the reference to an instance that is defined
      %   within the instance. 
      %   instance = test.GETINSTANCE(name) returns the reference to
      %   an instance if available, [] otherwise.

      instance = [];

      if ischar(name)

        javaInstance = javaMethod('getInstance', this.javaObj, ...
          javaObject('java.lang.String', name));

        if ~isempty(javaInstance)
          instance = Instance.init(javaInstance);
        end

      else
         error('Provided parameter name is not a character array');
      end
    end

    function terminalNames = getTerminalNames(this)
      %GETTERMINALNAMES Get a cell array of all terminal names of the instance
      %   terminalNames = instance.GETTERMINALNAMES() returns a cell array of all
      %   terminalNames.

      javaArray = javaMethod('getTerminalNames', this.javaObj);

      terminalNames = cell(1, numel(javaArray));

      for i=1:numel(javaArray)
        terminalNames{i} = handleJavaString(javaArray(i));
      end
    end

    function terminal = getTerminal(this, name)
      %GETTERMINAL Get the reference to a terminal of the instance
      %   terminal = instance.GETTERMINAL(name) returns the reference to
      %   an a terminal if available, [] otherwise.

      terminal = [];

      if ischar(name)

        javaInstance = javaMethod('getTerminal', this.javaObj, ...
          javaObject('java.lang.String', name));

        if ~isempty(javaInstance)
          terminal = Terminal.init(javaInstance);
        end

      else
         error('Provided parameter name is not a character array');
      end
    end


    function operatingPointNames = getOperatingPointNames(this)
      % GETOPERATINGPOINTNAMES Get a cell array of all operating points names
      %  operatingPointNames = instance.GETOPERATINGPOINTNAMES() returns a cell
      %  array of all operating point names.

      javaArray = javaMethod('getOperatingPointNames', this.javaObj);

      operatingPointNames = cell(1, numel(javaArrray));

      for i=1:numel(javaArray)
        operatingPointNames{i} = handleJavaString(javaArray(i));
      end
    end

    function operatingPoint = getOperatingPoint(this, name)
      % GETOPERATINGPOINT Get the reference to a operating point of the instance
      %  operatingPoint = instance.GETOPERATINGPOINT(name) returns the
      %  reference to an operating point if available, [] otherwise.

      operatingPoint = [];

      if ischar(name)

        javaInstance = javaMethod('getOperatingPoint', this.javaObj, ...
          javaObject('java.lang.String', name));

        if ~isempty(javaInstance)
          operatingPoint = OperatingPoint.init(javaInstance);
        end

      else
         error('Provided parameter name is not a character array');
      end
    end

    function parameter = getParameter(this, name)
      %GETPARAMETER Get the reference to a parameter of the instance
      %   parameter = instance.GETPARAMETER(name) returns the
      %   reference to a parameter if available, [] otherwise.

      parameter = [];

      if ischar(name)

        javaInstance = javaMethod('getParameter', this.javaObj, ...
          javaObject('java.lang.String', name));

        if ~isempty(javaInstance)
          parameter = DatabaseParameter.init(javaInstance);
        end

      else
         error('Provided parameter name is not a character array');
      end
    end

    function tf = addSizingFunction(this, name, fun)
      % ADDSIZINGFUNCTION
      %  tf = inst.ADDSIZINGFUNCTION(name, fun)

      javaInstance = javaMethod('getDatabaseInstance', this.javaObj);

      instance = DatabaseInstance.init(javaInstance);

      tf = instance.addSizingFunction(name, fun);
    end

    function tf = removeSizingFunction(this, name)
      % REMOVESIZINGFUNCTION Removes a sizing function with a given name
      %  tf = inst.REMOVESIZINGFUNCTION(name) 

      javaInstance = javaMethod('getDatabaseInstance', this.javaObj);

      instance = DatabaseInstance.init(javaInstance);

      tf = instance.removeSizingFunction(name, fun);
    end

    function tf = size(this, name, varargin)
      % SIZE Call the sizing function of an instance
      %  tf = inst.SIZE(name, varargin)

      javaInstance = javaMethod('getDatabaseInstance', this.javaObj);

      instance = DatabaseInstance.init(javaInstance);

      tf = instance.size(name, varargin);
    end

    function retval = get(this, name)
      % GET  Get the value of a parameter

      retval = [];

      if ischar(name)
        javaValue = javaMethod('get', this.javaObj, ...
          javaObject('java.lang.String', name));
        if isa(javaValue, 'java.math.BigDecimal')
          retval = javaMethod('doubleValue', javaValue);
        elseif isa(javaValue, 'java.lang.String')
          retval = handleJavaString(javaValue);
        elseif isempty(javaValue)
          retval = [];
        end
      else
        error('Provided parameter name is not a character array');
      end
    end


    function retval = set(this, name, value)
      % SET  Set the value of a parameter
      %   value = instance.SET(name, value) returns the
      %   new value of the parameter
      retval = [];

      javaValue = [];


      if ischar(name)
        if numel(value)==1 && isreal(value)

          javaValue = javaMethod('set', ...
                                  this.javaObj, ...
                                  javaObject('java.lang.String', name), ...
                                  javaObject('java.math.BigDecimal', value));

        elseif ischar(value)
          javaValue = javaMethod('set', ...
                                 this.javaObj, ...
                                 javaObject('java.lang.String', name), ...
                                 javaObject('java.lang.String', value));
        elseif isempty(value)
          javaValue = javaMethod('set', ...
                                 this.javaObj, ...
                                 javaObject('java.lang.String', name), ...
                                 []);
        end

        if isa(javaValue, 'java.math.BigDecimal')
          retval = javaMethod('doubleValue', javaValue);
        elseif isa(javaValue, 'java.lang.String')
          retval = handleJavaString(javaValue);
        end

      else
        error('Provided parameter name is not a character array');
      end
    end

    function tf = canSet(this, name, value)
      % CANSET  Identify if a parameter can be set to a value

      if ischar(name)

        if numel(value)==1 && isreal(value)

          tf = javaMethod('canSet', ...
                this.javaObj, ...
                javaObject('java.lang.String', name), ...
                javaObject('java.math.BigDecimal', value));

        elseif ischar(value)
          tf = javaMethod('canSet', ...
                 this.javaObj, ...
                javaObject('java.lang.String', name), ...
                javaObject('java.lang.String', value));

        elseif isempty(value)
          tf = javaMethod('canSet', ...
                this.javaObj, ...
                javaObject('java.lang.String', name), ...
                []);
        end

      else
        error('Provided parameter ''name'' is not a character array');
      end
    end

    function tf = pullParameters(this, inst)
      % PULLPARAMETERS Pull all parameters another instance.
      %
      %  Example: 
      %  >> tf = inst1.PULLPARAMETERS(inst2)
      %
      %  tf =
      %
      %      true

      if isa(inst, 'DatabaseInstance') || isa(inst, 'Instance') 
        tf = javaMethod('pullParameters', this.javaObj, inst.javaObj);
      else
        error('Provided parameter is not an instance.')
      end
    end

    function tf = pushParameters(this, inst)
      % PUSHPARAMETERS Push all parameters to another instance.
      %
      %  Example: 
      %  >> tf = inst2.PUSHPARAMETERS(inst1)
      %
      %  tf =
      %
      %      true

      if isa(inst, 'DatabaseInstance') || isa(inst, 'Instance') 
        tf = javaMethod('pushParameters', this.javaObj, inst.javaObj);
      else
        error('Provided parameter is not an instance.')
      end
    end
    
    function tf = show(this)
      % SHOW Show the instance in Virtuoso
      tf = javaMethod('show', this.javaObj);
    end

    function tf = isTopInst(this)
      % ISTOPINST Identify if this is the top instance
      tf = javaMethod('isTopInst', this.javaObj);
    end

    function address = getAddress(this)
      % GETADDRESS  Get address to terminal
      javaArray = javaMethod('getAddress',this.javaObj);
    
      address = cell(1, numel(javaArray));

      for i=1:numel(javaArray)
        address{i} = handleJavaString(javaArray(i));
      end
    end
  end

  methods (Static)
    function obj = init(javaObj)
      %INIT Do not call this function by yourself.
      if  isjava(javaObj) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.simulation.datastruct.Instance', javaObj)
        obj = Instance(javaObj);
      else
        obj=[];
      end
    end
  end
end