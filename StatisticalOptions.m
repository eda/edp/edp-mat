classdef StatisticalOptions < ExpertDesignPlanWrapper
  % STATISTICALOPTIONS This class handles the statistical options for a
  %   Cadence Spectre simulation

  methods (Access = private)
    function this = StatisticalOptions(javaObj)
      this.javaObj = javaObj;
    end
  end

  methods (Access = public)

    function retval = getSampling(this)
      % GETSAMPLING Get the 'sampling' option
      %   The sampling option is either:
      %    - standard
      %    - lhs
      %    - orthogonal
      %    - lds
      %    - []
      %
      % See also StatisticalOptions.setSampling

      retval = [];

      javaValue = javaMethod('getSampling', this.javaObj);

      if isa(javaValue, 'java.lang.String')
        retval = handleJavaString(javaValue);
      end
    end

    function retval = setSampling(this, sampling)
      % SETSAMPLING  Set the 'sampling' option
      % See also StatisticalOptions.getSampling

      retval = [];

      if ischar(sampling)
        
        javaValue = javaMethod('setSampling', ...
          this.javaObj, javaObject('java.lang.String', sampling));

      elseif isempty(sampling)

        javaValue = javaMethod('setSampling', this.javaObj, []);
        
      else
        error('Provided ''sampling'' is not a character array');
      end

      if isa(javaValue, 'java.lang.String')
        retval = handleJavaString(javaValue);
      end
    end

    function retval = getVariation(this)
      % GETVARIATION Get the 'variation' option
      %   The variation option is either:
      %    - all
      %    - process
      %    - mismatch
      %    - []
      %
      % See also StatisticalOptions.setVariation

      retval = [];

      javaValue = javaMethod('getVariation', this.javaObj);

      if isa(javaValue, 'java.lang.String')
        retval = handleJavaString(javaValue);
      end
    end

    function retval = setVariation(this, variation)
      % SETVARIATION Set the 'variation' option
      % See also StatisticalOptions.getVariation

      retval = [];

      if ischar(variation)
        
        javaValue = javaMethod('setVariation', ...
          this.javaObj, javaObject('java.lang.String', variation));

      elseif isempty(variation)

        javaValue = javaMethod('setVariation', this.javaObj, []);
        
      else
        error('Provided ''variation'' is not a character array');
      end

      if isa(javaValue, 'java.lang.String')
        retval = handleJavaString(javaValue);
      end
    end

    function retval = getSeed(this)
      % GETSEED Get the seed
      %  analysis.GETSEED() returns the seed
      % See also StatisticalOptions.setSeed.

      javaValue = javaMethod('getSeed', this.javaObj);
      retval = javaValue;
    end

    function retval = setSeed(this, value)
      % SETSEED Specify the seed
      %  analysis.SETSEED(value) specifies the seed
      % See also StatisticalOptions.getSeed.

      if numel(value)==1 && isinteger(int8(value)) && int8(value)>0

        javaValue = javaMethod('setSeed', ...
          this.javaObj, int8(value));

      elseif isempty(value)
        javaValue = javaMethod('setSeed', this.javaObj, []);
      else
        error('No new value is provided');
      end

      retval = javaValue;
    end


    function retval = getNumprocesses(this)
      % GETNUMPROCESSES Get the number of parallel simulations
      %  analysis.GETNUMPROCESSES() returns the seed
      % See also StatisticalOptions.setNumprocesses.

      javaValue = javaMethod('getNumprocesses', this.javaObj);
      retval = javaValue;
    end

    function retval = setNumprocesses(this, value)
      % SETNUMPROCESSES Specify the seed
      %  analysis.SETNUMPROCESSES(value) specifies the number of parallel
      %  runs
      % See also StatisticalOptions.getNumprocesses.

      if numel(value)==1 && isinteger(int8(value)) && int8(value) >0

        javaValue = javaMethod('setNumprocesses', ...
          this.javaObj, int8(value));
      else
        error('No new value is provided');
      end

      retval = javaValue;
    end

    function retval = getNumruns(this)
      % GETNUMRUNS Get the number of runs
      %  analysis.GETNUMRUNS() returns the number of runs
      % See also StatisticalOptions.setNumruns.

      javaValue = javaMethod('getNumruns', this.javaObj);
      retval = javaValue;
    end

    function retval = setNumruns(this, value)
      % SETNUMRUNS Specify the numer of runs
      %  analysis.SETNUMRUNS(value) specifies the numer of runs
      % See also StatisticalOptions.getNumruns.

      if numel(value)==1 && isinteger(int8(value)) && int8(value) >0

        javaValue = javaMethod('setNumruns', ...
          this.javaObj, int8(value));

      elseif isempty(value)
        javaValue = javaMethod('setNumruns', this.javaObj, []);
      else
        error('No new value is provided');
      end

      retval = javaValue;
    end
  end

  methods (Static)
    function obj = init(javaObj)
      %INIT Do not call this function by yourself.
      if  isjava(javaObj) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.simulation.test.StatisticalOptions', javaObj)
        obj = StatisticalOptions(javaObj);
      else
        obj=[];
      end
    end
  end
end