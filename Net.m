classdef Net < ExpertDesignPlanWrapper
  %NET Reference of a Net in a Test in the Expert Design Plan

  % Copyright 2023 Reutlingen University, Electronics & Drives

  methods (Access = private)
    function this = Net(javaObj)
      this.javaObj = javaObj;
    end
  end

  methods (Access = public)

    function name = getName(this)
      % GETNAME  Get the name of a net
      name = handleJavaString(javaMethod('getName', this.javaObj));
    end

    function test = getTest(this)
      % GETTEST  Get the test where the net is defined

      javaTestObj = javaMethod('getTest',this.javaObj);
      test = Test.init(javaTestObj);
    end

    function instance = getInstance(this)
      % GETINSTANCE  Get the instance that is associated with the net

      javaInstanceObj = javaMethod('getInstance',this.javaObj);
      instance = Instance.init(javaInstanceObj);
    end

    function identifier = getIdentifier(this)
      % GETIDENTIFIER  Get an identifier of the net
      identifier = handleJavaString(...
        javaMethod('getIdentifier',this.javaObj));
    end

    function tf = isGlobal(this)
      % ISGLOBAL  Identify if the net is global
      tf = javaMethod('isGlobal',this.javaObj);
    end

    function address = getAddress(this)
      % GETADDRESS  Get address to net
      javaArray = javaMethod('getAddress',this.javaObj);
    
      address = cell(1, numel(javaArray));

      for i=1:numel(javaArray)
        address{i} = handleJavaString(javaArray(i));
      end
    end
  end

  methods (Static)
    function obj = init(javaObj)
      %INIT Do not call this function by yourself.
      if  isjava(javaObj) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.simulation.datastruct.Net', javaObj)
        obj = Net(javaObj);
      else
        obj=[];
      end
    end
  end
end