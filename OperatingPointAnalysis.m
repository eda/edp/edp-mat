classdef OperatingPointAnalysis < Analysis
  %OPERATINGPOINTANALYSIS Reference of an Operating Point Analysis
  
  methods (Access = private)
    function this = OperatingPointAnalysis(javaObj)
      this.javaObj = javaObj;
    end
  end

  methods (Access = public)
    function maxiters = getMaxiters(this)
      % GETMAXITERS Get the maximum number of iterations
      %  analysis.GETMAXITERS() returns the maximum number of iterations
      %  during Newton-Raphson algorithm.
      %
      % See also OperatingPointAnalysis.setMaxiters

      maxiters = javaMethod('getMaxiters', this.javaObj);
    end

    function maxiters = setMaxiters(this, maxiters)
      % SETMAXITERS Set the maximum number of iterations
      %  analysis.SETMAXITERS(maxiters) sets the maximum number of iterations
      %  during Newton-Raphson algorithm and returns the current maxiters.
      %  maxiters is a positive integer.
      %
      % See also OperatingPointAnalysis.getMaxiters

      if isinteger(maxiters) && numel(maxiters) ==1
        maxiters = javaMethod('setMaxiters', this.javaObj, maxiters);
      else
        error('Provided parameter ''maxiters'' is not an integer');
      end
    end   

    function maxsteps = getMaxsteps(this)
      % GETMAXSTEPS Get the maximum number of steps
      %  analysis.GETMAXSTEPS() returns the maximum number of steps used in 
      %  the homotopy method.
      %
      % See also OperatingPointAnalysis.setMaxsteps

      maxsteps = javaMethod('getMaxsteps', this.javaObj);
    end

    function maxsteps = setMaxsteps(this, maxsteps)
      % SETMAXSTEPS Set the maximum number of steps
      %  analysis.SETMAXSTEPS(maxsteps) sets the maximum number if steps used in 
      %  the homotopy method and returns the current number of steps.
      %  maxsteps is a positive integer.
      %
      % See also OperatingPointAnalysis.getMaxsteps 

      if isinteger(maxsteps) && numel(maxsteps) == 1
        maxsteps = javaMethod('setMaxsteps', this.javaObj, maxsteps);
      else
        error('Provided parameter ''maxsteps'' is not an integer');
      end
    end   

    function homotopy = getHomotopy(this)
      % GETHOMOTOPY Get the homotopy method
      %  analysis.GETHOMOTOPY() returns the homotopy method.
      %  The homotopy is either
      %   - NONE
      %   - GMIN
      %   - SOURCE
      %   - DPTRAN
      %   - PTRAN
      %   - ARCLENGTH
      %   - TRANRAMPUP
      %   - ALL
      %
      % See also OperatingPointAnalysis.setHomotopy


      homotopy = handleJavaString(javaMethod('getHomotopy', this.javaObj));
    end

    function homotopy = setHomotopy(this, homotopy)
      % SETHOMOTOPY Set the homotopy method
      %  analysis.SETHOMOTOPY(homotopy) sets the homotopy method
      %  and returns the current active homotopy.
      %
      % See also OperatingPointAnalysis.getHomotopy

      if ischar(homotopy)
        javaHomotopy = javaMethod('setHomotopy', this.javaObj, ...
          javaObject('java.lang.String', homotopy));

        homotopy = handleJavaString(javaHomotopy);
      else
        error('Provided parameter ''homotopy'' is not a character array');
      end
    end
  end

  methods (Static)
    function obj = init(javaObj)
      %INIT Do not call this function by yourself.
      if  isjava(javaObj) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.simulation.analysis.OperatingPointAnalysis', javaObj)
        obj = OperatingPointAnalysis(javaObj);
      else
        obj=[];
      end
    end
  end
end