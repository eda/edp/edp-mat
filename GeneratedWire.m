classdef GeneratedWire < ExpertDesignPlanWrapper
  % GENERATEDWIRE Reference to a wire that in generated in a SchematicGenerator
  %
  % See also SchematicGenerator.

  % Copyright 2023 Reutlingen University, Electronics & Drives

  methods (Access = private)
    function this = GeneratedWire(javaObj)
      this.javaObj = javaObj;
    end
  end

  methods (Access = public)

    function point = last(this)
      % LAST Get the position of the last point of the wire
      %  obj.LAST() returns the position of the last point of the wire
      %
      %  Example: obj.last()      => [1.0 2.0]
      %
      javaPoint = javaMethod('last', this.javaObj);

      point = [ javaMethod('doubleValue', javaMethod('getX', javaPoint)), ...
                javaMethod('doubleValue', javaMethod('getY', javaPoint))];
    end

    function points = getPoints(this)
      % GETPOINTS Get a cell array of all points of the wire
      %  obj.GETPOINTS() returns an array of all points
      %
      %  Examples: obj.getPoints() => {[0.0 0.0] [1.0 0.0]}
      %
      javaPoints = javaMethod('getPoints', this.javaObj);

      points = cell(1, numel(javaPoints));

      for i = 1:numel(javaPoints)
        point = [ ...
          javaMethod('doubleValue', javaMethod('getX', javaPoints(i))), ...
          javaMethod('doubleValue', javaMethod('getY', javaPoints(i)))];
        points{i} = point;
      end
    end

    function inst = inst(this, master, anchor, varargin)
      % INST Place an instance at the end point of the wire.
      % obj.INST(master) places a symbol in the schematic for the provided
      % master.
      %   
      %   Keyword Parameters: 
      %    - name        : Name of the instance, e.g. M0, R1, etc.
      %    - anchor      : Anchor of the symbol where the symbol is placed.
      %                    Must correspond to an anchor of the symbol
      %   - rotation    : Rotation of the symbol, must be either integer
      %                   0, 1, 2, 3. The number indicates how much by 90
      %                   degrees the instance is rotated in positive direction
      %                   (counter-clockwise).
      %   - mirrorX    : Reflection about x axis, logical
      %   - mirrorY    : Reflection about y axis, logical   
      %
      %   Example: 
      %     nmos = generator.getSymbolMaster('PRIMLIB','nmos','symbol')
      %     inst = wire.inst(nmos,'anchor','D');
      %
      % See also GeneratedInstance.

      if isa(master, 'SymbolMaster')

        expectedAnchors = master.getTerminals();

        p = inputParser;

        addRequired(p,'master', @(x) isa(x, 'SymbolMaster'));
        addRequired(p,'anchor',@(x) any(validatestring(x, expectedAnchors)));
        addParameter(p,'name',[] , @ischar);

        addParameter(p,'rotation', 0, ...
                  @(x) isinteger(int8(x)) && numel(x)==1 && x>=0 && x<4);
        addParameter(p,'mirrorX', false, @(x) islogical(x) && numel(x)==1);
        addParameter(p,'mirrorY', false, @(x) islogical(x) && numel(x)==1);

        parse(p, master, anchor, varargin{:});

        if ischar(p.Results.name)
          instJavaObj = javaMethod('addInst', ...
                    this.javaObj, ...
                    master.javaObj, ...
                    javaObject('java.lang.String', p.Results.name), ...
                    int8(p.Results.rotation), ...
                    p.Results.mirrorX, ...
                    p.Results.mirrorY, ...
                    javaObject('java.lang.String', p.Results.anchor));
        else
          instJavaObj = javaMethod('addInst', ...
                    this.javaObj, ...
                    master.javaObj, ...
                    [], ...
                    int8(p.Results.rotation), ...
                    p.Results.mirrorX, ...
                    p.Results.mirrorY, ...
                    javaObject('java.lang.String', p.Results.anchor));
        end

        inst = GeneratedInstance.init(instJavaObj);
        
      else
        error('No valid master is provided');
      end
    end

    function pin = pin(this, name, varargin)
      % PIN Draw a pin at the end point of  a wire 
      % wire.pin(name,varargin) draws a pin at the end point of a wire
      % with the provided name. 
      %
      %  Keyword Parameters: 
      %   - name        : Name of the pin, e.g. INP, GND, etc.
      %   - direction   : Direction of the pin, must be either
      %                   'inputOutput', 'input' or  'output'.
      %                   'inputOutput' is used as default.
      %   - rotation    : Rotation of the symbol, must be either integer
      %                   0, 1, 2, 3. The number indicates how much by 90
      %                   degrees the instance is rotated in positive direction
      %                   (counter-clockwise).
      %   - mirrorX    : Reflection about x axis, logical
      %   - mirrorY    : Reflection about y axis, logical
      %
      %   Examples: 
      %    - Draw an 'inputOutput' with name 'A'
      %      pin = w1.pin('A');
      %    - Draw an 'input' with name B' and rotation by 90 degrees
      %      pin = w1.pin('B', 'rotation', 1, 'direction', 'input');
      %
      % See also GeneratedPin.

      expectedDirections = {'inputOutput', 'input', 'output'};

      p = inputParser;

      addRequired(p,'name', @ischar);
      addParameter(p,'direction',expectedDirections{1} ,...
                     @(x) any(validatestring(x,expectedDirections)));
      addParameter(p,'rotation', 0, ...
                @(x) isinteger(int8(x)) && numel(x)==1 && x>=0 && x<4);
      addParameter(p,'mirrorX', false, @(x) islogical(x) && numel(x)==1);
      addParameter(p,'mirrorY', false, @(x) islogical(x) && numel(x)==1);

      parse(p, name, varargin{:});

      pinJavaObj = javaMethod('addPin', ...
              this.javaObj, ...
              javaObject('java.lang.String', p.Results.name), ...
              int8(p.Results.rotation), ...
              p.Results.mirrorX, ...
              p.Results.mirrorY, ...    
              javaObject('java.lang.String', p.Results.direction));

      pin = GeneratedPin.init(pinJavaObj);
    end

    function wire = wire(this, point, varargin)
      % WIRE Draw a wire starting from the end point of a wire.
      % obj.wire(point,varargin) draws a wire starting from the end point of a
      % wire  to the provided point. The point can be specified either as
      % absolute or incremental value.
      %
      % Optional Keyword Parameters: 
      %    - name  : label as character array
      %    - inc   : can be either 
      %                   false   : utilize absolute coordinate system
      %                             (point is interpreted as absolute value)
      %                   true    : utilize incremental coordinate system
      %                            (point is interpreted as increment 
      %                             wrt. to the anchor)
      %    - style : can be either 
      %                   '-',    : no solder dots at the end
      %                   '-*'    : solder dot at the end
      %    - route : can be either 
      %                   '--',   : direct route between anchor and point
      %                   '-|'    : draw a perpendicular route from the anchor
      %                             to the point, starting with a horizontal
      %                             wire, followed by a vertical wire
      %                   '|-'    : draw a perpendicular route from the anchor
      %                             to the point, starting with a vertical
      %                             wire, followed by a horizontal wire
      %                   '-'     : draws a horizontal route starting at the 
      %                             anchor to the intersection of a horizontal
      %                             line crossing the anchor and a vertical line
      %                             crossing the point
      %                   '|'     : draws a vertical route starting at the 
      %                             anchor to the intersection of a vertical
      %                             line crossing the anchor and a vertical line
      %                             crossing the point
      %    - ratio : (0 1.0] draw the route only to a certain percentage
      %                      i.e. 0.5 means that only half of the route between
      %                      anchor and point is drawn
      % Examples:
      %  - Draw a wire incrementally by 1.0 in X direction
      %     w1.wire([1 0]);
      %  - Draw a wire incrementally by 0.5 in Y direction
      %     w1.wire([0 0.5]);
      %  - Draw a wire incrementally by 0.5 in Y direction and add the label 'X'
      %     w1.wire([0 0.5],'name','X');
      %  - Draw a wire to the absolute position [1.0 1.0]
      %     w1.wire([1.0 1.0], 'inc', false);
      %  - Draw a wire by 50% to the absolute position [1.0 1.0]
      %     w1.wire([1.0 1.0], 'inc', false, 'ratio', 0.5);
      %  - Draw a wire incrementally by 1.0 in X direction first and 0.5
      %     in Y direction afterwards
      %     w1.wire([1.0 0.5], 'route', '-|');      
      %  - Draw a wire incrementally by 0.5 in Y direction first and 1.0
      %     in X direction afterwards
      %     w1.wire([1.0 0.5], 'route', '|-');   
      %
      % See also GeneratedWire.
      p = inputParser;

      expectedStyles = {'-', '*-', '-*', '*-*'};
      expectedRoutes = {'--', '-', '|','-|', '|-'};

      addRequired(p,'point', @(x) numel(x)==2 && isreal(x(1)) && isreal(x(2)));

      addParameter(p,'ratio',1.0 ,...
                     @(x) isreal(x) && x>0 && x<=1.0);

      addParameter(p,'name',[], @ischar);

      addParameter(p,'style',expectedStyles{1} ,...
                     @(x) any(validatestring(x,expectedStyles)));

      addParameter(p,'route',expectedRoutes{1} ,...
                     @(x) any(validatestring(x,expectedRoutes)));

      addParameter(p,'inc', true);

      parse(p, point, varargin{:});

      startSolderDot = false;
      endSolderDot = false;

      if strcmp(p.Results.style,'*-')
        startSolderDot = true;
      elseif strcmp(p.Results.style,'-*')
        endSolderDot = true;
      elseif strcmp(p.Results.style,'*-*')
        startSolderDot = true;
        endSolderDot = true;
      end

      javaPoint =  javaObject('edlab.eda.goethe.Point', ...
                              p.Results.point(1), ...
                              p.Results.point(2));

      javaRatio =  javaObject('java.math.BigDecimal', p.Results.ratio);

      if strcmp(p.Results.route,'--')
        method = 'draw';
      elseif strcmp(p.Results.route,'-')
        method = 'drawHorizontal';
      elseif strcmp(p.Results.route,'|')
        method = 'drawVertical';
      elseif strcmp(p.Results.route,'-|')
        method = 'drawHorizontalVertical';
      else
        method = 'drawVerticalHorizontal';
      end

      if ischar(p.Results.name)
        javaMethod(method                                         , ...
                   this.javaObj                                   , ...
                   javaPoint                                      , ...
                   javaRatio                                      , ...
                   javaObject('java.lang.String', p.Results.name) , ...
                   p.Results.inc                                  , ...
                   startSolderDot                                 , ...
                   endSolderDot                                   , ...
                   true);
      else
        javaMethod(method         , ...
                   this.javaObj   , ...
                   javaPoint      , ...
                   javaRatio      , ...
                   []             , ...
                   p.Results.inc  , ...
                   startSolderDot , ...
                   endSolderDot   , ...
                   true);
      end

      wire = this;
    end
  end

  methods (Static)

    function obj = init(javaObj)
      %INIT Do not call this function by yourself.

      if  isjava(javaObj) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.core.schgen.GeneratedWire', javaObj)

        obj = GeneratedWire(javaObj);
      else
        obj=[];
      end
    end
  end
end