classdef ACMatchAnalysis < Analysis
  %ACMATCHANALYSIS
  
  methods (Access = private)
    function this = ACMATCHANALYSIS(javaObj)
      this.javaObj = javaObj;
    end
  end

  methods (Access = public)

    function retval = getThreshold(this)
      % GETTHRESHOLD Get the threshold that is is used for simulation
      %  analysis.GETTHRESHOLD() returns the threshold that is used for simulation
      %

      javaValue = javaMethod('getThreshold', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      else
        retval = [];
      end
    end

    function retval = setThreshold(this, value)
      % SETTHRESHOLD Specify the threshold that is is used for simulation 
      %  analysis.SETTHRESHOLD(value) specifies the threshold that is used for simulation
      %

      if numel(value)==1 && isnumeric(value) && value > 0

        javaValue = javaMethod('setThreshold', ...
          this.javaObj, javaObject('java.math.BigDecimal', value));

      else
        error('No new value is provided');
      end

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      elseif isempty(javaValue)
        retval = [];
      end
    end

    function  net = getPlus(this)
      % GETPLUS Get the positive reference net 
      %
      % See also ACMatchAnalysis.setPlus  

      javaRetval = javaMethod('getPlus', this.javaObj);
      net = Net.init(javaRetval);
    end

    function  net = setPlus(this, net)
      % SETPLUS Set the positive reference net for identification of voltage
      %  mismatch contribution.
      %  analysis.SETPLUS(net) specifies the net as positive reference.
      %  The method returns the current active reference net.
      %
      % See also ACMatchAnalysis.getPlus

      if isa(net, 'Net')
        javaRetval = javaMethod('setPlus', this.javaObj, net.javaObj);
      elseif isempty(net)
        javaRetval = javaMethod('setPlus', this.javaObj, []);
      else
        error('Provided parameter is not a net or []');
      end

      net = Net.init(javaRetval);
    end

    function  net = getMinus(this)
      % GETMINUS Get the negative reference net 
      %
      % See also ACMatchAnalysis.setMinus

      javaRetval = javaMethod('getMinus', this.javaObj);
      net = Net.init(javaRetval);
    end

    function  net = setMinus(this, net)
      % SETMINUS Set the negative reference net for identification of voltage
      %  mismatch contribution.
      %  analysis.SETMINUS(net) specifies the net as negative reference.
      %  The method returns the current active reference net.
      %
      % See also ACMatchAnalysis.getMinus

      if isa(net, 'Net')
        javaRetval = javaMethod('setMinus', this.javaObj, net.javaObj);
      elseif isempty(net)
        javaRetval = javaMethod('setMinus', this.javaObj, []);
      else
        error('Provided parameter is not a net or []');
      end

      net = Net.init(javaRetval);
    end

    function inst = getProbe(this)
      % GETPROBE  Get the Current mismatch probe
      %  analysis.GETPROBE() returns the probe that is used for current mismatch 
      %  identification
      %

      javaRetval = javaMethod('getProbe', this.javaObj);

      if  isjava(javaRetval) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.simulation.datastruct.Instance', javaRetval)
        inst = Instance.init(javaRetval);
      else
        inst = [];
      end
    end

    function  tf = setProbe(this, probe)
      % SETPROBE Specify the current mismatch probe
      %  analysis.SETPROBE(probe) specified the probe that is used for 
      %  current mismatch  identification
      %

      if isa(probe, 'Instance')

        javaRetval = javaMethod('setProbe', this.javaObj, probe.javaObj);

        if  isjava(javaRetval) && ...
            javaMethod('isInstanceOf',...
              'edlab.eda.edp.simulation.datastruct.Instance', javaRetval)
          tf = true;
        else
          tf = false;
        end
      else
        error('Provided parameter is not an instance');
      end
    end
  end

  methods (Static)
    function obj = init(javaObj)
      %INIT Do not call this function by yourself.
      if  isjava(javaObj) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.simulation.analysis.ACMatchAnalysis', javaObj)
        obj = ACMatchAnalysis(javaObj);
      else
        obj=[];
      end
    end
  end
end