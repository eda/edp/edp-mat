classdef ACParameterSweepAnalysis < Analysis
  %ACPARAMETERSWEEPANALYSIS Reference of an AC parameter sweep analysis
  
  methods (Access = private)
    function this = ACParameterSweepAnalysis(javaObj)
      this.javaObj = javaObj;
    end
  end

  methods (Access = public)

    function parameter = getParameter(this)
      % GETPARAMETER Get the parameter that is varied
      %  analysis.GETPARAMETER() returns the parameter that is varied.
      %
      % See also DatabaseParameter.

      javaParameter = javaMethod('getParameter', this.javaObj);

      parameter = DatabaseParameter.init(javaParameter);
    end

    function tf = setParameter(this, parameter)
      % SETPARAMETER Specify the parameter to be varied
      %  analysis.SETPARAMETER(param) sets the parameter to be varied.
      %  The method return true when successful, false otherwise.
      %
      % See also DatabaseParameter. 

      if isa(parameter, 'DatabaseParameter')

        tf = javaMethod('setParameter', this.javaObj, ...
               parameter.javaObj);
      else
        error('Provided argument is not a parameter');
      end
    end

    function values = getValues(this)
      % GETVALUES Get the frequencies that are used for simulation
      %  analysis.GETVALUES() returns a vector of all frequencies that 
      %  are used for simulation
      %
      % Example:
      %  values = analysis.getValues()
      %
      %  values =
      %
      %      [1 10 100]

      javaArray = javaMethod('getValues', this.javaObj);

      values = zeros(1, numel(javaArray));

      for i=1:numel(javaArray)
        values(i) = javaMethod('doubleValue', javaArray(i));
      end
    end

    function values = setValues(this, values)
      % SETVALUES Specify the frequencies that are used for simulation
      %  analysis.SETVALUES(freq) returns the new values when valid, 
      %  [] otherwise.
      %  One must provide an array of numbers as parameter.
      %
      % Example:
      %  values = analysis.setValues([1 10 100])
      %
      %      [1 10 100]

      if isreal(values)

        javaValuesArray = javaArray('java.math.BigDecimal',...
          numel(values));

        for i=1:numel(javaValuesArray)
          javaValuesArray(i) = javaObject('java.math.BigDecimal', ...
            values(i));
        end

        javaValuesArray = javaMethod('setValues', this.javaObj,...
          javaValuesArray);

        values = zeros(1, numel(javaArray));
  
        for i=1:numel(javaArray)
          values(i) = javaMethod('doubleValue', javaValuesArray(i));
        end
      else
        error('No array of values is provided');
      end
    end


    function frequency = getFrequency(this)
      % GETFREQUENCY Get the frequency that is used for simulation
      %  analysis.GETFREQUENCY() returns the frequency that is used
      %  for simulation

      frequency = javaMethod('doubleValue', ...
       javaMethod('getFrequency', this.javaObj));
    end

    function frequency = setFrequency(this, frequency)
      % SETFREQUENCY Specify the frequency that is used for simulation
      %  analysis.SETFREQUENCY(frequency) sets the frequency to be used.
      %  The method returns the current frequency.

      if isreal(frequency) && numel(frequency) == 1

        javaFrequency = javaMethod('setFrequency', this.javaObj, ...
                          javaObject('java.math.BigDecimal',frequency));


        if isempty(javaFrequency)
          frequency = [];
        else
          frequency = javaMethod('doubleValue', javaFrequency);
        end

      else
        error('Provided argument is not a number');
      end
    end
  end

  methods (Static)
    function obj = init(javaObj)
      %INIT Do not call this function by yourself.
      if  isjava(javaObj) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.simulation.analysis.ACParameterSweepAnalysis', javaObj)
        obj = ACParameterSweepAnalysis(javaObj);
      else
        obj=[];
      end
    end
  end
end