classdef Terminal < ExpertDesignPlanWrapper
  % TERMINAL Reference in a Test in the Expert Design Plan

  % Copyright 2023 Reutlingen University, Electronics & Drives
  
  methods (Access = private)
    function this = Terminal(javaObj)
      this.javaObj = javaObj;
    end
  end

  methods (Access = public)

    function name = getName(this)
      % GETNAME  Get the name of a terminal
      name = handleJavaString(javaMethod('getName', this.javaObj));
    end

    function test = getTest(this)
      % GETTEST  Get the test where the net is defined

      javaTestObj = javaMethod('getTest',this.javaObj);
      test = Test.init(javaTestObj);
    end

    function instance = getInstance(this)
      % GETINSTANCE  Get the instance that is associated with the net

      javaInstanceObj = javaMethod('getInstance',this.javaObj);
      instance = Instance.init(javaInstanceObj);
    end

    function net = getNet(this)
      % GETNET  Get the net that is connected to a terminal

      net = [];

      javaNet = javaMethod('getNet', this.javaObj);

      if ~isempty(javaNet)
        net = Net.init(javaNet);
      end
    end

    function address = getAddress(this)
      % GETADDRESS  Get address to terminal
      javaArray = javaMethod('getAddress',this.javaObj);
    
      address = cell(1, numel(javaArray));

      for i=1:numel(javaArray)
        address{i} = handleJavaString(javaArray(i));
      end
    end
  
  end

  methods (Static)
    function obj = init(javaObj)
      %INIT Do not call this function by yourself.
      if  isjava(javaObj) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.simulation.datastruct.Terminal', javaObj)
        obj = Terminal(javaObj);
      else
        obj=[];
      end
    end
  end
end