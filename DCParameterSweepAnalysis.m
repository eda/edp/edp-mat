classdef DCParameterSweepAnalysis < Analysis
  %ANALYSIS Reference of a DCParameterSweepAnalysis
  
  methods (Access = private)
    function this = DCParameterSweepAnalysis(javaObj)
      this.javaObj = javaObj;
    end
  end

  methods (Access = public)

    function maxiters = getMaxiters(this)
      % GETMAXITERS Get the maximum number of iterations
      %  analysis.GETMAXITERS() returns the maximum number of iterations
      %  during Newton-Raphson algorithm.
      %
      % See also DCParameterSweepAnalysis.setMaxiters

      maxiters = javaMethod('getMaxiters', this.javaObj);
    end

    function maxiters = setMaxiters(this, maxiters)
      % SETMAXITERS Set the maximum number of iterations
      %  analysis.SETMAXITERS(maxiters) sets the maximum number of iterations
      %  during Newton-Raphson algorithm and returns the current maxiters.
      %  maxiters is a positive integer.
      %
      % See also DCParameterSweepAnalysis.getMaxiters

      if isinteger(maxiters) && numel(maxiters) ==1
        maxiters = javaMethod('setMaxiters', this.javaObj, maxiters);
      else
        error('Provided parameter ''maxiters'' is not an integer');
      end
    end   

    function maxsteps = getMaxsteps(this)
      % GETMAXSTEPS Get the maximum number of steps
      %  analysis.GETMAXSTEPS() returns the maximum number of steps used in 
      %  the homotopy method.
      %
      % See also DCParameterSweepAnalysis.setMaxsteps

      maxsteps = javaMethod('getMaxsteps', this.javaObj);
    end

    function maxsteps = setMaxsteps(this, maxsteps)
      % SETMAXSTEPS Set the maximum number of steps
      %  analysis.SETMAXSTEPS(maxsteps) sets the maximum number if steps used in 
      %  the homotopy method and returns the current number of steps.
      %  maxsteps is a positive integer.
      %
      % See also DCParameterSweepAnalysis.getMaxsteps 

      if isinteger(maxsteps) && numel(maxsteps) == 1
        maxsteps = javaMethod('setMaxsteps', this.javaObj, maxsteps);
      else
        error('Provided parameter ''maxsteps'' is not an integer');
      end
    end   

    function homotopy = getHomotopy(this)
      % GETHOMOTOPY Get the homotopy method
      %  analysis.GETHOMOTOPY() returns the homotopy method.
      %  The homotopy is either
      %   - NONE
      %   - GMIN
      %   - SOURCE
      %   - DPTRAN
      %   - PTRAN
      %   - ARCLENGTH
      %   - TRANRAMPUP
      %   - ALL
      %
      % See also DCParameterSweepAnalysis.setHomotopy

      homotopy = handleJavaString(javaMethod('getHomotopy', this.javaObj));
    end

    function homotopy = setHomotopy(this, homotopy)
      % SETHOMOTOPY Set the homotopy method
      %  analysis.SETHOMOTOPY(homotopy) sets the homotopy method
      %  and returns the current active homotopy.
      %
      % See also DCParameterSweepAnalysis.getHomotopy

      if ischar(homotopy)
        javaHomotopy = javaMethod('setHomotopy', this.javaObj,...
          javaObject('java.lang.String', homotopy));

        homotopy = handleJavaString(javaHomotopy);
      else
        error('Provided parameter ''homotopy'' is not a character array');
      end
    end

    function parameter = getParameter(this)
      % GETPARAMETER Get the parameter that is varied
      %  analysis.GETPARAMETER() returns the parameter that is varied.
      %
      % See also DatabaseParameter. 

      javaParameter = javaMethod('getParameter', this.javaObj);

      parameter = DatabaseParameter.init(javaParameter);
    end

    function tf = setParameter(this, parameter)
      % SETPARAMETER Specify the parameter to be varied
      %  analysis.SETPARAMETER(param) sets the parameter to be varied.
      %  The method return true when successful, false otherwise.
      %
      % See also DatabaseParameter. 

      if isa(parameter, 'DatabaseParameter')

        tf = javaMethod('setParameter', this.javaObj, ...
               parameter.javaObj);
      else
        error('Provided argument is not a parameter');
      end
    end

    function values = getValues(this)
      % GETVALUES Get the values that are used for simulation
      %  analysis.GETVALUES() returns a vector of all values that 
      %  are used for simulation
      %
      % Example:
      %  values = analysis.getValues()
      %
      %  values =
      %
      %      [1 10 100]

      javaArray = javaMethod('getValues', this.javaObj);

      values = zeros(1, numel(javaArray));

      for i=1:numel(javaArray)
        values(i) = javaMethod('doubleValue', javaArray(i));
      end
    end

    function values = setValues(this, values)
      % SETVALUES Specify the values that are used for simulation
      %  analysis.SETVALUES(values) returns the new values when valid, 
      %  [] otherwise.
      %  One must provide an array of numbers as parameter.
      %
      % Example:
      %  values = analysis.setValues([1 10 100])
      %
      %      [1 10 100]

      if isreal(values)

        javaValuesArray = javaArray('java.math.BigDecimal',...
          numel(values));

        for i=1:numel(javaValuesArray)
          javaValuesArray(i) = javaObject('java.math.BigDecimal', ...
            values(i));
        end

        javaValuesArray = javaMethod('setValues', this.javaObj,...
          javaValuesArray);

        values = zeros(1, numel(javaValuesArray));
  
        for i=1:numel(javaValuesArray)
          values(i) = javaMethod('doubleValue', javaValuesArray(i));
        end
      else
        error('No array of values is provided');
      end
    end
  end

  methods (Static)
    function obj = init(javaObj)
      %INIT Do not call this function by yourself.
      if  isjava(javaObj) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.simulation.analysis.DCParameterSweepAnalysis', javaObj)
        obj = DCParameterSweepAnalysis(javaObj);
      else
        obj=[];
      end
    end
  end
end