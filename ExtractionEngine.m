classdef ExtractionEngine
  % EXTRACTIONENGINE Handle to the LUT of a primitive device
    
  properties (Access = private)
    jEnv
    jEng
  end

  methods (Access = private)
    function obj = ExtractionEngine(varargin)

      obj.jEnv = varargin{1};

      if numel(varargin)==1
        obj.jEng = javaObject(...
          'edlab.eda.predict.extraction.ExtractionEngine', obj.jEnv, false);
      else
        obj.jEng = varargin{2};
      end
    end
  end

  methods

    function retval = eq(this, obj)
      % EQ Check if an EXTRACTIONENGINE object matches with 
      %  another EXTRACTIONENGINE
      if isa(obj, 'ExtractionEngine')
        retval = javaMethod('equals',this.javaObj, obj.javaObj);
      else
        retval  = false;
      end
    end

    function retval = ne(this, obj)
      % NE Check if an EXTRACTIONENGINE does not match with 
      %  another ExtractionEngine object
      retval = ~this.eq(obj);
    end

    function lib = getLib(this)
      % GETLIB Get the library name
      %  obj.GETLIB() returns the library name as character array.
      %
      %  Example: 
      %  >> libName = obj.getLib()
      %
      %  libName =
      %
      %      'PRIMLIB'
      %
      lib = ExtractionEngine.handleJavaString( ...
              javaMethod('getLib', this.jEnv));
    end

    function cell = getCell(this)
      % GETCELL Get the cell name
      %  obj.GETCELL() returns the name of the cell as character array.
      %
      %  Example: 
      %  >> cellName = obj.getCell()
      %
      %  cellName =
      %
      %      'nmos'
      %
      cell = ExtractionEngine.handleJavaString( ...
              javaMethod('getCell', this.jEnv));
    end

    function modelName = getModelName(this)
      % GETMODELNAME Get the name of the simulation model
      %  obj.GETMODELNAME() returns the name of the simulation model.
      %
      %  Example: 
      %  >> modelName = obj.getModelName()
      %
      %  modelName =
      %
      %      'nch'
      %
      modelName = ExtractionEngine.handleJavaString( ...
              javaMethod('getModelName', this.jEnv));
    end

    function modelType = getModelType(this)
      % GETMODELTYPE Get the model type
      %  obj.GETMODELTYPE() returns the model type.
      %
      %  Example: 
      %  >> modelType = obj.getModelType()
      %
      %  modelType =
      %
      %      'bsim3v3'
      %
      modelType = ExtractionEngine.handleJavaString( ...
              javaMethod('getModelType', this.jEnv));
    end

    function tag = getTag(this)
      % GETTAG Get the tag of the database
      %  obj.GETTAG() returns tag of the database
      %
      %  Example: 
      %  >> tag = obj.getTag()
      %
      %  tag =
      %
      %      '6.1'
      %
      tag = ExtractionEngine.handleJavaString( ...
              javaMethod('getTag', this.jEnv));
    end

    function models = getConstantModels(this)
      % GETCONSTANTMODELS Get the constant device models
      %  obj.GETCONSTANTMODELS() returns a cell array of the constant models
      %
      %  Example: 
      %  >> models = obj.getConstantModels()
      %
      %  models =
      %
      %      {'dev'}
      %
      javaConstantModelsList = javaMethod('getConstantModels', ....
         this.jEnv);
      listSize = javaMethod('size', javaConstantModelsList);

      models = cell(listSize, 1);
 
      for i = 1:listSize
        models{i} = ...
          ExtractionEngine.handleJavaString(...
            javaMethod('getName', javaMethod('get', ...
                                  javaConstantModelsList, i-1)));
      end
    end

    function models = getVariableModels(this)
      % GETVARIABLEMODELS Get the variable models
      %  obj.GETVARIABLEMODELS() returns a cell array of the variable models
      %
      %  Example: 
      %  >> models = obj.getVariableModels()
      %
      %  models =
      %
      %      {'tech'}
      %
      javaVariableModelsList = javaMethod('getVariableModels',...
        this.jEnv);
      listSize = javaMethod('size', javaVariableModelsList);

      models = cell(listSize, 1);

      for i = 1:listSize
        models{i} = ...
          ExtractionEngine.handleJavaString(...
            javaMethod('getName', javaMethod('get', ...
                                  javaVariableModelsList, i-1)));
      end
    end

    function sections = getSections(this, name)
      % GETSECTIONS Get all sections of a variable model
      %  obj.GETSECTIONS(<name>) returns the sections of a variable model
      %  as cell array.
      %
      %  Example: 
      %  >> models = obj.getVariableModels('tech')
      %
      %  models =
      %
      %      {'slow' 'typ' 'fast'}
      %
      % See also getVariableModels.

       if ischar(name)

        javaVariableModel = javaMethod('getVariableModel', ...
          this.jEnv, name);

        if isjava(javaVariableModel) && ...
           javaMethod('isInstanceOf',...
            'edlab.eda.predict.model.VariableModel', javaVariableModel)
 
          sections = ExtractionEngine.handleJavaStringArray( ...
                       javaMethod('getSectionAliases', javaVariableModel));

        else
          error('''%s'' is not variable model', name);
        end
      else
        error('Provided parameter ''name'' is not a character array');
      end
    end

    function operatingPoints = getOperatingPoints(this)
      % GETOPERATINGPOINTS Get all operating points in the database
      %  obj.GETOPERATINGPOINTS() returns a cell array of all operating points
      %
      %  Example: 
      %  >> operatingPoints = obj.getOperatingPoints()
      %
      %  operatingPoints =
      %
      %      {'gm' 'vdsat' 'id'}
      %
      javaList = javaMethod('getOperatingPoints', this.jEnv);
      listSize = javaMethod('size', javaList);

      operatingPoints = cell(listSize, 1);

      for i = 1:listSize
        operatingPoints{i} = ...
          ExtractionEngine.handleJavaString(...
            javaMethod('getName', javaMethod('get', javaList, i-1)));
      end
    end

    function unit = getOperatingPointUnit(this, operatingPoint)
      % GETOPERATINGPOINTUNIT Get the unit of an operating point
      %  obj.GETOPERATINGPOINTUNIT(<name>) returns the unit for an operating
      %  point.
      %
      %  Example: 
      %  >> unit = obj.getOperatingPointUnit('id')
      %
      %  unit =
      %
      %      'A'
      %
      % See also getOperatingPoints.

      if ischar(operatingPoint)

        javaOperatingPoint = javaMethod('getOperatingPoint', ...
                             this.jEnv, operatingPoint);

        if javaMethod('isInstanceOf',...
              'edlab.eda.predict.model.OperatingPoint', javaOperatingPoint)
          
          unit = ExtractionEngine.handleJavaString( ...
              javaMethod('getUnit', javaOperatingPoint));
        else
          error('''%s'' is not an operating point', operatingPoint);
        end
      else
         error('Provided parameter is not a character array');
      end
    end

    function values = getTemperatures(this)
      % GETTEMPERATURES Get all temperatures in the database
      %  obj.GETTEMPERATURES() returns all temperatures that were used during 
      %  characterization as array.
      %
      %  Example: 
      %  >> temperatures = obj.getTemperatures()
      %
      %  temperatures =
      %
      %      [-40.0 0.0 25.0 100.0]
      %

      javaParameterRange = ...
        javaMethod('getTemperatures', this.jEnv);

      if isjava(javaParameterRange) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.predict.model.ParameterRange', javaParameterRange)

        values = ExtractionEngine.handleJavaBigDecimalArray(...
          javaMethod('getValuesAsArray', javaParameterRange));
      else
        error('No temperatures are available');
      end
    end

    function currents = getVariableCurrents(this)
      % GETVARIABLECURRENTS Get all variable currents
      %  obj.GETVARIABLECURRENTS() returns a cell array of all currents
      %  that were varied during characterization.
      %
      %  Example: 
      %  >> vc = obj.getVariableCurrents()
      %
      %  vc =
      %
      %      {'B'}
      %
      javaVaryingCurrentsList = ...
        javaMethod('getVaryingCurrents', this.jEnv);

      listSize = javaMethod('size', javaVaryingCurrentsList);

      currents = cell(listSize, 1);

      for i=1:listSize

        electricalRange = javaMethod('get', javaVaryingCurrentsList, i-1);

        currents{i} =  ExtractionEngine.handleJavaString( ...
                        javaMethod('getName', electricalRange));
      end
    end

    function tf = isVariableCurrent(this, name)
      % ISVARIABLECURRENT Check if a current is varied in the database
      %  obj.GETVARIABLECURRENTS(name) returns true when a current is varied,
      %  false otherwise.
      %
      %  Example: 
      %  >> tf = obj.isVariableCurrent('D')
      %
      %  tf =
      %
      %      false
      %


      if ischar(name)
        tf = javaMethod('isVaryingCurrentsRange', this.jEnv,...
          javaObject('java.lang.String', name));
      else
        error('Provided parameter is not a character array')
      end
    end

    function values = getVariableCurrentValues(this, name)
      % GETVARIABLECURRENTVALUES Get all currents used for characterization
      %  obj.GETVARIABLECURRENTVALUES(<name>) returns am array of current values
      %  used during characterization for a current that was varied during
      %  characterization.
      %
      %  Example: 
      %  >> vals = obj.getVariableCurrentValues('B')
      %
      %  vals =
      %
      %      [1e-6 2e-6 3e-6 4e-5]
      %

      if ischar(name)

        javaElectricalRange = ...
          javaMethod('getVaryingCurrentsRange', this.jEnv, name);

        if isjava(javaElectricalRange) && ...
            javaMethod('isInstanceOf',...
              'edlab.eda.predict.model.ElectricalRange', javaElectricalRange)

          values = ExtractionEngine.handleJavaBigDecimalArray(...
            javaMethod('getValuesAsArray', javaElectricalRange));
        else
          error('''%s'' is not a variable current');
        end
      else
        error('Provided parameter ''name'' is not a character array');
      end
    end

    function voltages = getVariableVoltages(this)
      % GETVARIABLEVOLTAGES Get all variable voltages
      %  obj.GETVARIABLEVOLTAGES() returns a cell array of all voltages
      %  that were varied during characterization.
      %
      %  Example: 
      %  >> vv = obj.getVariableVoltages()
      %
      %  vv =
      %
      %      {'D' 'GS' 'DS'}
      %
      javaVaryingVoltagesList = ...
        javaMethod('getVaryingVoltages', this.jEnv);

      listSize = javaMethod('size', javaVaryingVoltagesList);

      voltages = cell(listSize, 1);

      for i=1:listSize

        electricalRange = javaMethod('get', javaVaryingVoltagesList, i-1);

        voltages{i} =  ExtractionEngine.handleJavaString( ...
                        javaMethod('getName', electricalRange));
      end
    end

    function tf = isVariableVoltage(this, name)
      % ISVARIABLEVOLTAGE Check if a voltage is varied in the database
      %  obj.ISVARIABLEVOLTAGE(name) returns true when a voltage is varied,
      %  false otherwise.
      %
      %  Example: 
      %  >> tf = obj.isVariableVoltage('D')
      %
      %  tf =
      %
      %      false
      %


      if ischar(name)
        tf = javaMethod('isVaryingVoltageRange', this.jEnv,...
          javaObject('java.lang.String', name));
      else
        error('Provided parameter is not a character array')
      end
    end

    function values = getVariableVoltageValues(this, name)
      % GETVARIABLEVOLTAGEVALUES Get all voltages used for characterization
      %  obj.GETVARIABLEVOLTAGEVALUES(<name>) returns an array of voltage values
      %  used during characterization for a voltage that was varied during
      %  characterization.
      %
      %  Example: 
      %  >> vals = obj.getVariableVoltageValues('GS')
      %
      %  vals =
      %
      %      [0 0.1 0.2 0.3 0.4 0.5]
      %
      if ischar(name)

        javaElectricalRange = ...
          javaMethod('getVaryingVoltageRange', this.jEnv, name);

        if isjava(javaElectricalRange) && ...
            javaMethod('isInstanceOf',...
              'edlab.eda.predict.model.ElectricalRange', javaElectricalRange)

          values = ExtractionEngine.handleJavaBigDecimalArray(...
            javaMethod('getValuesAsArray', javaElectricalRange));
        else
          error('''%s'' is not a variable voltage');
        end
      else
        error('Provided parameter ''name'' is not a character array');
      end
    end

    function parameters = getVariableParameters(this)
      % GETVARIABLEPARAMETERS Get all variable parameters
      %  obj.GETVARIABLEVOLTAGES() returns a cell array of all parameters
      %  that were varied during characterization.
      %
      %  Example: 
      %  >> vp = obj.getVariableParameters()
      %
      %  vp =
      %
      %      {'w' 'l'}
      %
      javaVaryingParametersList = ...
        javaMethod('getVaryingParameters', this.jEnv);

      listSize = javaMethod('size', javaVaryingParametersList);

      parameters = cell(listSize, 1);

      for i=1:listSize
        parameters{i} = ExtractionEngine.handleJavaString( ...
                          javaMethod('getName', ...
                            javaMethod('get', javaVaryingParametersList, i-1)));
      end
    end

    function tf = isVariableParameter(this, name)
      % ISVARIABLEPARAMETER Check if a parameter is varied in the database
      %  obj.ISVARIABLEPARAMETER(name) returns true when a parameter is varied,
      %  false otherwise.
      %
      %  Example: 
      %  >> tf = obj.isVariableVoltage('nf')
      %
      %  tf =
      %
      %      false
      %


      if ischar(name)
        tf = javaMethod('isVaryingParameterRange', this.jEnv,...
          javaObject('java.lang.String', name));
      else
        error('Provided parameter is not a character array')
      end
    end

    function values = getVariableParameterValues(this, name)
      % GETVARIABLEPARAMETERVALUES Get all parameter values used for characterization
      %  obj.GETVARIABLEPARAMETERVALUES(<name>) returns an array of parameter values
      %  used during characterization for a parameter that was varied during
      %  characterization.
      %
      %  Example: 
      %  >> vals = obj.getVariableParameterValues('w')
      %
      %  vals =
      %
      %      [1e-6 5e-6 10e-6]
      %
      if ischar(name)

        javaParameterRange = ...
          javaMethod('getVaryingParameterRange', this.jEnv, name);

        if isjava(javaParameterRange) && ...
            javaMethod('isInstanceOf',...
              'edlab.eda.predict.model.ParameterRange', javaParameterRange)

          values = ExtractionEngine.handleJavaBigDecimalArray(...
            javaMethod('getValuesAsArray', javaParameterRange));
        else
          error('''%s'' is not variable parameter');
        end
      else
        error('Provided parameter ''name'' is not a character array');
      end
    end

    function currents = getConstantCurrents(this)
      % GETCONSTANTCURRENTS Get all constant currents
      %  obj.GETCONSTANTCURRENTS() returns a cell array of all currents
      %  that are constant.
      %
      %  Example: 
      %  >> cc = obj.getConstantCurrents()
      %
      %  cc =
      %
      %      {'B'}
      %
      javaConstantCurrentsList = ...
        javaMethod('getConstantCurrents', this.jEnv);

      listSize = javaMethod('size', javaConstantCurrentsList);

      currents = cell(listSize, 1);

      for i=1:listSize

        currents{i} = ExtractionEngine.handleJavaString(...
                        javaMethod('getName', ...
                          javaMethod('get', javaConstantCurrentsList, i-1)));
      end
    end

    function tf = isConstantCurrent(this, name)
      % ISCONSTANTCURRENT Check if a current is constant in the database
      %  obj.ISCONSTANTCURRENT(name) returns true when a current is constant,
      %  false otherwise.
      %
      %  Example: 
      %  >> tf = obj.isConstantCurrent('S')
      %
      %  tf =
      %
      %      false
      %


      if ischar(name)
        tf = javaMethod('isConstantCurrent', this.jEnv,...
          javaObject('java.lang.String', name));
      else
        error('Provided parameter is not a character array')
      end
    end


    function value = getConstantCurrentValue(this, name)
      % GETCONSTANTCURRENTVALUE Get a current that was used for characterization
      %  obj.GETCONSTANTCURRENTVALUE(<name>) returns the value of a current
      %  that was applied for characterization.
      %
      %  Example: 
      %  >> val = obj.getConstantCurrentValue('D')
      %
      %  val =
      %
      %      1e-5
      %
      if ischar(name)

        javaConstantElectrical = ...
          javaMethod('getConstantCurrent', this.jEnv, name);

        if isjava(javaConstantElectrical) && ...
            javaMethod('isInstanceOf', ...
              'edlab.eda.predict.model.ConstantElectrical', ...
                javaConstantElectrical)

          value = javaMethod('doubleValue', ...
              javaMethod('getValue', javaConstantElectrical));
        else
          error('''%s'' is not a constant parameter', name);
        end
      else
        error('Provided parameter ''name'' is not a character array');
      end
    end  

    function voltages = getConstantVoltages(this)
      % GETCONSTANTVOLTAGES Get all constant voltages
      %  obj.GETCONSTANTVOLTAGES() returns a cell array of all voltages
      %  that are constant.
      %
      %  Example: 
      %  >> cv = obj.getConstantVoltages()
      %
      %  cv =
      %
      %      {'GD'}
      %
      javaConstantVoltagesList = ...
        javaMethod('getConstantVoltages', this.jEnv);

      listSize = javaMethod('size', javaConstantVoltagesList);

      voltages = cell(listSize, 1);

      for i=1:listSize

        voltages{i} = ExtractionEngine.handleJavaString(...
                        javaMethod('getName', ...
                          javaMethod('get', javaConstantVoltagesList, i-1)));
      end
    end

    function tf = isConstantVoltage(this, name)
      % ISCONSTANTVOLTAGE Check if a voltage is constant in the database
      %  obj.ISCONSTANTVOLTAGE(name) returns true when a voltage is constant,
      %  false otherwise.
      %
      %  Example: 
      %  >> tf = obj.isConstantVoltage('B')
      %
      %  tf =
      %
      %      true
      %


      if ischar(name)
        tf = javaMethod('isConstantVoltage', this.jEnv,...
          javaObject('java.lang.String', name));
      else
        error('Provided parameter is not a character array')
      end
    end

    function value = getConstantVoltageValue(this, name)
      % GETCONSTANTVOLTAGEVALUE Get a voltage that was used for characterization
      %  obj.GETCONSTANTVOLTAGEVALUE(<name>) returns the value of a voltage
      %  that was applied for characterization.
      %
      %  Example: 
      %  >> val = obj.getConstantVoltageValue('S')
      %
      %  val =
      %
      %      0.0
      %
      if ischar(name)

        javaConstantElectrical = ...
          javaMethod('getConstantVoltage', this.jEnv, name);

        if isjava(javaConstantElectrical) && ...
            javaMethod('isInstanceOf', ...
              'edlab.eda.predict.model.ConstantElectrical', ...
                javaConstantElectrical)

          value = javaMethod('doubleValue', ...
              javaMethod('getValue', javaConstantElectrical));

        else
          error('''%s'' is not a constant parameter', name);
        end
      else
        error('Provided parameter ''name'' is not a character array');
      end
    end  

    function parameters = getConstantParameters(this)
      % GETCONSTANTPARAMETERS Get all constant parameters
      %  obj.GETCONSTANTPARAMETERS() returns a cell array of all parameters
      %  that are constant.
      %
      %  Example: 
      %  >> cp = obj.getConstantParameters()
      %
      %  cp =
      %
      %      {'m'}
      %
      javaConstantParametersList = ...
        javaMethod('getConstantParameters', this.jEnv);

      listSize = javaMethod('size', javaConstantParametersList);

      parameters = cell(listSize, 1);

      for i=1:listSize

        parameters{i} = ExtractionEngine.handleJavaString(...
                        javaMethod('getName', ...
                          javaMethod('get', javaConstantParametersList, i-1)));
      end
    end

    function tf = isConstantParameter(this, name)
      % ISCONSTANTPARAMETER Check if a parameter is constant in the database
      %  obj.ISCONSTANTPARAMETER(name) returns true when a parameter is constant,
      %  false otherwise.
      %
      %  Example: 
      %  >> tf = obj.isConstantParameter('w')
      %
      %  tf =
      %
      %      true
      %


      if ischar(name)
        tf = javaMethod('isConstantParameter', this.jEnv,...
          javaObject('java.lang.String', name));
      else
        error('Provided parameter is not a character array')
      end
    end

    function value = getConstantParameterValue(this, name)
      % GETCONSTANTPARAMETERVALUE Get a parameter value that was used for characterization
      %  obj.GETCONSTANTPARAMETERVALUE(<name>) returns the value of a parameter
      %  that was used for characterization.
      %
      %  Example: 
      %  >> val = obj.getConstantParameterValue('w')
      %
      %  val =
      %
      %      1e-2
      %
      if ischar(name)

        javaConstantParameter = ...
          javaMethod('getConstantParameter', this.jEnv, name);

        if isjava(javaConstantParameter) 

          if javaMethod('isInstanceOf',...
              'edlab.eda.predict.model.ConstantString', javaConstantParameter)
            value = ExtractionEngine.handleJavaString( ...
                          javaMethod('getValue', javaConstantParameter));
          elseif javaMethod('isInstanceOf',...
              'edlab.eda.predict.model.ConstantValue', javaConstantParameter)

            value = javaMethod('doubleValue', ...
                      javaMethod('getValue', javaConstantParameter));
          else
            value = [];
          end
        end
      else
        error('Provided parameter ''name'' is not a character array');
      end
    end 

    function defaultEvalOrder = getDefaultEvalOrder(this)
      javaIdentifiersArray = javaMethod('getIdentifiersAsArray', this.jEng);
      
      defaultEvalOrder = cell(1, numel(javaIdentifiersArray));
      
      for i=1:numel(javaIdentifiersArray)

        javaIdentifier = javaIdentifiersArray(i);
        
        defaultEvalOrder{i} = ExtractionEngine.handleJavaString(...
          javaMethod('getName', javaIdentifier));
      end
    end

    function points = getPoints(this, varargin)

      parser = inputParser;
  
      variableModels = this.getVariableModels();

      % create default value  for 'models' when no value is provided
      defaultModels = cell(1,numel(variableModels));
      
      for i = 1:numel(variableModels)

        sections = this.getSections(variableModels{i});

        defaultModels{i} = { variableModels{i} ...
                             sections{floor(numel(sections)/2)}};
      end
      
      % add 'models' as keyword parameter (checks are performed later)
      addParameter(parser,'models', defaultModels);

      % add 'operatingPoints' as keyword parameter (checks are performed)
      addParameter(parser,'operatingPoints', ...
        {this.getOperatingPoints{1}}, ...
        @(subSet) isempty(setdiff(subSet, this.getOperatingPoints)));

      javaIdentifiersArray = javaMethod('getIdentifiersAsArray', this.jEng);
      
      defaultEvalOrder = cell(1, numel(javaIdentifiersArray));
      
      for i=1:numel(javaIdentifiersArray)

      	javaIdentifier = javaIdentifiersArray(i);
      	
      	defaultEvalOrder{i} = ExtractionEngine.handleJavaString(...
      		javaMethod('getName', javaIdentifier));
        
        % add parameter for each degree of freedom from the database
      	addParameter(parser,...
            defaultEvalOrder{i}, ...
      		  [javaMethod('doubleValue', ...
            javaMethod('getValue', ...
              javaMethod('getRange',...
                javaIdentifier)))],...
            @(x) isreal(x) ||  iscell(x));	
      end

      % add 'evalOrder' as keyword parameter (checks are performed)
	    addParameter(parser,'evalOrder',defaultEvalOrder, ...
         @(x) isempty(setdiff(x,defaultEvalOrder)));
      
      parse(parser, varargin{:});
      
      javaOperatingPoints = javaArray('edlab.eda.predict.model.OperatingPoint', ....
                            numel(parser.Results.operatingPoints));

      for i=1:numel(parser.Results.operatingPoints)
        javaOperatingPoints(i) = javaMethod('getOperatingPoint', this.jEnv, ...
          javaObject('java.lang.String',parser.Results.operatingPoints{i}));
      end
      
      models = parser.Results.models;

      [m, n] = size(models);

      if m ~= 1 || n~= numel(variableModels)
        error('Parameter ''models'' must be cell array of 1x%d', n);
      end

      javaModels = javaArray('java.lang.String', n, 2);

      for i=1:n

        model = models{i};

        [o, p] = size(model);

        if o~=1 || p~=2
          error('Parameter ''models'' at index %d must be cell array of 1x2', i);
        end

        if ~ischar(model{1}) || ~ischar(model{2})
          error('Parameter ''models'' at index %d must be cell array of 1x2 of character arrays', i);
        end

        javaModels(i,1) = javaObject('java.lang.String', model{1});
        javaModels(i,2) = javaObject('java.lang.String', model{2});
      end

      if numel(javaModels) > 0
        modelIndex = javaMethod('getModelIndex', this.jEnv, javaModels);
      else
        modelIndex = 0;
      end

      if modelIndex < 0
        error('Provided model combination is not available');
      end

      evalOrder = parser.Results.evalOrder;

      javaEvalOrderArray = ...
        javaArray('edlab.eda.predict.extraction.EvaluationIdentifier', ...
          numel(evalOrder));

      matrixSize = zeros(1, numel(evalOrder));

      for i = 1:numel(evalOrder)

        javaEvalOrderArray(i) = ...
          javaIdentifiersArray(...
          find(strcmp(defaultEvalOrder, evalOrder{i})));
      end

      javaEvalRoutine = javaMethod('createRoutine', this.jEng, ...
        javaEvalOrderArray);

      for i = 1:numel(evalOrder)

        data = parser.Results.(evalOrder{i});

        if isreal(data)

          javaBigDecimalArray = javaArray('java.math.BigDecimal', numel(data));

          matrixSize(i) = numel(data);
          
          for j=1:numel(data)
            javaBigDecimalArray(j) = javaObject('java.math.BigDecimal', data(j));
          end

          javaMethod('setValuesExplicitForIdentifier', javaEvalRoutine, ...
            javaEvalOrderArray(i), javaBigDecimalArray);

        elseif iscell(data)

          [n,~] = size(data);

          if n~=1
            error('ERROR');
          end

          matrixSize(i) = 1;

          abs = 0;
          identifiers = {};
          scalings = {};
          cnt=1;

          for j = 1:numel(data)

            elem = data{j};

            if isreal(elem) && numel(elem)==1

              abs = elem;

            elseif ischar(elem)

              identifiers{cnt}=elem;
              scalings{cnt}=1.0;

              cnt = cnt+1;

            elseif iscell(elem) && numel(elem)>1 && ischar(elem{1})

              if numel(elem)==1
                identifiers{cnt}=elem{1};
                scalings{cnt}=1.0;
                cnt = cnt+1;
              elseif isreal(elem{2}) && numel(elem{2})==1
                identifiers{cnt}=elem{1};
                scalings{cnt}=elem{2};
              else
                error('ERROR') 
              end

            else
              error('ERROR');
            end
          end

          javaDependeciesArray = ...
            javaArray('edlab.eda.predict.extraction.EvaluationIdentifier', ...
            numel(identifiers));
          javaFactorsArray = ...
            javaArray('java.math.BigDecimal', ...
            numel(identifiers));

          for j=1:numel(identifiers)
            javaDependeciesArray(j) = javaMethod('getIdentifier', this.jEng, ...
              javaObject('java.lang.String', identifiers{j}));
            javaFactorsArray(j)=javaObject('java.math.BigDecimal', scalings{j});
          end

          javaMethod('setLinearEquationSystemForIdentifier', javaEvalRoutine, ...
            javaEvalOrderArray(i), javaDependeciesArray, javaFactorsArray, ...
            javaObject('java.math.BigDecimal', abs));
        else
          error('ERROR');
        end
      end
      
      javaMap = javaMethod('extract', this.jEng, modelIndex, ...
        javaOperatingPoints, javaEvalRoutine);
      
      %create return struct
      points = struct();

      %points.matrixSize = matrixSize;

      %extract all
      for i=1:javaOperatingPoints.length

        javaWave = javaMethod('get', javaMap, ...
          javaMethod('getName',javaOperatingPoints(i)));

        javaUnwrappedWave = javaMethod('unwrap', javaWave);

        unwrappedWave = ExtractionEngine.handleJavaBigDecimalArray(...
          javaMethod('getWave', javaUnwrappedWave));
        
        operatingPoint = ExtractionEngine.handleJavaString(...
          javaMethod('getName',javaOperatingPoints(i)));

        y = reshape(unwrappedWave, flip(matrixSize));

        points.(operatingPoint) = permute(y, flip(1:numel(size(y))));
      end
    end
  end

  methods (Static, Access=public)
    function obj = init(varargin)
      % INIT Create a new handle to a PREDICT database
      %  obj=PrimitiveDecive.init(<path>) creates a handle to the 
      %  a PREDICT database by providing the path top the database as 
      %  character array when available, [] otherwise.
      %
      %  Example: 
      %  >> obj = obj=PrimitiveDecive.init('~/db/nmos)
      % 

      obj = [];

      if numel(varargin) == 1 && ischar(varargin{1})

        jEnv = javaMethod('build', ...
                'edlab.eda.predict.model.PredictEnvironment', ...
                javaObject('java.lang.String',varargin{1}));

        if isjava(jEnv) && ...
          javaMethod('isInstanceOf',...
              'edlab.eda.predict.model.PredictEnvironment', jEnv)
          %call private constructor of this class when jEnv valid
          obj = ExtractionEngine(jEnv);
        else
          error('Provided parameter ''path'' does not point to a directory');
        end
      elseif numel(varargin) == 1 && javaMethod('isInstanceOf',...
         'edlab.eda.predict.model.PredictEnvironment', varargin{1})
        obj = ExtractionEngine(varargin{1});
      elseif numel(varargin) == 2 && javaMethod('isInstanceOf',...
         'edlab.eda.predict.model.PredictEnvironment', varargin{1}) && ...
         javaMethod('isInstanceOf',...
         'edlab.eda.predict.extraction.ExtractionEngine', varargin{2})
        obj = ExtractionEngine(varargin{1}, varargin{2});
      end
    end
  end
  
  methods (Static, Access=private)
    function retval = handleJavaString(javaString)
      % HANDLEJAVASTRING This function transforms a Java String into 
      %                  a MATLAB / Octave character array
      if exist("OCTAVE_VERSION", "builtin") > 0
        retval = javaMethod('toString',javaString);
      else
        retval = transpose(javaMethod('toCharArray', javaString));
      end
    end

    function values = handleJavaBigDecimalArray(javaArray)
      % HANDLEJAVABIGDECIMALVECTOR This function transforms a Java 
      %                   BigDecimal Array into a MATLAB / Octave vector

      values = zeros(1,javaArray.length);

      for i=1:javaArray.length
        values(i) = javaMethod('doubleValue', javaArray(i));
      end
    end

    function values = handleJavaStringArray(javaArray)
      % HANDLEJAVASTRINGVECTOR This function transforms a Java 
      %                   String Array into a MATLAB / Octave vector

      values = cell(1,javaArray.length);

      for i=1:javaArray.length
        values{i} = ExtractionEngine.handleJavaString(javaArray(i));
      end
    end
  end
end