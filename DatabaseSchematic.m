classdef DatabaseSchematic < DatabaseStructural
  % DATABASESCHEMATIC Reference of a database schematic view
  
  methods (Access = private)
    function this = DatabaseSchematic(javaObj)
      this.javaObj = javaObj;
    end
  end

  methods (Access = public)

    function name = getLibName(this)
      % GETLIBNAME Get the library name
      %  schematic.GETLIBNAME() returns the library name as character array.
      %
      %  Example: 
      %  >> libName = schematic.getLibName()
      %
      %  libName =
      %
      %      'edp_dsgn'

      name = handleJavaString(javaMethod('getLibName', this.javaObj));
    end

    function name = getCellName(this)
      % GETCELLNAME Get the cell name of the schematic
      %  schematic.GETCELLNAME() returns the name of the cell as character array.
      %
      %  Example: 
      %  >> cellName = schematic.getCellName()
      %
      %  cellName =
      %
      %      'op2'

      name = handleJavaString(javaMethod('getCellName', this.javaObj));
    end

    function name = getViewName(this)
      % GETVIEWNAME Get the view name of the schematic
      %  schematic.GETVIEWNAME() returns the name of the view as character array.
      %
      %  Example: 
      %  >> viewName = schematic.getViewName()
      %
      %  cellName =
      %
      %      'schematic'

      name = handleJavaString(javaMethod('getViewName', this.javaObj));
    end

    function cell = getCell(this)
      % GETCELL Get the cell where the DatabaseSchematic is stored
      %  schematic.GETCELL() returns cell where the DatabaseSchematic is stored.
      %
      %  Example: 
      %  >> cell = schematic.getCell()
      %
      % See also DatabaseCell.   

      cell = DatabaseCell.init(javaMethod('getCell', this.javaObj));
    end

    function tf = backannotate(this)
      % BACKANNOTATE Backannotate the devices to the design environemnt
      %
      %  Example: 
      %  >> tf = schematic.backannotate()
      % 

      tf = javaMethod('backannotate', this.javaObj);
    end

    function tf = save(this)
      % SAVE Backannotate the devices to the design environemnt
      %
      %  Example: 
      %  >> tf = schematic.backannotate()
      % 

      tf = javaMethod('backannotate', this.javaObj);
    end

    function instance = getInstance(this, name)
      % GETINSTANCE Get a handle to an instance in the schematic.
      %  schematic.GETVIEWNAME(name) returns a handle to the instance, when
      %  name (character array) is 
      %
      %  Parameter:
      %  
      %  Example: 
      %  >> instance = schematic.getInstance()

      instance = [];

      javaInstance = javaMethod('getInstance', this.javaObj, name);

      if ~isempty(javaInstance)
        instance = DatabaseInstance.init(javaInstance);
      end
    end

    function retval = show(this)
      % SHOW Open the schematic in Virtuoso
      retval = javaMethod('show', this.javaObj);
    end
  end

  methods (Static)
    function obj = init(javaObj)
      %INIT Do not call this function by yourself.
      if  isjava(javaObj) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.core.database.DatabaseSchematic', javaObj)
        obj = DatabaseSchematic(javaObj);
      else
        obj=[];
      end
    end
  end
end