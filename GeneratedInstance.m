classdef GeneratedInstance < ExpertDesignPlanWrapper
  % GENERATEDINSTANCE Reference of an instance that in generated in 
  %  a SchematicGenerator
  %
  % See also SchematicGenerator.

  % Copyright 2023 Reutlingen University, Electronics & Drives

  methods (Access = private)
    function this = GeneratedInstance(javaObj)
      this.javaObj = javaObj;
    end
  end
    
  methods (Access = public)

    function lib = getLibraryName(this)
      %GETLIBRARYNAME Get the library name of the instance

      lib = handleJavaString(javaMethod('getLibraryName', this.javaObj));
    end

    function cell = getCellName(this)
      %GETCELLNAME Get the cell name of the instance

      cell = handleJavaString(javaMethod('getCellName', this.javaObj));
    end

    function view = getViewName(this)
      %GETVIEWNAME Get the view name of the instance

      view = handleJavaString(javaMethod('getViewName', this.javaObj));
    end

    function name = getName(this)
      %GETNAME Get the name of the instance

      name = handleJavaString(javaMethod('getName', this.javaObj));
    end

    function rotation = getRotation(this)
      %GETROTATION Get the rotation of the instance

      rotation = javaMethod('getRotation', this.javaObj);
    end

    function tf = isMirrorX(this)
      %ISMIRRORX Check if the instance is mirrored on the x axis

      tf = javaMethod('isMirrorX', this.javaObj);
    end

    function tf = isMirrorY(this)
      %ISMIRRORY Check if the instance is mirrored on the y axis

      tf = javaMethod('isMirrorY', this.javaObj);
    end

    function position = getPosition(this)
      %GETPOSITION Get the position of the instance

      javaPoint = javaMethod('getPosition', this.javaObj);

      position = [javaMethod('doubleValue', javaMethod('getX', javaPoint)) , ...
                  javaMethod('doubleValue', javaMethod('getY', javaPoint))];
    end    

    function point = getAnchor(this, anchor)
      %GETANCHOR Get the anchor of an instance.
      %  obj.GETANCHOR(anchor) returns the position of an anchor of the instance
      %  When no no valid anchor is provided, [] is returned.
      %
      %  Examples: obj.getAnchor('S')      => [1.0 2.0]
      %            obj.getAnchor('fuubar') => []
      %

      point = [];

      if ischar(anchor)

        if javaMethod('isAnchor', this.javaObj, anchor)

          javaPoint = javaMethod('getAnchor', this.javaObj, anchor);

          point = [ javaMethod('doubleValue', javaMethod('getX', javaPoint)), ...
                    javaMethod('doubleValue', javaMethod('getY', javaPoint))];

        else
          error('''%s'' is not an anchor');
        end
      else
        error('Provided anchor is no character array');
      end
    end

    function anchors = getAnchors(this)
      %GETANCHORS Get a cell array of all anchors of the instance.
      %
      %   Example: obj.getAnchors()  => {'D', 'G', 'S', 'B'}       
      %

      anchors = {};

      anchorsArray = javaMethod('getAnchors', this.javaObj);

      for i=1:numel(anchorsArray)
        anchors{i} = handleJavaString(anchorsArray(i));
      end
    end  


    function wire = wire(this, point, varargin)
      %WIRE Draw a wire starting from an anchor of the instance.
      %   obj.wire(point, varargin) draws a wire starting from an anchor
      %   to the provided point. The point can be specified either as absolute
      %   or incremental value.
      %
      %   Optional Keyword Parameters: 
      %    - name  : label as character array
      %    - anchor: anchor (character arrow) where the wire starts
      %    - inc   : can be either 
      %                   false   : utilize absolute coordinate system
      %                             (point is interpreted as absolute value)
      %                   true    : utilize incremental coordinate system
      %                            (point is interpreted as increment 
      %                             wrt. to the anchor)
      %    - style : can be either 
      %                   '-',    : no solder dots at the end
      %                   '-*'    : solder dot at the end
      %    - route : can be either 
      %                   '--',   : direct route between anchor and point
      %                   '-|'    : draw a perpendicular route from the anchor
      %                             to the point, starting with a horizontal
      %                             wire, followed by a vertical wire
      %                   '|-'    : draw a perpendicular route from the anchor
      %                             to the point, starting with a vertical
      %                             wire, followed by a horizontal wire
      %                   '-'     : draws a horizontal route starting at the 
      %                             anchor to the intersection of a horizontal
      %                             line crossing the anchor and a vertical line
      %                             crossing the point
      %                   '|'     : draws a vertical route starting at the 
      %                             anchor to the intersection of a vertical
      %                             line crossing the anchor and a vertical line
      %                             crossing the point
      %    - ratio : (0 1.0] draw the route only to a certain percentage
      %                      i.e. 0.5 means that only half of the route between
      %                      anchor and point is drawn
      %

      p = inputParser();

      expectedStyles = {'-', '-*'};
      expectedRoutes = {'--', '-', '|','-|', '|-'};

      javaAnchorsArray = javaMethod('getAnchors', this.javaObj);

      addParameter(p,'anchor', handleJavaString(javaAnchorsArray(1)));

      addRequired(p,'point', ...
                    @(x) numel(x)==2 && isreal(x(1)) && isreal(x(2)));

      addParameter(p,'ratio',1.0 ,...
                     @(x) isreal(x) && x>0 && x<=1.0);

      addParameter(p,'name',[], @ischar);

      addParameter(p,'style',expectedStyles{1} ,...
                     @(x) any(validatestring(x,expectedStyles)));

      addParameter(p,'route',expectedRoutes{1} ,...
                     @(x) any(validatestring(x,expectedRoutes)));

      addParameter(p,'inc', true);
      addParameter(p,'draw', false);

      parse(p, point, varargin{:});

      startSolderDot = false;
      endSolderDot = false;

      if strcmp(p.Results.style,'*-')
        startSolderDot = true;
      elseif strcmp(p.Results.style,'-*')
        endSolderDot = true;
      elseif strcmp(p.Results.style,'*-*')
        startSolderDot = true;
        endSolderDot = true;
      end

      javaPoint = javaObject('edlab.eda.goethe.Point', ...
                              p.Results.point(1), ...
                              p.Results.point(2));

      javaRatio = javaObject('java.math.BigDecimal', p.Results.ratio);

      if strcmp(p.Results.route,'--')
        method = 'draw';
      elseif strcmp(p.Results.route,'-')
        method = 'drawHorizontal';
      elseif strcmp(p.Results.route,'|')
        method = 'drawVertical';
      elseif strcmp(p.Results.route,'-|')
        method = 'drawHorizontalVertical';
      else
        method = 'drawVerticalHorizontal';
      end

      wireJavaObj = javaMethod(method, ...
                      this.javaObj, ...
                      p.Results.anchor,...
                      javaPoint, ...
                      javaRatio, ...
                      p.Results.name, ...
                      p.Results.inc, ...
                      startSolderDot, ...
                      endSolderDot, ...
                      true);

      wire = GeneratedWire.init(wireJavaObj);
      
    end
  end
    
  methods (Static)

    function obj = init(javaObj)
      %INIT Do not call this function by yourself.
      if  isjava(javaObj) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.core.schgen.GeneratedInstance', javaObj)

        obj = GeneratedInstance(javaObj);
      else
        obj=[];
      end
    end
  end
end