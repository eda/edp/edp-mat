classdef GeneratedPin < ExpertDesignPlanWrapper
  %GENERATEDPIN Reference of an pin that in generated in 
  %   a SchematicGenerator

  % Copyright 2023 Reutlingen University, Electronics & Drives

  methods (Access = private)
    function this = GeneratedPin(javaObj)
      this.javaObj = javaObj;
    end
  end

  methods (Access = public)

    function name = getName(this)
      % GETNAME Get the name of the pin

      name = handleJavaString(javaMethod('getName', this.javaObj));
    end

    function rotation = getRotation(this)
      %GETROTATION Get the rotation of the pin

      rotation = javaMethod('getRotation', this.javaObj);
    end

    function tf = isMirrorX(this)
      %ISMIRRORX Check if the pin is mirrored on the x axis

      tf = javaMethod('isMirrorX', this.javaObj);
    end

    function tf = isMirrorY(this)
      %ISMIRRORY Check if the pin is mirrored on the y axis

      tf = javaMethod('isMirrorY', this.javaObj);
    end

    function direction = getDirection(this)
      % GETDIRECTION Get the direction of the pin

      direction = handleJavaString(javaMethod('getDirection', this.javaObj));
    end

    function position = getPosition(this)
      % GETPOSITION Get the position of the pin

      javaPoint = javaMethod('getPosition', this.javaObj);

      position = [javaMethod('doubleValue', javaMethod('getX', javaPoint)) , ...
                  javaMethod('doubleValue', javaMethod('getY', javaPoint))];
    end

    function wire = wire(this, point, varargin)
      % WIRE Draw a wire starting from a pin
      %   obj.wire(point,varargin) draws a wire starting from the pin
      %   to the provided point. The point can be specified eiter as abolute or
      %   incremental value.
      %
      %   Optional Parameters: 
      %    - name  : label as character array
      %    - inc   : can be either 
      %                   false   : utilize absolute coordinate system
      %                             (point is interpreted as absolute value)
      %                   true    : utilize incremental coordinate system
      %                            (point is interpreted as increment 
      %                             wrt. to the anchor)
      %    - style : can be either 
      %                   '-',    : no solder dots at the end
      %                   '-*'    : solder dot at the end
      %    - route : can be either 
      %                   '--',   : direct route between anchor and point
      %                   '-|'    : draw a perpendicular route from the anchor
      %                             to the point, starting with a horizontal
      %                             wire, followed by a vertical wire
      %                   '|-'    : draw a perpendicular route from the anchor
      %                             to the point, starting with a vertical
      %                             wire, followed by a horizontal wire
      %                   '-'     : draws a horizontal route starting at the 
      %                             anchor to the intersection of a horizontal
      %                             line crossing the anchor and a vertical line
      %                             crossing the point
      %                   '|'     : draws a vertical route starting at the 
      %                             anchor to the intersection of a vertical
      %                             line crossing the anchor and a vertical line
      %                             crossing the point
      %    - ratio : (0 1.0] draw the route only to a certain percentage
      %                      i.e. 0.5 means that only half of the route between
      %                      anchor and point is drawn
      %

      p = inputParser();

      expectedStyles = {'-', '-*'};
      expectedRoutes = {'--', '-', '|','-|', '|-'};

      addRequired(p,'point', ...
                    @(x) numel(x)==2 && isnumeric(x(1)) && isnumeric(x(2)));

      addParameter(p,'ratio',1.0 ,...
                     @(x) isnumeric(x) && x>0 && x<=1.0);

      addParameter(p,'name',[], @ischar);

      addParameter(p,'style',expectedStyles{1} ,...
                     @(x) any(validatestring(x,expectedStyles)));

      addParameter(p,'route',expectedRoutes{1} ,...
                     @(x) any(validatestring(x,expectedRoutes)));

      addParameter(p,'inc', true);
      addParameter(p,'draw', false);

      parse(p, point, varargin{:});

      startSolderDot = false;
      endSolderDot = false;

      if strcmp(p.Results.style,'*-')
        startSolderDot = true;
      elseif strcmp(p.Results.style,'-*')
        endSolderDot = true;
      elseif strcmp(p.Results.style,'*-*')
        startSolderDot = true;
        endSolderDot = true;
      end

      javaPoint = javaObject('edlab.eda.goethe.Point', ...
                              p.Results.point(1), ...
                              p.Results.point(2));

      javaRatio = javaObject('java.math.BigDecimal', p.Results.ratio);

      if strcmp(p.Results.route,'--')
        method = 'draw';
      elseif strcmp(p.Results.route,'-')
        method = 'drawHorizontal';
      elseif strcmp(p.Results.route,'|')
        method = 'drawVertical';
      elseif strcmp(p.Results.route,'-|')
        method = 'drawHorizontalVertical';
      else
        method = 'drawVerticalHorizontal';
      end
      
      wireJavaObj = javaMethod(method, ...
                      this.javaObj, ...
                      javaPoint, ...
                      javaRatio, ...
                      p.Results.inc, ...
                      startSolderDot, ...
                      endSolderDot, ...
                      true);

      wire = GeneratedWire.init(wireJavaObj);
      
    end
  end

  methods (Static)

    function obj = init(javaObj)
      % INIT Do not call this function by yourself.
      if  isjava(javaObj) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.core.schgen.GeneratedPin', javaObj)
        
        obj = GeneratedPin(javaObj);
      else
        obj=[];
      end
    end
  end
end