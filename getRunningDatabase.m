function db = getRunningDatabase()
%GETRUNNINGDATABSE Get the running EDP database
  db = Database.getRunningDatabase();
end