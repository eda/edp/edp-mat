function transformSizingCallback(e,fun)
  javaSizingCall = javaMethod('getSource', e);

  javaInstance = javaMethod('getInstance', javaSizingCall);
  javaParameters = javaMethod('getParameters', javaSizingCall); 
  javaReturn = javaMethod('getSizingReturn', javaSizingCall); 

  inst = DatabaseInstance.init(javaInstance);

  parameters = cell(1, numel(javaParameters));

  for i = 1:numel(parameters)

    if javaMethod('isInstanceOf', ...
        'edlab.eda.edp.core.database.sizing.NumberSizingParameter', ...
        javaParameters(i))
      parameters{i} = javaMethod('doubleValue', ...
        javaMethod('getValue', javaParameters(i)));
      
    elseif javaMethod('isInstanceOf', ...
        'edlab.eda.edp.core.database.sizing.StringSizingParameter', ...
        javaParameters(i))
      parameters{i} = handleJavaString(...
        javaMethod('getValue', javaParameters(i)));
    end
  end
  
  try
    fun(inst, parameters{:});
  catch ME
    disp('Evaluation of sizing function failed');
  end

  javaMethod('setFinished', javaReturn ,true);
end