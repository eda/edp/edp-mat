classdef SymbolMaster < ExpertDesignPlanWrapper
  % SYMBOLMASTER Master representation of a symbol in a schematic generator 
  %
  % See also SchematicGenerator.

  % Copyright 2023 Reutlingen University, Electronics & Drives
  
  methods (Access = private)
    function this = SymbolMaster(javaObj)
      this.javaObj = javaObj;
    end
  end
    
  methods (Access = public)
  
    function lib = getLibraryName(this)
      %GETLIBRARAYNAME Get the library name of the symbol
      
      lib = handleJavaString(javaMethod('getLibraryName', this.javaObj));
    end

    function cell = getCellName(this)
      %GETCELLNAME Get the cell name of the symbol

      cell = handleJavaString(javaMethod('getCellName', this.javaObj));
    end

    function view = getViewName(this)
      %GETVIEWNAME Get the view name of the symbol

      view = handleJavaString(javaMethod('getViewName', this.javaObj));
    end 

    function terminals = getTerminals(this)
      % GETTERMINALS Get a cell of all terminals of the symbol
      %
      %   Example: obj.getTerminals() 
      %            => {'D', 'G', 'S', 'B'}
      %

      terminalsArray = javaMethod('getTerminals', this.javaObj);

      terminals = cell(1,numel(terminalsArray));

      for i=1:numel(terminalsArray)
        terminals{i} = handleJavaString(terminalsArray(i));
      end
    end  

    function tf = isTerminal(this, terminal)
      % ISTERMINAL Check if a symbol has a terminal with a given name
      %
      %  Example: obj.isTerminal('D') 
      %           => 1
      %

      tf = false;

      if ischar(terminal)
        tf = javaMethod('isTerminal', this.javaObj, terminal);
      end
    end

    function point = getPosition(this, terminal)
      % GETPOSITION Get the position of a terminal
      %
      %  Example: obj.getPosition('D') 
      %           => [0 1]
      %
      point = [];

      if this.isTerminal(terminal)

        javaPoint = javaMethod('getPosition', this.javaObj, terminal);

        point = [ javaMethod('doubleValue', javaMethod('getX', javaPoint)), ...
                  javaMethod('doubleValue', javaMethod('getY', javaPoint))];
      end
    end
  end

  methods (Static)

    function obj = init(javaObj)
      %INIT Do not call this function by yourself.
      
      if  isjava(javaObj) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.core.schgen.SymbolMaster', javaObj)

        obj = SymbolMaster(javaObj);
      else
        obj=[];
      end
    end
  end
end