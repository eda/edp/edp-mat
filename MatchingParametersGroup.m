classdef MatchingParametersGroup < ExpertDesignPlanWrapper
  %PARAMETER Reference of a Parameter Group in the Expert Design Plan
  
  methods (Access = public)
    function this = MatchingParametersGroup(varargin)
      if numel(varargin) == 1 && javaMethod('isInstanceOf',...
        'edlab.eda.edp.core.database.MatchingParametersGroup', varargin{1})
        this.javaObj = varargin{1};
      else
        this.javaObj = ...
          javaObject('edlab.eda.edp.core.database.MatchingParametersGroup');
      end
    end
  end

  methods (Access = public)

    function retval = set(this, value)
      % SET  Set the value of matching group

      retval = [];

      javaValue = [];

      if numel(value)==1 && isnumeric(value)

        javaValue = javaMethod('set', ...
                                this.javaObj, ...
                                javaObject('java.math.BigDecimal', value));

      elseif ischar(value)
        javaValue = javaMethod('set', ...
                               this.javaObj, ...
                               javaObject('java.lang.String', value));
      end

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      elseif isa(javaValue, 'java.lang.String')
        retval = handleJavaString(javaValue);
      end
    end

    function retval = get(this)
      % GET  Get the associated value

      retval = [];

      javaValue = javaMethod('get', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      elseif isa(javaValue, 'java.lang.String')
        retval = handleJavaString(javaValue);
      end
    end

    function retval = getParameters(this)
      % GETPARAMETERS  Get a cell array of all parameters
      %  group.GETPARAMETERS() returns a cell array of all parameters that
      %  are associated with this group

      javaParametersGroup = javaMethod('getParametersAsArray', this.javaObj);

      retval = cell(1, numel(javaParametersGroup));

      for i = 1:numel(retval)
        retval{i} = Parameters.init(javaParametersGroup(i));
      end
    end

    function tf = addParameter(this, parameter)
      % ADDPARAMETER  Add a parameter to the group
      %  group.ADDPARAMETER(param) adds a parameter to the group.
      %  The method returns true when successful, false otherwise.
      %

      if isa(parameter, 'Parameter')
        tf = javaMethod('addParameter', this.javaObj, parameter.javaObj);
      else
        error('No valid parameter is provided')
      end
    end

    function tf = removeParameter(this, parameter)
      % REMOVEPARAMETER  Remove a parameter from the group
      %  group.REMOVEPARAMETER(param) removes a parameter from the group.
      %  The method returns true when successful, false otherwise.
      %

      if isa(parameter, 'Parameter')
        tf = javaMethod('removeParameter', this.javaObj, parameter.javaObj);
      else
        error('No valid parameter is provided')
      end
    end

    function tf = clear(this)
      % CLEAR  Clear the matching group
      tf = javaMethod('clear', this.javaObj);
    end
  end

  methods (Static)
    function obj = init(javaObj)
      %INIT Do not call this function by yourself.
      if  isjava(javaObj) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.core.database.MatchingParametersGroup', javaObj)
        obj = MatchingParametersGroup(javaObj);
      else
        obj=[];
      end
    end
  end
end