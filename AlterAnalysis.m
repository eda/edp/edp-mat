classdef AlterAnalysis < Analysis
  % ALTERANALYSIS 
  
  methods (Access = private)
    function this = AlterAnalysis(javaObj)
      this.javaObj = javaObj;
    end
  end

  methods (Access = public)

    function parameter = getParameter(this)
      % GETPARAMETER Get the parameter that is altered
      %  analysis.GETPARAMETER() returns the parameter that is altered.
      %
      % See also DatabaseParameter. 

      javaParameter = javaMethod('getParameter', this.javaObj);

      parameter = DatabaseParameter.init(javaParameter);
    end

    function retval = setParameter(this, parameter)
      % SETPARAMETER Specify the parameter to be altered
      %  analysis.SETPARAMETER(param) sets the parameter to be varied.
      %  The method returns the parameter when successful, [] otherwise.
      %
      % See also DatabaseParameter. 

      if isa(parameter, 'DatabaseParameter')

        javaParameter = javaMethod('setParameter', this.javaObj, ...
                                parameter.javaObj);

        retval = DatabaseParameter.init(javaParameter);

      else
        error('Provided argument is not a parameter');
      end
    end

    function retval = getValue(this)
      % GETVALUES Get the altered value
      %  analysis.GETVALUE() returns the altered value
      %

      javaValue = javaMethod('getValue', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      elseif isa(javaValue, 'java.lang.String')
        retval = handleJavaString(javaValue);
      elseif isempty(javaValue)
        retval = [];
      end
    end

    function retval = setValue(this, value)
      % SETVALUE Specify the altered value
      %  analysis.SETVALUE(value) specifies the altered value.
      %  The method returns the value when valid, false otherwise.
      %

      if numel(value)==1 && isnumeric(value)

        javaValue = javaMethod('setValue', ...
          this.javaObj, javaObject('java.math.BigDecimal', value));

      elseif ischar(value)
        javaValue = javaMethod('setValue', ...
          this.javaObj,  javaObject('java.lang.String', value));
      else
        error('No new value is provided');
      end

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      elseif isa(javaValue, 'java.lang.String')
        retval = handleJavaString(javaValue);
      elseif isempty(javaValue)
        retval = [];
      end
    end
  end

  methods (Static)
    function obj = init(javaObj)
      %INIT Do not call this function by yourself.
      if  isjava(javaObj) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.simulation.analysis.AlterAnalysis', javaObj)
        obj = AlterAnalysis(javaObj);
      else
        obj=[];
      end
    end
  end
end