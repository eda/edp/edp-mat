classdef ModelFile < ExpertDesignPlanWrapper
  % MODELFILE Reference of a Model File in the Expert Design Plan

  % Copyright 2023 Reutlingen University, Electronics & Drives

  methods (Access = private)
    function this = ModelFile(javaObj)
      this.javaObj = javaObj;
    end
  end

  methods (Access = public)

    function name = getName(this)
      % GETNAME  Get the name of the model file
      name = handleJavaString(javaMethod('getName', this.javaObj));
    end

    function section = getSection(this)
      % GETSECTION  Get the name of the current active section
      javaSection = javaMethod('getSection', this.javaObj);

      if isa('java.lang.String', javaSection)
        section = handleJavaString(javaSection);
      else
        section = [];
      end
    end

    function tf = setSection(this, name)
      % SETSECTION  Set the the current active section

      if ischar(name)
        tf = javaMethod('setSection', this.javaObj, ...
          javaObject('java.lang.String', name));
      else
        error('Provided parameter it not a character array');
      end
    end

    function sections = getSections(this)
      % GETSECTIONS  Get a cell array of all sections

      javaSections = javaMethod('getSections', this.javaObj);

      sections = cell(1, numel(javaSections));

      for i=1:numel(javaSections)
        sections{i} = handleJavaString(javaSections(i));
      end
    end

  end

  methods (Static)
    function obj = init(javaObj)
      %INIT Do not call this function by yourself.
      if  isjava(javaObj) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.simulation.test.ModelFile', javaObj)
        obj = ModelFile(javaObj);
      else
        obj=[];
      end
    end
  end
end