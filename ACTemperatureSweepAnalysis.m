classdef ACTemperatureSweepAnalysis < Analysis
  %ACTEMPERATURESWEEPANALYSIS Reference of a AC temperature sweep analysis
  
  methods (Access = private)
    function this = ACTemperatureSweepAnalysis(javaObj)
      this.javaObj = javaObj;
    end
  end

  methods (Access = public)

    function values = getValues(this)
      % GETVALUES Get the sweeped temperature values as array
      %  analysis.GETVALUES() returns an array of temperatures.
      %

      javaArray = javaMethod('getValues', this.javaObj);

      values = zeros(1,numel(javaArray));

      for i=1:numel(javaArray)
        values(i) = javaMethod('doubleValue', javaArray(i));
      end
    end

    function tf = setValues(this, values)
      % SETVALUES Specify the temperature values
      %  analysis.SETVALUES(values) specifies the sweep valued, by providing
      %  the values as array.
      %  The method return true when successful, false otherwise.
      %

      if isnumeric(values)

        javaValuesArray = javaArray('java.math.BigDecimal', numel(values));

        for i=1:numel(javaValuesArray)
          javaValuesArray(i) = javaObject('java.math.BigDecimal', values(i));
        end

        tf = javaMethod('setValues', this.javaObj, javaValuesArray);
      else
        error('No array of values is provided');
      end
    end

    function frequency = getFrequency(this)
      % GETFREQUENCY Get the frequency that is used for simulation
      %  analysis.GETFREQUENCY() returns the frequency that is used
      %  for simulation

      frequency = javaMethod('doubleValue', ...
       javaMethod('getFrequency', this.javaObj));
    end

    function frequency = setFrequency(this, frequency)
      % SETFREQUENCY Specify the frequency that is used for simulation
      %  analysis.SETFREQUENCY(frequency) sets the frequency to be used.
      %  The method returns the current frequency.

      if isreal(frequency) && numel(frequency) == 1

        javaFrequency = javaMethod('setFrequency', this.javaObj, ...
                          javaObject('java.math.BigDecimal',frequency));


        if isempty(javaFrequency)
          frequency = [];
        else
          frequency = javaMethod('doubleValue', javaFrequency);
        end

      else
        error('Provided argument is not a number');
      end
    end
  end

  methods (Static)
    function obj = init(javaObj)
      %INIT Do not call this function by yourself.
      if  isjava(javaObj) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.simulation.analysis.ACTemperatureSweepAnalysis', javaObj)
        obj = ACTemperatureSweepAnalysis(javaObj);
      else
        obj=[];
      end
    end
  end
end