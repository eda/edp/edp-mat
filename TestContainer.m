classdef TestContainer < ExpertDesignPlanWrapper
  % TESTCONTAINER Reference of a TestContainer in the Expert Design Plan
  %  Container which is capable to run simulations of multiple tests in
  %  parallel
  %
  %  Example:
  %    ;create a container
  %    container = db.createTestContainer();
  %
  %    ;add a first test to the container
  %    container.addTest(test1);
  %
  %    ;add a second test to the container
  %    container.addTest(test2);
  %
  %    ;run all tests in the container
  %    container.run();
  %
  %    ;access the results of the first test in the container
  %    container.run();
  %
  % See also Test.

  % Copyright 2023 Reutlingen University, Electronics & Drives

  methods (Access = private)
    function this = TestContainer(javaObj)
      this.javaObj = javaObj;
    end
  end

  methods (Access = public)

    function db = getDatabase(this)
      % GETDATABASE Get the database where the test was created

      javaDatabase = javaMethod('getDatabase', this.javaObj);

      db = Database.initdb(javaDatabase);
    end

    function tf = addTest(this, test)
      % ADDTEST Add a Test to the container
      %  tf = container.ADDTEST(test) returns true when the test is added,
      %  false otherwise.
      %
      %  See also Test.

      if isa(test,'Test') 
        tf = javaMethod('addTest', this.javaObj, test.javaObj);
      else
        error('Provided parameter is not a test.');
      end
    end

    function tf = removeTest(this, test)
      % REMOVETEST Remove a Test from the container
      %  tf = container.REMOVETEST(test) returns true when the test is removed,
      %  false otherwise.
      %
      %  See also Test.

      if isa(test,'Test') 
        tf = javaMethod('removeTest', this.javaObj, test.javaObj);
      else
        error('Provided parameter is not a test.');
      end
    end

    function tf = run(this)
      % RUN Run all tests in the container
      %  tf = container.RUN(test) returns true when all tests are run
      %  false otherwise.
      tf = javaMethod('run', this.javaObj);
    end

    function parallel = getNoOfParallelRuns(this)
      % GETNOOFPARALLELRUNS Return the number of parallel runs
      %  parallel = container.GETNOOFPARALLELRUNS() returns the number of 
      %  parallel threads that are used during simulation
      parallel = javaMethod('getNoOfParallelRuns', this.javaObj);
    end

    function parallel = setNoOfParallelRuns(this, parallel)
      % SETNOOFPARALLELRUNS Set the number of parallel runs
      %  parallel = container.SETNOOFPARALLELRUNS(parallel) sets the number
      %  of parallel threads that are used during simulation

      if isinteger(int8(parallel)) && numel(parallel)==1
        parallel = javaMethod('setNoOfParallelRuns', ...
                              this.javaObj, int8(parallel));
      else
        error('No single integer value is provided');
      end
    end

    function tests = getTests(this)
      % GETTESTS Get all tests that are registered in the container
      %  tests = container.GETTESTS() get the number
      %  of tests in the container as a cell array

        javaTestsArrray = javaMethod('getTests', this.javaObj);

        tests = cell(numel(javaTestsArrray), 1);

        for i=1:numel(javaTestsArrray)
          tests{i} = Test.init(javaTestsArrray(i));
        end
    end

    function sdb = getResult(this, test)
      % GETRESULT Get a result from a test in the container
      %  sdb = container.GETRESULT(test) 
      %  returns a simulation databasebase which contains the
      %  simulation results
      %
      % See also SimulationResults.

      if isa(test, 'Test')
              
        javaSimulationResultsArray = javaMethod('getResult', ...
          this.javaObj, test.javaObj);

        sdb = cell(1,numel(javaSimulationResultsArray));

        for i = 1:numel(javaSimulationResultsArray)
          sdb{i} = SimulationResults.init(javaSimulationResultsArray(i));
        end

        if numel(sdb) == 1
          sdb=sdb{1};
        end
      else
        error('No valid test is provided');
      end
    end
  end

  methods (Static)
    function obj = init(javaObj)
      %INIT Do not call this function by yourself.
      if  isjava(javaObj) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.simulation.test.TestContainer', javaObj)
        obj = TestContainer(javaObj);
      else
        obj=[];
      end
    end
  end
end