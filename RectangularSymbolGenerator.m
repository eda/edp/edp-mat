classdef RectangularSymbolGenerator < ExpertDesignPlanWrapper
  % RECTANGULARSYMBOLGENERATOR Symbol Generator
  % 
  % The Symbol Generator is a utility to create a Symbol in
  % the Cadence Virtuoso Design Environment using a MATLAB or Octave 
  % script.
  % The generator is created from a database.
  %
  % See also Database.createRectSymbolGenerator.

  % Copyright 2023 Reutlingen University, Electronics & Drives

  methods (Access = private)
    function this = RectangularSymbolGenerator(javaObj)
      this.javaObj = javaObj;
    end
  end
    
  methods (Access = public)
    function lib = getLibraryName(this)
      %GETLIBRARAYNAME Get the library name of the symbol
      
      lib = handleJavaString(javaMethod('getLibraryName', this.javaObj));
    end

    function cell = getCellName(this)
      %GETCELLNAME Get the cell name of the symbol

      cell = handleJavaString(javaMethod('getCellName', this.javaObj));
    end

    function view = getViewName(this)
      %GETVIEWNAME Get the view name of the symbol

      view = handleJavaString(javaMethod('getViewName', this.javaObj));
    end 

    function retval = show(this, show)
      % SHOW Show the symbol in Virtuoso
      %  generator.SHOW(show) Specify if a symbol is shown in Virtuoso 
      %  by providing a boolean property.
      %

      if islogical(show)
        retval = javaMethod('show', this.javaObj, show);
      else
        error('Provided parameter is not logical');
      end
    end

    function width = getWidth(this)
      % GETWIDTH Get the width of the inner box of the symbol
      %  generator.GETWIDTH() Returns the width of the
      %  inner box of the symbol.
      %
      width = [];

      javaValue = javaMethod('getWidth', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        width = javaMethod('doubleValue', javaValue);
      end
    end

    function height = getHeight(this)
      % GETHEIGHT Get height width of the inner box of the symbol
      %  generator.GETHEIGHT() Returns the height of the
      %  inner box of the symbol.
      %
      height = [];

      javaValue = javaMethod('getHeight', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        height = javaMethod('doubleValue', javaValue);
      end
    end

    function stubLength = getStubLength(this)
      % GETSTUBLENGTH Get stub length of a pin
      %  generator.GETSTUBLENGTH() Returns the stub length of a
      %  a pin in the symbol.
      %
      % See also RectangularSymbolGenerator.setStubLength

      stubLength = [];

      javaValue = javaMethod('getStubLength', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        stubLength = javaMethod('doubleValue', javaValue);
      end
    end

    function retval = setStubLength(this, stubLength)
      % SETSTUBLENGTH Set the stub length of a pin
      %  generator.SETSTUBLENGTH(stubLength) Sets the stub length of a
      %  a pin an returns the set value.
      %
      % See also RectangularSymbolGenerator.getStubLength
      retval = [];

      if isreal(stubLength) && numel(stubLength)==1

        javaValue = javaMethod(...
                    'setStubLength'       , ...
                    this.javaObj          , ...
                    javaObject('java.math.BigDecimal', stubLength));

        if isa(javaValue, 'java.math.BigDecimal')
          retval = javaMethod('doubleValue', javaValue);
        end
      else
        error('Provided parameter is not real');
      end
    end

    function tf = addLabel(this, content, varargin)
      % ADDLABELS Add a label to the symbol
      %  generator.ADDPIN(content, varargin) adds a label to the
      %  symbol. The parameter name is character array and corresponds to the
      %  contents of the parameter. The methods returns true when the
      %  label is added successfully.
      %
      % Optional Keyword Parameters: 
      %    - point  : position where the label is placed,
      %               two elements array consisting of numbers
      %    - choice : label choice (character array)
      %               Options are 
      %                o 'device annotate'
      %                o 'physical label'
      %                o 'analog device annotate'
      %                o 'logical label'
      %                o 'analog instance label'   
      %                o 'analog pin annotate'  
      %                o 'instance label'   
      %                o 'pin name'  
      %                o 'split annotate'   
      %                o 'pin annotate'  
      %   - rotation : Rotation of the label, must be either integer
      %                0, 1, 2, 3. The number indicates how much by 90
      %                degrees the instance is rotated in positive direction
      %                (counter-clockwise).
      %   - mirrorX  : Reflection about x axis, logical
      %   - mirrorY  : Reflection about y axis, logical
      %   - type     : label type (character array)
      %                Options are 
      %                 o 'normalLabel'
      %                 o 'ILLabel'
      %                 o 'NLPLabel'
      %   - anchor   : anchor (character array)
      %                Options are 
      %                 o 'center'
      %                 o 'northwest'
      %                 o 'north'
      %                 o 'northeast'
      %                 o 'east'
      %                 o 'southeast'
      %                 o 'south'
      %                 o 'southwest'
      %                 o 'west'      
      %
      %   Examples: 
      %    - Draw a 'cdsName()' ILLabel
      %      gen.addLabel('cdsName()','point', [0.5 0.5], ...
      %                    'type','ILLabel','choice','analog instance label')
      %    - Draw a '[@instanceName]' NLPLabel
      %      gen.addLabel('[@instanceName]','point', [0.5 0.5], ...
      %                    'type',' NLPLabel','choice','instance label')  
      %    - Draw a '[@instanceName]' NLPLabel
      %      gen.addLabel('[@instanceName]','point', [0.5 0.5], ...
      %                    'type',' NLPLabel','choice','instance label')  

      p = inputParser;

      addRequired(p,'content', @ischar);
      addParameter(p,'point', [0 0], ...
        @(x) numel(x)==2 && isreal(x(1)) && isreal(x(2)));

      expectedChoices = {'device annotate', 'physical label', ...
        'analog device annotate','logical label', 'analog instance label',...
        'analog pin annotate','instance label', 'pin name',...
        'split annotate','pin annotate'};


      addParameter(p,'choice', expectedChoices{1}, ...
                    @(x) any(validatestring(x, expectedChoices)));

      addParameter(p,'rotation', 0, ...
                  @(x) isinteger(int8(x)) && numel(x)==1 && x>=0 && x<4);

      addParameter(p,'mirrorX', false, @(x) islogical(x) && numel(x)==1);
      addParameter(p,'mirrorY', false, @(x) islogical(x) && numel(x)==1);

      expectedTypes = {'normalLabel', 'ILLabel', 'NLPLabel'};

      addParameter(p,'type', expectedTypes{1}, ...
                    @(x) any(validatestring(x, expectedTypes)));

      expectedAnchors = {'center', 'northwest', 'north', 'northeast',...
        'east', 'southeast', 'south', 'southwest', 'west'};

      addParameter(p,'anchor', expectedAnchors{1}, ...
                    @(x) any(validatestring(x, expectedAnchors)));

      parse(p, content, varargin{:});

      javaPoint =  javaObject('edlab.eda.goethe.Point', ...
                              p.Results.point(1), ...
                              p.Results.point(2));

      tf = javaMethod(...
        'addLabel'                                        , ...
        this.javaObj                                      , ...
        javaPoint                                         , ...
        javaObject('java.lang.String', p.Results.choice)  , ...
        javaObject('java.lang.String', p.Results.content) , ...
        javaObject('java.lang.String', p.Results.anchor)  , ...        
        int8(p.Results.rotation)                          , ... 
        p.Results.mirrorX                                 , ...
        p.Results.mirrorY                                 , ...
        javaObject('java.lang.String', p.Results.type));         
    end  

    function tf = addPin(this, name, direction, edge, position)
      % ADDPIN Add a pin to the symbol
      %  generator.ADDPIN(name, direction, edge, position) adds a pin to the
      %  symbol. The parameter name is character array and corresponds to the
      %  name of the parameter.
      %
      %   The direction option is either:
      %    - input
      %    - output
      %    - inputOutput
      %
      %   The edge option is either:
      %    - top
      %    - left
      %    - right
      %    - bottom
      %
      % The parameter position is a real value and defines the position of
      % the pin (depending on the edge) relative to the lower left
      % of the inner box. 
      %

      if ischar(name)

        if ischar(direction)

          if ischar(edge)

            if numel(position)==1 && isreal(position)

            tf = javaMethod(...
                  'addPin'                                      , ...
                  this.javaObj                                  , ...
                  javaObject('java.lang.String', name)          , ...      
                  javaObject('java.lang.String', direction)     , ...       
                  javaObject('java.lang.String', edge)          , ...                  
                  javaObject('java.math.BigDecimal', position));
            else
              error('Provided parameter ''edge'' is not a character array');
            end
          else
            error('Provided parameter ''edge'' is not a character array');
          end
        else
          error('Provided parameter ''direction'' is not a character array');
        end
      else
        error('Provided parameter ''name'' is not a character array');
      end
    end

    function tf = execute(this)
      % EXECUTE the generator
      %  TF = generator.EXECUTE() Executes the generator and annotates all
      %  elements in the Virtuoso session. The method returns true when
      %  the execution was processed successfully, false otherwise.
      %
      tf = javaMethod('execute', ...
                          this.javaObj);
    end

    function tf = addLine(this, points)
      % ADDLINE Add a line to the inner box to the symbol
      %  TF = generator.ADDLINE(points) adds a line to the symbol
      %  The parameter points is a 2xN array of numbers that correspond
      %  to the points of the line
      %
      %  Example:
      %   points = [ [0.25 0.25] ; [0.5 0.25] ; [0.5 0.5]]
      %   generator.addLine(points)
      %

      if isreal(points) && ismatrix(points) && size(points, 2) == 2
        
        javaPoints = javaArray('edlab.eda.goethe.Point', size(points, 1));

        for i=1:size(points, 1)
          javaPoint = javaObject(                          ...
            'edlab.eda.goethe.Point'                       ,...
            javaObject('java.math.BigDecimal', points(i,1)) ,...
            javaObject('java.math.BigDecimal', points(i,2)));

          javaPoints(i) = javaPoint;
        end

        tf = javaMethod('addLine', this.javaObj, javaPoints);
      else
        error('Provided parameter ''name'' is not a character array');
      end
    end
  end

  methods (Static)

    function obj = init(javaObj)
      %INIT Do not call this function by yourself.
      
      if  isjava(javaObj) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.core.symgen.rect.RectangularSymbolGenerator', javaObj)

        obj = RectangularSymbolGenerator(javaObj);
      else
        obj=[];
      end
    end
  end
end