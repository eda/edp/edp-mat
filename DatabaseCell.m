classdef DatabaseCell < ExpertDesignPlanWrapper
  %DATABASECELL Reference of a database cell in the Expert Design Plan
  
  methods (Access = private)
    function this = DatabaseCell(javaObj)
      this.javaObj = javaObj;
    end
  end

  methods (Access = public)

    function name = getLibraryName(this)
      % GETLIBRARYNAME Get the library name
      %  cell.GETLIBRARYNAME() returns the library name as character array.
      %
      %  Example: 
      %  >> libName = cell.getLibraryName()
      %
      %  libName =
      %
      %      'edp_dsgn'

      name = handleJavaString(javaMethod('getLibraryName', this.javaObj));
    end

    function name = getCellName(this)
      % GETCELLNAME Get the cell name of the database cell
      %  cell.GETCELLNAME() returns the name of the cell as character array.
      %
      %  Example: 
      %  >> cellName = cell.getCellName()
      %
      %  cellName =
      %
      %      'op2'

      name = handleJavaString(javaMethod('getCellName', this.javaObj));
    end

    function pins = getPins(this)
      %GETPINS Get a all pins of the cell
      %  cell.GETPINS() returns a cell array of all pins as 
      %  character arrays.
      %
      %  Example: 
      %  >> pins = db.pins()
      %
      %  pins =
      %
      %      {'I', 'O', 'VDD', 'VSS'}


      pinsArray = javaMethod('getPins', this.javaObj);
      pins = cell(1, numel(pinsArray));

      for i=1:numel(pinsArray)
        pins{i} = handleJavaString(pinsArray(i));
      end
    end

    function switchList = getSwitchList(this)
      % GETSWITCHLIST Get the switch list of the cell
      %  cell.GETSWITCHLIST() returns a cell array of switch view names as
      %  character arrays.
      %
      %  Example: 
      %  >> switchList = cell.getSwitchList()
      %
      %  switchList =
      %
      %      {'schematic', 'veriloga'}


      switchListArray = javaMethod('getSwitchList', this.javaObj);
      switchList = cell(1, numel(switchListArray));

      for i=1:numel(switchListArray)
        switchList{i} = handleJavaString(switchListArray(i));
      end
    end

    function retval = setSwitchList(this, switchList)
      % SETSWITCHLIST Set the switch list of the cell
      %  cell.SETSWITCHLIST(switchList) sets the switch list of the instance.
      %  You must provide a cell array consisting of character arrays as
      %  parameter. The method returns the new set switch list.
      %
      %  Example: 
      %  >> switchList = cell.setSwitchList({'schematic', 'veriloga'})
      %
      %  switchList =
      %
      %      {'schematic', 'veriloga'}


      if iscellstr(switchList)

        switchListArray = javaArray('java.lang.String', numel(switchList));

        for i=1:numel(switchListArray)
          switchListArray(i) = javaObject('java.lang.String', switchList{i});
        end

        switchListArray = javaMethod('setSwitchList', ...
          this.javaObj, switchListArray);

        retval = cell(1, numel(switchListArray));

        for i=1:numel(switchListArray)
          retval{i} = handleJavaString(switchListArray(i));
        end

      else
        error('Provided parameter is not a cell array of character arrays');
      end
    end

    function viewNames = getViewNames(this)
      % GETVIEWNAMES Get all view names in the cell
      %  cell.GETVIEWNAMES() returns a cell array of all view names as 
      %  character arrays.
      %
      %  Example: 
      %  >> viewNames = db.getViewNames()
      %
      %  viewNames =
      %
      %      {'schematic'}

      viewNamesArray = javaMethod('getViewNames', this.javaObj);
      viewNames = cell(1, numel(viewNamesArray));

      for i=1:numel(viewNamesArray)
        viewNames{i} = handleJavaString(viewNamesArray(i));
      end
    end

    function view = getView(this, viewName)
      % GETVIEW Get a view of a cell

      view = [];

      javaSwitchListArray = javaArray('java.lang.String', 0);

      if ischar(viewName)

         javaSwitchListArray = javaArray('java.lang.String',1);
         javaSwitchListArray(1) = javaObject('java.lang.String', viewName);

      elseif iscell(viewName)

        javaSwitchListArray = javaArray('java.lang.String', numel(viewName));

        for i=1:numel(viewName)

          if ischar(viewName(i))
            javaSwitchListArray(i) = javaObject('java.lang.String', viewName(i));
          else
            javaSwitchListArray = javaArray('java.lang.String', 0);
            break;
          end
        end
      end

      javaViewObj = javaMethod('getView', ...
        this.javaObj, javaSwitchListArray);

      if ~isempty(javaViewObj)
        view = DatabaseSchematic.init(javaViewObj);
      end
    end
  end

  methods (Static)
    function obj = init(javaObj)
      %INIT Do not call this function by yourself.
      if  isjava(javaObj) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.core.database.DatabaseCell', javaObj)
        obj = DatabaseCell(javaObj);
      else
        obj=[];
      end
    end
  end
end