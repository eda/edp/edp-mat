function retval = handleBigDecimal(javaBigDecimal)
  % HANDLEJAVASTRING This function transforms a Java String into 
  %                  a MATLAB / Octave character array.

  if isa(javaBigDecimal, 'java.math.BigDecimal')
    if isOctave()
      retval = javaMethod('toString', javaString);
    else
      retval = transpose(javaMethod('toCharArray', javaString));
    end
  else
    retval = [];
  end
end