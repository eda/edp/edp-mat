classdef SchematicGenerator < ExpertDesignPlanWrapper
  % SCHEMATICGENERATOR SchematicGenerator
  %
  % The Schematic Generator is a utility to create a Schematic in
  % the Cadence Virtuoso Design Environment using a MATLAB or Octave 
  % script.
  % The generator is created from a database.
  %
  % See also Database.createSchematicGenerator.

  % Copyright 2023 Reutlingen University, Electronics & Drives

  methods (Access = private)
    function this = SchematicGenerator(javaObj)
      this.javaObj = javaObj;
    end
  end
    
  methods (Access = public)

    function master = getSymbolMaster(this, lib, cell, varargin)
      % GETSYMBOLMASTER Get the master view of symbol
      % generator.GETSYMBOLMASTER(this, lib, cell, view) returns a handle
      % to the symbol.
      % When 'view' is not provided, 'symbol' is is used as default.
      % Before an instance can be placed in a schematic, first
      % a reference of the symbol must be extracted from the design
      % environment.
      % Provide lib/cell of the symbol to this command and a valid
      % handle to the symbol is returned.
      % When no corresponding symbol is specified in Virtuoso, []
      % is returned.
      %   
      % Example: 
      %  nmos = generator.getSymbolMaster('PRIMLIB','nmos')
      %
      %  pmos = generator.getSymbolMaster('PRIMLIB','nmos','symbol')
      %   
      % See also SymbolMaster.

      if numel(varargin)==0 && ischar(lib) && ischar(cell)
        masterJavaObj = javaMethod('getSymbolMaster', this.javaObj, ...
                                   javaObject('java.lang.String', lib),...
                                   javaObject('java.lang.String', cell));
        master = SymbolMaster.init(masterJavaObj);

      elseif numel(varargin)==1 && ischar(lib) && ...
        ischar(cell) && ischar(varargin{1})

        masterJavaObj = javaMethod('getSymbolMaster', this.javaObj, ...
                                   javaObject('java.lang.String', lib),...
                                   javaObject('java.lang.String', cell),...
                                   javaObject('java.lang.String', varargin{1}));
        master = SymbolMaster.init(masterJavaObj);
      else
        error('Provided parameters are invalid');
      end
    end

    function pin = pin(this, name, varargin)
      % PIN Draw a pin at the end point of  a wire 
      %  obj.pin(name,varargin) draws a pin in a schematic;
      %  with the provided name.
      %
      %  Keyword Parameters: 
      %   - name        : Name of the instance, e.g. M0, R1, etc.
      %   - position    : Position where the instance is placed, e.g. [1 0]    
      %   - direction   : Direction of the pin, must be either
      %                   'inputOutput', 'input' or  'output'.
      %   - rotation    : Rotation of the symbol, must be either integer
      %                   0, 1, 2, 3. The number indicates how much by 90
      %                   degrees the pin is rotated in positive direction
      %                   (counter-clockwise).
      %   - mirrorX    : Reflection about x axis, logical
      %   - mirrorY    : Reflection about y axis, logical      
      %
      % See also GeneratedPin.

      expectedDirections = {'inputOutput', 'input', 'output'};

      p = inputParser;

      addRequired(p,'name', @ischar);

      addParameter(p,'rotation', 0, ...
                  @(x) isinteger(int8(x)) && numel(x)==1 && x>=0 && x<4);

      addParameter(p,'mirrorX', false, @(x) islogical(x) && numel(x)==1);

      addParameter(p,'mirrorY', false, @(x) islogical(x) && numel(x)==1);

      addParameter(p,'direction', expectedDirections{1} ,...
                     @(x) any(validatestring(x,expectedDirections)));

      addParameter(p,'position',[0 0] ,...
                   @(x) numel(x)==2 && isreal(x(1)) && isreal(x(2)));

      parse(p, name, varargin{:});

      pinJavaObj = ...
        javaMethod('addPin', ...
          this.javaObj, ...
          javaObject('edlab.eda.goethe.Point', ...
            p.Results.position(1), p.Results.position(2)),...
          javaObject('java.lang.String', p.Results.name), ...
          int8(p.Results.rotation), ...
          p.Results.mirrorX, ...
          p.Results.mirrorY, ...          
          javaObject('java.lang.String', p.Results.direction));

      pin = GeneratedPin.init(pinJavaObj);
    end


    function inst = inst(this, master, varargin)
      % INST Place an instance in the schematic.
      %  obj.INST(master) places a symbol in the schematic for the provided 
      %  master. The method returns a handle to the instance.
      %   
      %  Optional Keyword Parameters: 
      %   - name        : Name of the instance, e.g. M0, R1, etc.
      %   - position    : Position where the instance is placed, e.g. [1 0]    
      %   - anchor      : Anchor of the symbol where the symbol is placed.
      %                    Must correspond to an anchor of the symbol
      %   - rotation    : Rotation of the symbol, must be either integer
      %                   0, 1, 2, 3. The number indicates how much by 90
      %                   degrees the instance is rotated in positive direction
      %                   (counter-clockwise).
      %   - mirrorX    : Reflection about x axis, logical
      %   - mirrorY    : Reflection about y axis, logical      
      %
      %  Example: 
      %    nmos = generator.getSymbolMaster('PRIMLIB','nmos','symbol')
      %    mnd11 = generator.inst(nmos,'point',[-1 0]);
      %    mnd12 = generator.inst(nmos,'rotation',2 ,'point',[1 0]);
      %
      % See also GeneratedInstance.
      
      if isa(master, 'SymbolMaster')

        expectedAnchors = master.getTerminals();

        p = inputParser;

        addRequired(p,'master');

        addParameter(p,'anchor',expectedAnchors{1}, ...
                       @(x) any(validatestring(x, expectedAnchors)));

        addParameter(p,'name',[], @ischar);

        addParameter(p,'position', [0 0] ,...
                       @(x) numel(x)==2 && isreal(x(1)) && isreal(x(2)));

        addParameter(p,'rotation', 0, ...
                  @(x) isinteger(int8(x)) && numel(x)==1 && x>=0 && x<4);

        addParameter(p,'mirrorX', false, @(x) islogical(x) && numel(x)==1);

        addParameter(p,'mirrorY', false, @(x) islogical(x) && numel(x)==1);

        parse(p, master, varargin{:})

        javaPoint =  javaObject('edlab.eda.goethe.Point', ...
                                p.Results.position(1), ...
                                p.Results.position(2));

        if ischar(p.Results.anchor)

         instJavaObj = javaMethod('addInst', ...
                  this.javaObj, ...
                  master.javaObj, ...
                  p.Results.name, ...
                  javaPoint, ...
                  int8(p.Results.rotation), ...
                  p.Results.mirrorX, ...
                  p.Results.mirrorY, ... 
                  p.Results.anchor);
        else
         instJavaObj = javaMethod('addInst', ...
                  this.javaObj, ...
                  master.javaObj, ...
                  p.Results.name, ...
                  javaPoint, ...
                  int8(p.Results.rotation), ...
                  p.Results.mirrorX, ...
                  p.Results.mirrorY);
        end

        inst = GeneratedInstance.init(instJavaObj);
        
      else
        error('No valid master is provided');
      end
    end

    function wire = wire(this, start, stop, varargin)
      % WIRE Draw a wire from a starting position to a ending position
      %  obj.wire(start,stop, varargin) draws a wire starting from point 'start'
      %  to position 'stop'. The method returns a handle to the wire.
      %
      %  Optional Keyword Parameters: 
      %    - name  : label as character array
      %
      p = inputParser;

      addRequired(p,'start', @(x) numel(x)==2 && isreal(x(1)) && isreal(x(2)));

      addRequired(p,'stop',  @(x) numel(x)==2 && isreal(x(1)) && isreal(x(2)));   

      addParameter(p,'name', [], @ischar);

      parse(p, start, stop, varargin{:});


      javaStartPoint =  javaObject('edlab.eda.goethe.Point', ...
                                    p.Results.start(1), ...
                                    p.Results.start(2));
      javaStopPoint  =  javaObject('edlab.eda.goethe.Point', ...
                                    p.Results.stop(1), ...
                                    p.Results.stop(2));

      if ischar(p.Results.name)
        wireJavaObj = javaMethod('addWire', ...
                        this.javaObj, ...
                        javaStartPoint, ...
                        javaStopPoint, ...
                        javaObject('java.lang.String', p.Results.name));
      else
        wireJavaObj = javaMethod('addWire', ...
                        this.javaObj, ...
                        javaStartPoint, ...
                        javaStopPoint, ...
                        []);
      end

      wire = GeneratedWire.init(wireJavaObj);
    end

    function tf = execute(this)
      % EXECUTE the generator
      %  TF = generator.EXECUTE() Executes the generator and annotates all
      %  elements in the Virtuoso session. The method returns true when
      %  the execution was processed successfully, false otherwise.
      %
      tf = javaMethod('execute', ...
                          this.javaObj);
    end

    function tf = check(this)
      % CHECK the generated result
      %  TF = generator.CHECK() extracts the connectivity in the 
      %  generated schematic. The method returns true when there are no errors.
      %
      tf = javaMethod('check', ...
                          this.javaObj);
    end

    function retval = show(this, show)
      % SHOW Show the schematic in Virtuoso
      %  generator.SHOW(show) Specify if a schematic is shown in Virtuoso 
      %  by providing a boolean property.
      %

      if islogical(show)
        retval = javaMethod('show', this.javaObj, show);
      else
        error('Provided parameter is not logical');
      end
    end

    function tf = interactive(this, interactive)
      % INTERACTIVE Interactive execution of the generator
      % generator.INTERACTIVE(interactive) Specify if the generator is executed
      % interactively by providing a boolean property.
      %

      if islogical(interactive)
        tf = javaMethod('interactive', this.javaObj, interactive);
      else
        error('Provided parameter is not logical');
      end
    end

    function tf = enableAutomaticCheck(this, automaticCheck)
      % ENABLEAUTOMATICCHECK You can control with this parameter
      %  whether a check (extraction of connectivity) is always performed
      %  when the generator is executed.
      %  You can trigger extraction of connectivity manually with CHECK
      %

      if islogical(automaticCheck)
        tf = javaMethod('enableAutomaticCheck', this.javaObj, automaticCheck);
      else
        error('Provided parameter is not logical');
      end
    end

    function tf =  clear(this)
      % CLEAR Clear all components in the generator

      javaMethod('clear', this.javaObj);

      tf = true;
    end

    function tf =  setShowBoundingBox(this, lowerLeft, upperRight)
      % SETSHOWBOUNDINGBOX Set the bounding box that is used 

      if isempty(lowerLeft) &&  isempty(upperRight)
        javaBbox = javaMethod('setShowBoundingBox', this.javaObj, []);

        tf = javaMethod('isInstanceOf', 'edlab.eda.goethe.BoundingBox',javaBbox);
      else
        if numel(lowerLeft)==2 && isreal(lowerLeft(1)) && isreal(lowerLeft(2))

        javaLowerLeftPoint =  javaObject('edlab.eda.goethe.Point', ...
                                        lowerLeft(1), ...
                                        lowerLeft(2));

          if numel(upperRight)==2 && isreal(upperRight(1)) && isreal(upperRight(2))

            javaUpperRightPoint  =  javaObject('edlab.eda.goethe.Point', ...
                                               upperRight(1), ...
                                               upperRight(2));

            javaBbox = javaMethod('setShowBoundingBox', this.javaObj, ...
                               javaLowerLeftPoint, javaUpperRightPoint);


            tf = javaMethod('isInstanceOf','edlab.eda.goethe.BoundingBox',javaBbox);

          else
            error('Provided parameter "upperRight" in not a point');
          end
        else
          error('Provided parameter "lowerLeft" in not a point');
        end
      end
    end
  end

  methods (Static)

    function obj = init(javaObj)
      % INIT Do not call this function by yourself.
      if isjava(javaObj) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.core.schgen.SchematicGenerator', javaObj)
        obj = SchematicGenerator(javaObj);
      else
        obj=[];
      end
    end

    function obj = createSchematicGenerator(workingDir, evalLib, libName, cellName, viewName)
      % CREATESCHEMATICGENERATOR Create a new schematic generator
      %  SchematicGenerator.CREATESCHEMATICGENERATOR(workingDir, evalLib, libName, cellName, viewName)
      %  creates and returns a handle to a new schematic generator. 
      %  The parameters (type=character array) are:
      %    workingDir : working directory where the Skill socket is started
      %    evalLib    : library in Virtuoso where symbols can be evaluated
      %    libName    : library name of resulting schematic       
      %    cellName   : cell name of resulting schematic       
      %    viewName   : view name of resulting schematic  
      %      
      if ischar(workingDir) && ischar(evalLib) && ischar(libName) ...
         && ischar(cellName) && ischar(viewName)

        javaSchematicGenerator = ...
          javaMethod(...
          'createSchematicGenerator',...
          'edlab.eda.edp.core.direct.DirectCoreAccess' ,...
          javaObject('java.lang.String', workingDir)   ,...
          javaObject('java.lang.String', evalLib)      ,...
          javaObject('java.lang.String', libName)      ,...
          javaObject('java.lang.String', cellName)     ,...
          javaObject('java.lang.String', viewName));

        obj = SchematicGenerator.init(javaSchematicGenerator);
      else
        error('Type of provided parameters is invalid');
      end
    end
  end
end