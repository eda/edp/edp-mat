function tf = isCellArrayOfGeneratedPins(data)

  if ~iscell(data)
    tf = false;
    return;
  end
  
  for i = 1:numel(data)
    if ~isa(data{i}, GeneratedPin)
      tf = false;
      return;
    end
  end

  tf = true;

end