classdef DatabaseParameter < ExpertDesignPlanWrapper
  %DEVICEPARAMETER Reference of a DatabaseParameter in the Expert Design Plan
  
  methods (Access = private)
    function this = DatabaseParameter(javaObj)
      this.javaObj = javaObj;
    end
  end

  methods (Access = public)

    function name = getName(this)
      % GETNAME  Get the name of a parameter

      name = handleJavaString(javaMethod('getName', this.javaObj));
    end

    function instance = getInstance(this)
      % GETINSTANCE  Get the associated instance

      instance = [];

      javaInstance = javaMethod('getInstance', this.javaObj);

      if ~isempty(javaInstance)
        instance = Instance.init(javaInstance);
      end
    end

    function retval = set(this, value)
      % SET  Set the value of the parameter

      retval = [];

      javaValue = [];

      if numel(value)==1 && isnumeric(value)

        javaValue = javaMethod('set', ...
                                this.javaObj, ...
                                javaObject('java.math.BigDecimal', value));

      elseif ischar(value)
        javaValue = javaMethod('set', ...
                               this.javaObj, ...
                               value);
      elseif isempty(value)
        javaValue = javaMethod('set', ...
                               this.javaObj, ...
                               value);
      end

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      elseif isa(javaValue, 'java.lang.String')
        retval = handleJavaString(javaValue);
      end
    end

    function retval = get(this)
      % GET  Get the value of the parameter

      retval = [];

      javaValue = javaMethod('getValue', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      elseif isa(javaValue, 'java.lang.String')
        retval = handleJavaString(javaValue);
      elseif isempty(javaValue)
        retval = [];
      end
    end

    function retval = getMin(this)
      % GETMIN  Get the lower bound of the parameter

      javaValue = javaMethod('getMin', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      else
        retval = [];
      end
    end

    function retval = getMax(this)
      % GETMAX  Get the upper bound of the parameter

      javaValue = javaMethod('getMax', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      else
        retval = [];
      end
    end

    function retval = getInit(this)
      % GETINIT  Get the initial value of the parameter

      javaValue = javaMethod('getInit', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      else
        retval = [];
      end
    end

    function retval = getGrid(this)
      % GETGRID Get the grid of the parameter

      javaValue = javaMethod('getGrid', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      else
        retval = [];
      end
    end
  end

  methods (Static)
    function obj = init(javaObj)
      %INIT Do not call this function by yourself.
      if  isjava(javaObj) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.core.database.DatabaseParameter', javaObj)
        obj = DatabaseParameter(javaObj);
      else
        obj=[];
      end
    end
  end
end