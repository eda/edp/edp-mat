function db = getdb()
% GETDB Get the running EDP database
  db = Database.getRunningDatabase();
end