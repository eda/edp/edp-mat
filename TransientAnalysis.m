classdef TransientAnalysis < Analysis
  % TRANSIENTANALYSIS Transient Analysis in an Expert Design Plan
  
  methods (Access = private)
    function this = TransientAnalysis(javaObj)
      this.javaObj = javaObj;
    end
  end

  methods (Access = public)

    function retval = getStartTime(this)
      % GETSTARTTIME Get start time of the simulation
      %  analysis.GETSTARTTIME() returns the start time
      % See also TransientAnalysis.setStartTime.

      javaValue = javaMethod('getStartTime', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      else
        retval = [];
      end
    end

    function retval = setStartTime(this, value)
      % SETSTARTTIME Specify the start time of the simulation
      %  analysis.SETSTARTTIME(value) specifies the start time of the simulation
      % See also TransientAnalysis.getStartTime.

      if numel(value)==1 && isreal(value) && value >0

        javaValue = javaMethod('setStartTime', ...
          this.javaObj, javaObject('java.math.BigDecimal', value));

      elseif isempty(value)
        javaValue = javaMethod('setStartTime', this.javaObj, []);
      else
        error('No new value is provided');
      end

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      elseif isempty(javaValue)
        retval = [];
      end
    end

    function retval = getStopTime(this)
      % GETSTOPTIME Get the stop time of the simulation
      %  analysis.GETSTOPTIME() returns the stop time
      %

      javaValue = javaMethod('getStopTime', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      else
        retval = [];
      end
    end

    function retval = setStopTime(this, value)
      % SETSTOPTIME Specify the stop time of the simulation
      %  analysis.SETSTOPTIME(value) specifies the stop time of the simulation
      %

      if numel(value)==1 && isreal(value) && value >0

        javaValue = javaMethod('setStopTime', ...
          this.javaObj, javaObject('java.math.BigDecimal', value));

      else
        error('No new value is provided');
      end

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      elseif isempty(javaValue)
        retval = [];
      end
    end

    function retval = getMaxStepTime(this)
      % GETMAXSTEPTIME Get max step time of the simulation
      %  analysis.GETMAXSTEPTIME() returns the max step time
      % See also TransientAnalysis.setMaxStepTime.

      javaValue = javaMethod('getMaxStepTime', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      else
        retval = [];
      end
    end

    function retval = setMaxStepTime(this, value)
      % SETMAXSTEPTIME Specify the max step time time of the simulation
      %  analysis.SETMAXSTEPTIME(value) specifies max step time of the simulation
      % See also TransientAnalysis.getMaxStepTime.

      if numel(value)==1 && isreal(value) && value >0

        javaValue = javaMethod('setMaxStepTime', ...
          this.javaObj, javaObject('java.math.BigDecimal', value));

      elseif isempty(value)
        javaValue = javaMethod('setMaxStepTime', this.javaObj, []);
      else
        error('No new value is provided');
      end

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      elseif isempty(javaValue)
        retval = [];
      end
    end

    function retval = getStepTime(this)
      % GETSTEPTIME Get step time of the simulation
      %  analysis.GETSTEPTIME() returns the step time
      % See also TransientAnalysis.setStepTime.

      javaValue = javaMethod('getStepTime', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      else
        retval = [];
      end
    end

    function retval = setStepTime(this, value)
      % SETSTEPTIME Specify the step time time of the simulation
      %  analysis.SETSTEPTIME(value) specifies step time of the simulation
      % See also TransientAnalysis.getStepTime.

      if numel(value)==1 && isreal(value) && value >0

        javaValue = javaMethod('setStepTime', ...
          this.javaObj, javaObject('java.math.BigDecimal', value));

      elseif isempty(value)
        javaValue = javaMethod('setStepTime', this.javaObj, []);
      else
        error('No new value is provided');
      end

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      elseif isempty(javaValue)
        retval = [];
      end
    end

    function retval = getMinStepTime(this)
      % GETMINSTEPTIME Get min step time of the simulation
      %  analysis.GETMINSTEPTIME() returns the step time
      % See also TransientAnalysis.setStepTime.

      javaValue = javaMethod('getMinStepTime', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      else
        retval = [];
      end
    end

    function retval = setMinStepTime(this, value)
      % SETMINSTEPTIME Specify the min step time time of the simulation
      %  analysis.SETMINSTEPTIME(value) specifies min time of the simulation
      % See also TransientAnalysis.getMinStepTime.

      if numel(value)==1 && isreal(value) && value >0

        javaValue = javaMethod('setMinStepTime', ...
          this.javaObj, javaObject('java.math.BigDecimal', value));

      elseif isempty(value)
        javaValue = javaMethod('setMinStepTime', this.javaObj, []);
      else
        error('No new value is provided');
      end

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      elseif isempty(javaValue)
        retval = [];
      end
    end

    function retval = getIStepTime(this)
      % GETISTEPTIME Get i step time of the simulation
      %  analysis.GETISTEPTIME() returns the i step time
      % See also TransientAnalysis.setIStepTime.

      javaValue = javaMethod('getMinStepTime', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      else
        retval = [];
      end
    end

    function retval = setIStepTime(this, value)
      % SETISTEPTIME Specify the i step time time of the simulation
      %  analysis.SETISTEPTIME(value) specifies i time of the simulation
      % See also TransientAnalysis.getIStepTime.

      if numel(value)==1 && isreal(value) && value >0

        javaValue = javaMethod('setIStepTime', ...
          this.javaObj, javaObject('java.math.BigDecimal', value));

      elseif isempty(value)
        javaValue = javaMethod('setIStepTime', this.javaObj, []);
      else
        error('No new value is provided');
      end

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      elseif isempty(javaValue)
        retval = [];
      end
    end


    function retval = getNoisefmin(this)
      % GETNOISEFMIN Get minimal noise frequency in the simulation
      %  analysis.GETNOISEFMIN() returns the minimal noise frequency
      % See also TransientAnalysis.setNoisefmin.

      javaValue = javaMethod('getNoisefmin', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      else
        retval = [];
      end
    end

    function retval = setNoisefmin(this, value)
      % SETNOISEFMIN Specify the minimal noise frequency in the simulation
      %  analysis.SETNOISEFMIN(value) specifies the minimal noise frequency
      % See also TransientAnalysis.getNoisefmin.

      if numel(value)==1 && isreal(value) && value >0

        javaValue = javaMethod('setNoisefmin', ...
          this.javaObj, javaObject('java.math.BigDecimal', value));

      elseif isempty(value)
        javaValue = javaMethod('setNoisefmin', this.javaObj, []);
      else
        error('No new value is provided');
      end

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      elseif isempty(javaValue)
        retval = [];
      end
    end

    function retval = getNoisefmax(this)
      % GETNOISEFMIN Get maximal noise frequency in the simulation
      %  analysis.GETNOISEFMAX() returns the maximal noise frequency
      % See also TransientAnalysis.setNoisefmax.

      javaValue = javaMethod('getNoisefmax', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      else
        retval = [];
      end
    end

    function retval = setNoisefmax(this, value)
      % SETNOISEFMIN Specify the maximal noise frequency in the simulation
      %  analysis.SETNOISEFMAX(value) specifies the maximal noise frequency
      % See also TransientAnalysis.getNoisefmax.

      if numel(value)==1 && isreal(value) && value >0

        javaValue = javaMethod('setNoisefmax', ...
          this.javaObj, javaObject('java.math.BigDecimal', value));

      elseif isempty(value)
        javaValue = javaMethod('setNoisefmax', this.javaObj, []);
      else
        error('No new value is provided');
      end

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      elseif isempty(javaValue)
        retval = [];
      end
    end

    function retval = getNoiseseed(this)
      % GETNOISESEED Get the noiseseed
      %  analysis.GETNOISESEED() returns the noiseseed
      % See also TransientAnalysis.setNoiseseed.

      javaValue = javaMethod('getNoiseseed', this.javaObj);

      retval = javaValue;
    end

    function retval = setNoiseseed(this, value)
      % SETNOISESEED Specify the noiseseed
      %  analysis.SETNOISESEED(value) specifies the noiseseed
      % See also TransientAnalysis.getNoiseseed.

      if numel(value)==1 && isreal(value) && value >0

        javaValue = javaMethod('setNoiseseed', ...
          this.javaObj, value);

      elseif isempty(value)
        javaValue = javaMethod('setNoiseseed', this.javaObj, []);
      else
        error('No new value is provided');
      end

      retval = javaValue;
    end

    function retval = getNoisescale(this)
      % GETNOISESCALE Get noise scaling in the simulation
      %  analysis.GETNOISESCALE() returns the noise scaling
      % See also TransientAnalysis.setNoisescale.

      javaValue = javaMethod('getNoisescale', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      else
        retval = [];
      end
    end

    function retval = setNoisescale(this, value)
      % SETNOISESCALE Specify the noise scaling in the simulation
      %  analysis.SETNOISESCALE(value) specifies the noise scaling
      % See also TransientAnalysis.getNoisescale.

      if numel(value)==1 && isreal(value) && value >0

        javaValue = javaMethod('setNoisescale', ...
          this.javaObj, javaObject('java.math.BigDecimal', value));

      elseif isempty(value)
        javaValue = javaMethod('setNoisescale', this.javaObj, []);
      else
        error('No new value is provided');
      end

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      elseif isempty(javaValue)
        retval = [];
      end
    end

    function retval = getCmin(this)
      % GETCMIN Get cmin in the simulation
      %  analysis.GETCMIN() returns the cmin
      % See also TransientAnalysis.setCmin.

      javaValue = javaMethod('getCmin', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      else
        retval = [];
      end
    end

    function retval = setCmin(this, value)
      % SETCMIN Specify the cmin in the simulation
      %  analysis.SETCMIN(value) specifies the cmin
      % See also TransientAnalysis.getCmin.

      if numel(value)==1 && isreal(value) && value >0

        javaValue = javaMethod('setCmin', ...
          this.javaObj, javaObject('java.math.BigDecimal', value));

      elseif isempty(value)
        javaValue = javaMethod('setCmin', this.javaObj, []);
      else
        error('No new value is provided');
      end

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      elseif isempty(javaValue)
        retval = [];
      end
    end

    function retval = getSkipcount(this)
      % GETSKIPCOUNT Get the skipcount
      %  analysis.GETSKIPCOUNT() returns the skipcount
      % See also TransientAnalysis.setSkipcount.

      javaValue = javaMethod('getSkipcount', this.javaObj);

      retval = javaValue;
    end

    function retval = setSkipcount(this, value)
      % SETSKIPCOUNT Specify the skipcount
      %  analysis.SETSKIPCOUNT(value) specifies the skipcount
      % See also TransientAnalysis.getSkipcount.

      if numel(value)==1 && isreal(value) && value >0

        javaValue = javaMethod('setSkipcount', ...
          this.javaObj, value);

      elseif isempty(value)
        javaValue = javaMethod('setSkipcount', this.javaObj, []);
      else
        error('No new value is provided');
      end

      retval = javaValue;
    end

    function retval = getStrobeperiod(this)
      % GETSTROBEPERIOD Get strobeperiod in the simulation
      %  analysis.GETSTROBEPERIOD() returns the strobeperiod
      % See also TransientAnalysis.setStrobeperiod.

      javaValue = javaMethod('getStrobeperiod', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      else
        retval = [];
      end
    end

    function retval = setStrobeperiod(this, value)
      % SETSTROBEPERIOD Specify the strobeperiod in the simulation
      %  analysis.SETSTROBEPERIOD(value) specifies the strobeperiod
      % See also TransientAnalysis.getStrobeperiod.

      if numel(value)==1 && isreal(value) && value >0

        javaValue = javaMethod('setStrobeperiod', ...
          this.javaObj, javaObject('java.math.BigDecimal', value));

      elseif isempty(value)
        javaValue = javaMethod('setStrobeperiod', this.javaObj, []);
      else
        error('No new value is provided');
      end

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      elseif isempty(javaValue)
        retval = [];
      end
    end

    function retval = getStrobedelay(this)
      % GETSTROBEDELAY Get strobedelay in the simulation
      %  analysis.GETSTROBEDELAY() returns the strobepriod
      % See also TransientAnalysis.setStrobedelay.

      javaValue = javaMethod('getStrobedelay', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      else
        retval = [];
      end
    end

    function retval = setStrobedelay(this, value)
      % SETSTROBEDELAY Specify the strobedelay in the simulation
      %  analysis.SETSTROBEDELAY(value) specifies the strobepriod
      % See also TransientAnalysis.getStrobedelay.

      if numel(value)==1 && isreal(value) && value >0

        javaValue = javaMethod('setStrobedelay', ...
          this.javaObj, javaObject('java.math.BigDecimal', value));

      elseif isempty(value)
        javaValue = javaMethod('setStrobedelay', this.javaObj, []);
      else
        error('No new value is provided');
      end

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      elseif isempty(javaValue)
        retval = [];
      end
    end

    function retval = getStrobestop(this)
      % GETSTROBESTOP Get strobestop in the simulation
      %  analysis.GETSTROBESTOP() returns the strobepriod
      % See also TransientAnalysis.setStrobestop.

      javaValue = javaMethod('getStrobestop', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      else
        retval = [];
      end
    end

    function retval = setStrobestop(this, value)
      % SETSTROBESTOP Specify the strobestop in the simulation
      %  analysis.SETSTROBESTOP(value) specifies the strobepriod
      % See also TransientAnalysis.getStrobestop.

      if numel(value)==1 && isreal(value) && value >0

        javaValue = javaMethod('setStrobestop', ...
          this.javaObj, javaObject('java.math.BigDecimal', value));

      elseif isempty(value)
        javaValue = javaMethod('setStrobestop', this.javaObj, []);
      else
        error('No new value is provided');
      end

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      elseif isempty(javaValue)
        retval = [];
      end
    end

    function retval = getStrobestart(this)
      % GETSTROBESTART Get strobestart in the simulation
      %  analysis.GETSTROBESTART() returns the strobepriod
      % See also TransientAnalysis.setStrobestart.

      javaValue = javaMethod('getStrobestart', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      else
        retval = [];
      end
    end

    function retval = setStrobestart(this, value)
      % SETSTROBESTART Specify the strobestart in the simulation
      %  analysis.SETSTROBESTART(value) specifies the strobepriod
      % See also TransientAnalysis.getStrobestart.

      if numel(value)==1 && isreal(value) && value >0

        javaValue = javaMethod('setStrobestart', ...
          this.javaObj, javaObject('java.math.BigDecimal', value));

      elseif isempty(value)
        javaValue = javaMethod('setStrobestart', this.javaObj, []);
      else
        error('No new value is provided');
      end

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      elseif isempty(javaValue)
        retval = [];
      end
    end


    function values = getStrobetimes(this)
      % GETSTROBETIMES Get the time points for strobing
      %  analysis.GETSTROBETIMES() returns a vector of all time points 
      %  for strobing
      %
      % Example:
      %  values = analysis.getStrobetimes()
      %
      %  values =
      %
      %      [0 1 2]

      javaArray = javaMethod('getStrobetimes', this.javaObj);

      values = zeros(1, numel(javaArray));

      for i=1:numel(javaArray)
        values(i) = javaMethod('doubleValue', javaArray(i));
      end
    end

    function values = setStrobetimes(this, values)
      % SETSTROBETIMES Specify the time points for strobing
      %  analysis.SETSTROBETIMES(values) returns the ntime points for strobing
      %  One must provide an array of numbers as parameter.
      %
      % Example:
      %  values = analysis.setStrobetimes([0 1 2])
      %
      %      [0 1 2]

      if isreal(values)

        javaStrobetimesArray = javaArray('java.math.BigDecimal',...
          numel(values));

        for i=1:numel(javaStrobetimesArray)
          javaStrobetimesArray(i) = javaObject('java.math.BigDecimal', ...
            values(i));
        end

        javaStrobetimesArray = javaMethod('setStrobetimes', this.javaObj,...
          javaStrobetimesArray);

        values = zeros(1, numel(javaArray));
  
        for i=1:numel(javaArray)
          values(i) = javaMethod('doubleValue', javaStrobetimesArray(i));
        end
      else
        error('No array of values is provided');
      end
    end
  end

  methods (Static)
    function obj = init(javaObj)
      %INIT Do not call this function by yourself.
      if  isjava(javaObj) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.simulation.analysis.TransientAnalysis', javaObj)
        obj = TransientAnalysis(javaObj);
      else
        obj=[];
      end
    end
  end
end