classdef ResultsDatabase < ExpertDesignPlanWrapper
  % RESULTSDATABASE Reference of a results database
  %
  %  An object of this class is a container of simulation results of
  %  a particular analysis. A RESULTSDATABASE can be retrieved from 
  %  an object of SimulationResults.
  %
  %  Methods:
  %  get - Get a value or waveform from the database
  %
  %  See also SimulationResults.

  methods (Access = private)
    function this = ResultsDatabase(javaObj)
      this.javaObj = javaObj;
    end
  end

  methods (Access = public)

    function retval = get(this, identifier)
      %GET Get a value or waveform 
      %   analysis.GET(identifier) returns a value or waveform when
      %   available, [] otherwise.
      %   The identifier can be either a character array that provides the full 
      %   path to the result or an object of type or a Net, Terminal
      %   or OperatingPoint.
      %
      %   See also Net, Terminal, OperatingPoint.

      retval = [];
      
      if   isa(identifier,'Net') ...
        || isa(identifier,'Terminal') ...
        || isa(identifier,'OperatingPoint')

        javaIdentifier = identifier.javaObj;

      elseif ischar(identifier)
        javaIdentifier = identifier;
      else
        error('No valid identifier is provided');
      end

    javaRetval = javaMethod('get', this.javaObj, javaIdentifier);

    if javaMethod('isInstanceOf', 'edlab.eda.ardb.Value', javaRetval)
      retval = ResultsDatabase.handleJavaValue(javaRetval);
    elseif javaMethod('isInstanceOf', 'edlab.eda.ardb.RealWaveform', ...
      javaRetval)|| javaMethod('isInstanceOf', ...
      'edlab.eda.ardb.ComplexWaveform', javaRetval)
      retval = Wave.init(javaRetval);
    end
    end
  end

  methods (Static, Access=public)
    function obj = init(javaObj)
      %INIT Do not call this function by yourself.
      if  isjava(javaObj) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.ardb.ResultsDatabase', javaObj)
        obj = ResultsDatabase(javaObj);
      else
        obj=[];
      end
    end
    function value = handleJavaValue(javaValue)
      %HANDLEJAVAVALUE 
      if  isjava(javaValue) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.ardb.ComplexValue', javaValue)
        value = complex(javaMethod('real', javaValue), ...
                        javaMethod('imag', javaValue));
      elseif isjava(javaValue) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.ardb.RealValue', javaValue)
          value = javaMethod('getValue', javaValue);
      else
        value=NaN;
      end
    end
  end
end