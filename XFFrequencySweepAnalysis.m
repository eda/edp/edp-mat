classdef XFFrequencySweepAnalysis < Analysis
  % XFFREQUENCYSWEEPANALYSIS Reference of an transfer function (XF)
  %  frequency sweep analysis
  
  methods (Access = private)
    function this = XFFrequencySweepAnalysis(javaObj)
      this.javaObj = javaObj;
    end
  end

  methods (Access = public)

   function  net = getPlus(this)
      % GETPLUS Get the positive reference net 

      javaRetval = javaMethod('getPlus', this.javaObj);

      if  isjava(javaRetval) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.simulation.datastruct.Net', javaRetval)
        net = Net.init(javaRetval);
      else
        net = [];
      end
    end

    function  tf = setPlus(this, net)
      % SETPLUS Set the positive reference net

      if isa(net, 'Net')

        javaRetval = javaMethod('setPlus', this.javaObj, net.javaObj);

        if  isjava(javaRetval) && ...
            javaMethod('isInstanceOf',...
              'edlab.eda.edp.simulation.datastruct.Net', javaRetval)
          tf = true;
        else
          tf = false;
        end
      else
        error('Provided parameter is not a net');
      end
    end

    function  net = getMinus(this)
      % GETMINUS Get the negative reference net 

      javaRetval = javaMethod('getMinus', this.javaObj);

      if  isjava(javaRetval) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.simulation.datastruct.Net', javaRetval)
        net = Net.init(javaRetval);
      else
        net = [];
      end
    end

    function  tf = setMinus(this, net)
      % SETMINUS Set the negative reference net

      if isa(net, 'Net')

        javaRetval = javaMethod('setMinus', this.javaObj, net.javaObj);

        if  isjava(javaRetval) && ...
            javaMethod('isInstanceOf',...
              'edlab.eda.edp.simulation.datastruct.Net', javaRetval)
          tf = true;
        else
          tf = false;
        end
      else
        error('Provided parameter is not a net');
      end
    end

    function  inst = getProbe(this)
      % GETPROBE

      javaRetval = javaMethod('getProbe', this.javaObj);

      if  isjava(javaRetval) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.simulation.datastruct.Instance', javaRetval)
        inst = Instance.init(javaRetval);
      else
        inst = [];
      end
    end

    function  tf = setProbe(this, probe)
      % SETPROBE

      if isa(probe, 'Instance')

        javaRetval = javaMethod('setProbe', this.javaObj, probe.javaObj);

        if  isjava(javaRetval) && ...
            javaMethod('isInstanceOf',...
              'edlab.eda.edp.simulation.datastruct.Instance', javaRetval)
          tf = true;
        else
          tf = false;
        end
      else
        error('Provided parameter is not an instance');
      end
    end

    function values = getValues(this)
      % GETVALUES Get the frequencies that are used for simulation
      %  analysis.GETVALUES() returns a vector of all frequencies rhat are used
      %  for simulation
      %
      % Example:
      %  values = analysis.getValues()
      %
      %  values =
      %
      %      [1 10 100]

      javaArray = javaMethod('getValues', this.javaObj);

      values = zeros(1, numel(javaArray));

      for i=1:numel(javaArray)
        values(i) = javaMethod('doubleValue', javaArray(i));
      end
    end

    function tf = setValues(this, values)
      % SETVALUES Specify the frequencies that are used for simulation
      %  analysis.SETVALUES() returns true when the frequencies are set
      %  to the spevified values, false otherwise.
      %  One must provide an array of numbers as parameter.
      %
      % Example:
      %  analysis.setValues([1 10 100])
      %
      %
      %      true

      if isnumeric(values)

        javaValuesArray = javaArray('java.math.BigDecimal', numel(values));

        for i=1:numel(javaValuesArray)
          javaValuesArray(i) = javaObject('java.math.BigDecimal', values(i));
        end

        tf = javaMethod('setValues', this.javaObj, javaValuesArray);
      else
        error('No array of values is provided');
      end
    end
  end

  methods (Static)
    function obj = init(javaObj)
      %INIT Do not call this function by yourself.
      if  isjava(javaObj) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.simulation.analysis.XFFrequencySweepAnalysis', javaObj)
        obj = XFFrequencySweepAnalysis(javaObj);
      else
        obj=[];
      end
    end
  end
end