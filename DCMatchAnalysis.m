classdef DCMatchAnalysis < Analysis
  % DCMATCHANALYSIS
  
  methods (Access = private)
    function this = DCMatchAnalysis(javaObj)
      this.javaObj = javaObj;
    end
  end

  methods (Access = public)

    function retval = getSigma(this)
      % GETSIGMA Get the sigma that is is used for simulation
      %  analysis.GETSIGMA() returns the sigma that is used for simulation
      %

      javaValue = javaMethod('getSigma', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      else
        retval = [];
      end
    end

    function retval = setSigma(this, value)
      % SETSIGMA Specify the sigma that is is used for simulation 
      %  analysis.SETSIGMA(value) specifies the sigma that is used for simulation
      %

      if numel(value)==1 && isnumeric(value) && value > 0

        javaValue = javaMethod('setSigma', ...
          this.javaObj, javaObject('java.math.BigDecimal', value));

      else
        error('No new value is provided');
      end

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      elseif isempty(javaValue)
        retval = [];
      end
    end

    function method = getMethod(this)
      % GETMETHOD Get the method that is used for simulation
      method = handleJavaString(javaMethod('getMethod', this.javaObj));
    end

    function tf = setMethod(this, method)
      % SETMETHOD Set the method that is used for simulation
      %  analysis.setMethod(method) sets the method for the simulation
      %  The method must be either 'standard' or 'statistics'.

      if ischar(method)

        javaRetval = javaMethod('setMethod', this.javaObj, ...
          javaObject('java.lang.String', method));

        if isa(javaRetval, 'java.lang.String')
          tf = true;
        else
          tf = false;
        end
      else
        error('Provided parameter is not a character array');
      end
    end

    function variation = getVariation(this)
      % GETVARIATION Get the variation that is used for simulation
      variation = handleJavaString(javaMethod('getVariation', this.javaObj));
    end

    function tf = setVariation(this, variation)
      % SETVARIATION Set the variation that is used for simulation
      %  analysis.SETVARIATION(variation) sets the variation for the simulation
      %  The method must be either 'mismatch', 'process' or 'all'.

      if ischar(variation)

        javaRetval = javaMethod('setVariation', this.javaObj, ...
          javaObject('java.lang.String', variation));

        if isa(javaRetval, 'java.lang.String')
          tf = true;
        else
          tf = false;
        end
      else
        error('Provided parameter is not a character array');
      end
    end

    function retval = getThreshold(this)
      % GETTHREASHOLD Get the threshold that is is used for simulation
      %  analysis.GETTHREASHOLD() returns the threshold that is used for simulation
      %

      javaValue = javaMethod('getThreshold', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      else
        retval = [];
      end
    end

    function retval = setThreshold(this, value)
      % SETTHRESHOLD Specify the threshold that is is used for simulation 
      %  analysis.SETTHRESHOLD(value) specifies the threshold that is used for simulation
      %

      if numel(value)==1 && isnumeric(value) && value > 0

        javaValue = javaMethod('setThreshold', ...
          this.javaObj, javaObject('java.math.BigDecimal', value));

      else
        error('No new value is provided');
      end

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      elseif isempty(javaValue)
        retval = [];
      end
    end

    function  net = getPlus(this)
      % GETPLUS Get the positive reference net 

      javaRetval = javaMethod('getPlus', this.javaObj);

      if  isjava(javaRetval) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.simulation.datastruct.Net', javaRetval)
        net = Net.init(javaRetval);
      else
        net = [];
      end
    end

    function  tf = setPlus(this, net)
      % SETPLUS Set the positive reference net

      if isa(net, 'Net')

        javaRetval = javaMethod('setPlus', this.javaObj, net.javaObj);

        if  isjava(javaRetval) && ...
            javaMethod('isInstanceOf',...
              'edlab.eda.edp.simulation.datastruct.Net', javaRetval)
          tf = true;
        else
          tf = false;
        end
      else
        error('Provided parameter is not a net');
      end
    end

    function  net = getMinus(this)
      % GETMINUS Get the negative reference net 

      javaRetval = javaMethod('getMinus', this.javaObj);

      if  isjava(javaRetval) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.simulation.datastruct.Net', javaRetval)
        net = Net.init(javaRetval);
      else
        net = [];
      end
    end

    function  tf = setMinus(this, net)
      % SETMINUS Set the negative reference net

      if isa(net, 'Net')

        javaRetval = javaMethod('setMinus', this.javaObj, net.javaObj);

        if  isjava(javaRetval) && ...
            javaMethod('isInstanceOf',...
              'edlab.eda.edp.simulation.datastruct.Net', javaRetval)
          tf = true;
        else
          tf = false;
        end
      else
        error('Provided parameter is not a net');
      end
    end

    function  inst = getProbe(this)
      % GETPROBE

      javaRetval = javaMethod('getProbe', this.javaObj);

      if  isjava(javaRetval) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.simulation.datastruct.Instance', javaRetval)
        inst = Instance.init(javaRetval);
      else
        inst = [];
      end
    end

    function  tf = setProbe(this, probe)
      % SETPROBE

      if isa(probe, 'Instance')

        javaRetval = javaMethod('setProbe', this.javaObj, probe.javaObj);

        if  isjava(javaRetval) && ...
            javaMethod('isInstanceOf',...
              'edlab.eda.edp.simulation.datastruct.Instance', javaRetval)
          tf = true;
        else
          tf = false;
        end
      else
        error('Provided parameter is not an instance');
      end
    end
  end

  methods (Static)
    function obj = init(javaObj)
      %INIT Do not call this function by yourself.
      if  isjava(javaObj) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.simulation.analysis.DCMatchAnalysis', javaObj)
        obj = DCMatchAnalysis(javaObj);
      else
        obj=[];
      end
    end
  end
end