function retval = handleJavaString(javaString)
  % HANDLEJAVASTRING This function transforms a Java String into 
  %                  a MATLAB / Octave character array.

  if isa(javaString, 'java.lang.String')
    if isOctave()
      retval = javaMethod('toString', javaString);
    else
      retval = transpose(javaMethod('toCharArray', javaString));
    end
  else
    retval = [];
  end
end