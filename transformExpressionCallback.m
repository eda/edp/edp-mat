function transformExpressionCallback(e,fun)
  javaExpressionCall = javaMethod('getSource', e);
  javaReturn = javaMethod('getEvaluationFinished', javaExpressionCall); 
  javaResults = javaMethod('getResults', javaExpressionCall);

  sdb = SimulationResults.init(javaResults);

  try
    s = fun(sdb);
  catch ME
    s = struct;
    disp('Evaluation of expressions failed');
  end

  if isstruct(s)
    
    names=fieldnames(s);

    for i = 1:numel(names)
      value = s.(names{i});
      if isreal(value) && numel(value)==1 && ischar(names{i}) && ...
          ~isnan(value)
        javaMethod('setExpression', javaResults, ...
          javaObject('java.lang.String', names{i}),...
          javaObject('java.math.BigDecimal', value));
      end
    end
  end

  javaMethod('setFinished', javaReturn ,true);
end