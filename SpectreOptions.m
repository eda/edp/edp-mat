classdef SpectreOptions < ExpertDesignPlanWrapper
  % SPECTREOPTIONS This class handles the options for a Cadence Spectre
  %  simulation

  methods (Access = private)
    function this = SpectreOptions(javaObj)
      this.javaObj = javaObj;
    end
  end

  methods (Access = public)

    function retval = getReltol(this)
      % GETRELTOL  Get the 'reltol' option
      %   Relative convergence criterion.
      % See also SpectreOptions.setReltol

      retval = [];

      javaValue = javaMethod('getReltol', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      end
    end

    function retval = setReltol(this, reltol)
      % SETRELTOL  Set the 'reltol' option
      % See also SpectreOptions.getReltol

      retval = [];

      if isreal(reltol) && numel(reltol)==1

        javaValue = javaMethod('setReltol', ...
          this.javaObj, javaObject('java.math.BigDecimal', reltol));

        if isa(javaValue, 'java.math.BigDecimal')
          retval = javaMethod('doubleValue', javaValue);
        end        
      else
        error('Provided ''reltol'' is not a real numeric');
      end
    end

    function retval = getVabstol(this)
      % GETVABSTOL  Get the 'vabstol' option
      %   Convergence criterion for absolute voltage tolerance
      % See also SpectreOptions.setVabstol

      retval = [];

      javaValue = javaMethod('getVabstol', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      end
    end

    function retval = setVabstol(this, vabstol)
      % SETVABSTOL  Set the 'reltol' option
      % See also SpectreOptions.getVabstol

      retval = [];

      if isreal(vabstol) && numel(vabstol)==1
        
        javaValue = javaMethod('setVabstol', ...
          this.javaObj, javaObject('java.math.BigDecimal', vabstol));

        if isa(javaValue, 'java.math.BigDecimal')
          retval = javaMethod('doubleValue', javaValue);
        end
      else
        error('Provided ''vabstol'' is not a real numeric');
      end
    end

    function retval = getIabstol(this)
      % GETIABSTOL  Get the 'iabstol' option
      %   Convergence criterion for absolute current tolerance
      % See also SpectreOptions.setIabstol

      retval = [];

      javaValue = javaMethod('getIabstol', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      end
    end

    function retval = setIabstol(this, iabstol)
      % SETIABSTOL  Set the 'iabstol' option
      % See also SpectreOptions.getIabstol

      retval = [];

      if isreal(iabstol) && numel(iabstol)==1
        
        javaValue = javaMethod('setIabstol', ...
          this.javaObj, javaObject('java.math.BigDecimal', iabstol));

        if isa(javaValue, 'java.math.BigDecimal')
          retval = javaMethod('doubleValue', javaValue);
        end
      else
        error('Provided ''iabstol'' is not a real numeric');
      end
    end

    function retval = getTnom(this)
      % GETTNOM  Get the 'tnom' option
      %   Temperature in degree celcius
      % See also SpectreOptions.setTnom

      retval = [];

      javaValue = javaMethod('getTnom', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      end
    end

    function retval = setTnom(this, tnom)
      % SETTNOM  Set the 'tnom' option
      % See also SpectreOptions.getTnom

      retval = [];

      if isreal(tnom) && numel(tnom)==1
        
        javaValue = javaMethod('setTnom', ...
          this.javaObj, javaObject('java.math.BigDecimal', tnom));

        if isa(javaValue, 'java.math.BigDecimal')
          retval = javaMethod('doubleValue', javaValue);
        end
      else
        error('Provided ''tnom'' is not a real numeric');
      end
    end

    function retval = getTemp(this)
      % GETTEMP  Get the 'temp' option
      %   Temperature in degree celcius
      % See also SpectreOptions.setTemp

      retval = [];

      javaValue = javaMethod('getTemp', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      end
    end

    function retval = setTemp(this, temp)
      % SETTEMP  Set the 'temp' option
      % See also SpectreOptions.getTemp

      retval = [];

      if isreal(temp) && numel(temp)==1
        
        javaValue = javaMethod('setTemp', ...
          this.javaObj, javaObject('java.math.BigDecimal', temp));

        if isa(javaValue, 'java.math.BigDecimal')
          retval = javaMethod('doubleValue', javaValue);
        end
      else
        error('Provided ''temp'' is not a real numeric');
      end
    end

    function retval = getScale(this)
      % GETSCALE  Get the 'scale' option
      %   Device instance scaling factor
      % See also SpectreOptions.setScale

      retval = [];

      javaValue = javaMethod('getScale', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      end
    end

    function retval = setScale(this, scale)
      % SETSCALE  Set the 'scale' option
      % See also SpectreOptions.getScale

      retval = [];

      if isreal(scale) && numel(scale)==1
        
        javaValue = javaMethod('setScale', ...
          this.javaObj, javaObject('java.math.BigDecimal', scale));

        if isa(javaValue, 'java.math.BigDecimal')
          retval = javaMethod('doubleValue', javaValue);
        end
      else
        error('Provided ''scale'' is not a real numeric');
      end
    end

    function retval = getScalem(this)
      % GETSCALEM  Get the 'scalem' option
      % See also SpectreOptions.setScalem

      retval = [];

      javaValue = javaMethod('getScalem', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      end
    end

    function retval = setScalem(this, scalem)
      % SETSCALEM  Set the 'scalem' option
      % See also SpectreOptions.getScalem

      retval = [];

      if isreal(scalem) && numel(scalem)==1
        
        javaValue = javaMethod('setScalem', ...
          this.javaObj, javaObject('java.math.BigDecimal', scalem));

        if isa(javaValue, 'java.math.BigDecimal')
          retval = javaMethod('doubleValue', javaValue);
        end
      else
        error('Provided ''scalem'' is not a real numeric');
      end
    end

    function retval = getSave(this)
      % GETSAVE  Get the 'save' option
      %   The save option is either:
      %    - all
      %    - lvl
      %    - allpub
      %    - lvlpub
      %    - selected
      %    - none

      retval = [];

      javaValue = javaMethod('getSave', this.javaObj);

      if isa(javaValue, 'java.lang.String')
        retval = handleJavaString(javaValue);
      end
    end

    function retval = getHomotopy(this)
      % GETHOMOTOPY Get the 'homotopy' option
      %   The homotopy option is either:
      %    - none
      %    - gmin
      %    - source
      %    - dptran
      %    - ptran
      %    - arclength
      %    - tranrampup
      %    - all
      %
      % See also SpectreOptions.setHomotopy

      retval = [];

      javaValue = javaMethod('getHomotopy', this.javaObj);

      if isa(javaValue, 'java.lang.String')
        retval = handleJavaString(javaValue);
      end
    end

    function retval =setHomotopy(this, homotopy)
      % SETHOMOTOPY  Set the 'homotopy' option
      % See also SpectreOptions.getHomotopy

      retval = [];

      if ischar(homotopy)
        
        javaValue = javaMethod('setHomotopy', ...
          this.javaObj, javaObject('java.lang.String', homotopy));

      elseif isempty(reltol)

        javaValue = javaMethod('setHomotopy', this.javaObj, []);
        
      else
        error('Provided ''homotopy'' is not a character array');
      end

      if isa(javaValue, 'java.lang.String')
        retval = handleJavaString(javaValue);
      end
    end

    function retval = getErrpreset(this)
      % GETERRPRESET  Get the 'errpreset' option
      %   The errpreset option is either:
      %    - liberal
      %    - conservative
      %    - moderate        
      % See also SpectreOptions.setErrpreset

      retval = [];

      javaValue = javaMethod('getErrpreset', this.javaObj);

      if isa(javaValue, 'java.lang.String')
        retval = handleJavaString(javaValue);
      end
    end

    function retval = setErrpreset(this, errpreset)
      % SETHOMOTOPY  Set the 'errpreset' option
      % See also SpectreOptions.getErrpreset

      retval = [];

      if ischar(errpreset)
        
        javaValue = javaMethod('setHomotopy', ...
          this.javaObj, javaObject('java.lang.String', errpreset));

      elseif isempty(errpreset)

        javaValue = javaMethod('setHomotopy',this.javaObj, []);

      else
        error('Provided ''errpreset'' is not a character array');
      end

      if isa(javaValue, 'java.lang.String')
        retval = handleJavaString(javaValue);
      end
    end

    function retval = getSkipDC(this)
      % GETSKIPDC  Get the 'skipdc' option
      %   The skipdc option is either:
      %    - yes
      %    - no
      %    - useprevic   
      %    - waveless
      %    - rampup
      %    - autodc    
      %    - sigrampup
      %    - dcrampup 
      % See also SpectreOptions.setSkipDC

      retval = [];

      javaValue = javaMethod('getSkipDC', this.javaObj);

      if isa(javaValue, 'java.lang.String')
        retval = handleJavaString(javaValue);
      end
    end

    function retval = setSkipDC(this, skipdc)
      % SETSKIPDC  Set the 'skipdc' option
      % See also SpectreOptions.getSkipDC

      retval = [];

      if ischar(skipdc)
        
        javaValue = javaMethod('setSkipDC', ...
          this.javaObj, javaObject('java.lang.String', skipdc));

      elseif isempty(skipdc)

        javaValue = javaMethod('setHomotopy', this.javaObj, []);

      else
        error('Provided ''skipdc'' is not a character array');
      end

      if isa(javaValue, 'java.lang.String')
        retval = handleJavaString(javaValue);
      end
    end

    function retval = getRelref(this)
      % GETRELREF  Get the 'relref' option
      %   The relref option is either:
      %    - pointlocal
      %    - allocal
      %    - sigglobal   
      %    - allocal
      % See also SpectreOptions.setRelref

      retval = [];

      javaValue = javaMethod('getRelref', this.javaObj);

      if isa(javaValue, 'java.lang.String')
        retval = handleJavaString(javaValue);
      end
    end

    function retval = setRelref(this, relref)
      % SETRELREF  Set the 'relref' option
      % See also SpectreOptions.getRelref

      retval = [];

      if ischar(relref)
        
        javaValue = javaMethod('setRelref', ...
          this.javaObj, javaObject('java.lang.String', relref));

      elseif isempty(relref)

        javaValue = javaMethod('setRelref', this.javaObj, []);

      else
        error('Provided ''relref'' is not a character array');
      end

      if isa(javaValue, 'java.lang.String')
        retval = handleJavaString(javaValue);
      end
    end

    function retval = getMethod(this)
      % GETMETHOD  Get the 'method' option
      %   The method option is either:
      %    - euler
      %    - trap
      %    - traponly   
      %    - gear2
      %    - gear2only
      %    - trap2gear    
      %    - trapeuler
      %    - []
      % See also SpectreOptions.setMethod

      retval = [];

      javaValue = javaMethod('getMethod', this.javaObj);

      if isa(javaValue, 'java.lang.String')
        retval = handleJavaString(javaValue);
      end
    end

    function retval = setMethod(this, method)
      % SETMETHOD  Set the 'method' option
      % See also SpectreOptions.getMethod

      retval = [];

      if ischar(method)
        
        javaValue = javaMethod('setMethod', ...
          this.javaObj, javaObject('java.lang.String', method));

      elseif isempty(method)

        javaValue = javaMethod('setRelref', this.javaObj, []);

      else
        error('Provided ''method'' is not a character array');
      end

      if isa(javaValue, 'java.lang.String')
        retval = handleJavaString(javaValue);
      end
    end

    function retval = getGmin(this)
      % GETGMIN  Get the 'gmin' option
      % See also SpectreOptions.setGmin

      retval = [];

      javaValue = javaMethod('getGmin', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      end
    end

    function retval = setGmin(this, gmin)
      % SETGMIN  Set the 'gmin' option
      % See also SpectreOptions.getGmin

      retval = [];

      if isreal(gmin) && numel(gmin)==1
        
        javaValue = javaMethod('setGmin', ...
          this.javaObj, javaObject('java.math.BigDecimal', gmin));

        if isa(javaValue, 'java.math.BigDecimal')
          retval = javaMethod('doubleValue', javaValue);
        end
      else
        error('Provided ''gmin'' is not a real numeric');
      end
    end

    function retval = getRforce(this)
      % GETRFORCE  Get the 'rforce' option
      % See also SpectreOptions.setRforce

      retval = [];

      javaValue = javaMethod('getRforce', this.javaObj);

      if isa(javaValue, 'java.math.BigDecimal')
        retval = javaMethod('doubleValue', javaValue);
      end
    end

    function retval = setRforce(this, rforce)
      % SETRFORCE  Set the 'rforce' option
      % See also SpectreOptions.getRforce

      retval = [];

      if isreal(rforce) && numel(rforce)==1
        
        javaValue = javaMethod('setRforce', ...
          this.javaObj, javaObject('java.math.BigDecimal', rforce));

        if isa(javaValue, 'java.math.BigDecimal')
          retval = javaMethod('doubleValue', javaValue);
        end
      else
        error('Provided ''rforce'' is not a real numeric');
      end
    end

    function retval = getInitialCondition(this)
      % GETINITIALCONDICTION Get the 'ic' option
      %   The ic option is either:
      %    - dc
      %    - node
      %    - dev   
      %    - all
      %    - []
      % See also SpectreOptions.setInitialCondition

      retval = [];

      javaValue = javaMethod('getInitialCondition', this.javaObj);

      if isa(javaValue, 'java.lang.String')
        retval = handleJavaString(javaValue);
      end
    end


    function retval = setInitialCondition(this, ic)
      % SETINITIALCONDICTION  Set the 'ic' option
      % See also SpectreOptions.getInitialCondition

      retval = [];

      if ischar(ic)
        
        javaValue = javaMethod('setInitialCondition', ...
          this.javaObj, javaObject('java.lang.String', ic));

      elseif isempty(ic)

        javaValue = javaMethod('setInitialCondition', this.javaObj, []);

      else
        error('Provided ''ic'' is not a character array');
      end

      if isa(javaValue, 'java.lang.String')
        retval = handleJavaString(javaValue);
      end
    end
  end

  methods (Static)
    function obj = init(javaObj)
      %INIT Do not call this function by yourself.
      if  isjava(javaObj) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.simulation.test.SpectreOptions', javaObj)
        obj = Test(javaObj);
      else
        obj=[];
      end
    end
  end
end