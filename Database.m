classdef Database < ExpertDesignPlanWrapper
  % DATABASE This class is the main entry point for an Expert Design Plan.
  %  In a databse you can:
  %   - access existing views with GETVIEW
  %   - 
  %
  %  Utilize the static methods INIT or INITFROMJSON to create a new database.
  %

  % Copyright 2023 Reutlingen University, Electronics & Drives
  
  methods (Access = private)
    function this = Database(javaObj)
      this.javaObj = javaObj;
    end
  end

  methods (Access = public)

    function retval = getWorkingDirectory(this)
      % GETWORKINGDIRECTORY Get the working directory
      %  db.GETWORKINGDIRECTORY() returns the EDP working directory

      retval = handleJavaString(...
        javaMethod('getWorkingDirectory', this.javaObj));
    end

    function retval = getEvalLib(this)
      % GETEVALLIB Get the evaluation library
      %  db.GETEVALLIB() returns the evaluation library

      retval = handleJavaString(...
        javaMethod('getEvalLib', this.javaObj));
    end

    function retval = getSimDir(this)
      % GETSIMDIR Get the simulation directory
      %  db.GETSIMDIR() returns the simulation directory

      retval = handleJavaString(...
        javaMethod('getSimDir', this.javaObj));
    end

    function retval = getLibNames(this)
      % GETLIBNAMES Get all library names
      %  db.GETLIBNAMES() returns a cell array of character array of all
      %  library names.
      %
      %  Example: 
      %  >> libNames = db.getLibNames()
      %
      %  libNames =
      %
      %      {'edp_dsgn', 'edp_tb'}

      javaLibs = javaMethod('getLibNames', this.javaObj);

      retval = cell(numel(javaLibs), 1);

      for i=1:numel(retval)
        retval{i} = handleJavaString(javaLibs(i));
      end
    end

    function retval = getCellNames(this, libName)
      % GETCELLNAMES Get all cell names in library
      %  db.GETCELLNAMES(libName) returns a cell array of character 
      %  array of all cell names in a library.
      %
      %  Example: 
      %  >> cellNames = db.getCellNames('edp_dsgn')
      %
      %  cellNames =
      %
      %      {'op1', 'op2'}

      if ischar(libName)

        jLibName = javaObject('java.lang.String', libName);
        javaCellNames = javaMethod('getCellNames', this.javaObj, jLibName);

        retval = cell(numel(javaCellNames), 1);

        for i=1:numel(retval)
          retval{i} = handleJavaString(javaCellNames(i));
        end
      else
        error('Provided parameter ''libName'' is not a character array');
      end
    end

    function retval = getCell(this, libName , cellName)
      % GETCELL  Get the reference to cell in the database.
      %  cell = db.GETCELL(libName, cellName) returns a reference to a cell
      %  with the provided library and cell name as character arrays.
      %  When a cell with the provided lib and cell is available, a 
      %  corresponding object is returned, [] otherwise.
      %
      %  Example: 
      %   cell = db.getCell('mylib', 'mycell');
      %
      % See also DatabaseCell.

      if ischar(libName) && ischar(cellName) 
        javaCell = javaMethod('getCell', this.javaObj, ...
            javaObject('java.lang.String',libName),...
            javaObject('java.lang.String',cellName));
        retval = DatabaseCell.init(javaCell);
      else
        error('Provided parameters ''libName'' and ''cellName'' are not character arrays');
      end
    end


    function tf = isCell(this, libName , cellName)
      % ISCELL  Identify if a cell defined in the database
      %  tf = db.ISCELL(libName, cellName) returns TRUE when a cell
      %  with the provided library and cell name is available, FALSE otherwise.
      %
      %  Example: 
      %   tf = db.isCell('mylib', 'mycell');
      %

      if ischar(libName) && ischar(cellName) 
        tf = javaMethod('isCell', this.javaObj,...
          javaObject('java.lang.String',libName),...
          javaObject('java.lang.String',cellName));
      else
        error(['Provided parameters ''libName'' and ''cellName'' ...' ...
          ' are not character arrays']);
      end
    end

    function tf = isPrimitive(this, libName , cellName)
      % ISPRIMITIVE  Identify if a primiitve defined in the database
      %  tf = db.ISPRIMITIVE(libName, cellName) returns TRUE when a
      %  primitive with the provided library and cell name is available, 
      %  FALSE otherwise.
      %
      %  Example: 
      %   tf = db.isPrimitive('mylib', 'mycell');
      %

      if ischar(libName) && ischar(cellName) 
        tf = javaMethod('isPrimitive', this.javaObj, ...
              javaObject('java.lang.String',libName),...
              javaObject('java.lang.String',cellName));
      else
        error(['Provided parameters ''libName'' and ''cellName'' ...' ...
          ' are not character arrays']);
      end
    end

    function retval = getView(this, libName, cellName, viewName)
      % GETVIEW  Get the reference to a view in the database.
      %  cell = db.GETVIEW(libName, cellName, viewName) returns a reference to 
      %  a view with the provided library, cell and view name as character
      %  arrays. When a view with the provided libName, cellName and viewName 
      %  is available, a corresponding object is returned, [] otherwise.
      %  
      %   Example: 
      %    view = db.getView('mylib', 'mycell', 'schematic');
      %   
      % See also DatabaseView.

      if ischar(libName) && ischar(cellName) && ischar(viewName) 
        javaView = javaMethod('getView', this.javaObj, libName, cellName, viewName);
        retval = DatabaseSchematic.init(javaView);
      else
        error('Provided parameters ''libName'', ''cellName'' and ''viewName'' are not character arrays');
      end
    end

    function tf = isView(this, libName , cellName, viewName)
      % ISVIEW Identify if a view is defined in the database
      %  tf = db.ISVIEW(libName, cellName, viewName) returns TRUE when a view
      %  with the provided library, cell and view name is available,
      %  FALSE otherwise.
      %
      %  Example: 
      %   tf = db.isView('mylib', 'mycell', 'schematic');
      %


      if ischar(libName) && ischar(cellName) && ischar(viewName) 
        tf = javaMethod('isView', ...
                         this.javaObj, ...
                         javaObject('java.lang.String', libName), ...
                         javaObject('java.lang.String', cellName), ...
                         javaObject('java.lang.String', viewName));
      else
        error('Provided parameters ''libName'',  ''cellName'' or ''viewName'' are not character arrays');
      end
    end

    function retval = quit(this)
      % QUIT Quit the database
      %  db.QUIT() Closes the database and shuts down all sessions.
      % 
      %  Example: 
      %  >> db.quit()
      %

      retval = javaMethod('quit', this.javaObj);

      vars = who();

      for i = 1:numel(vars)

        var=[];

        eval(['var = ',vars{i},';']);

        if isa(var, 'ExpertDesignPlanWrapper')
          clear(vars{i});
        end
      end
    end

    function obj = createSchematicGenerator(this, varargin)
      % CREATESCHEMATICGENERATOR  Create a new schematic generator
      %  db.CREATESCHEMATICGENERATOR(libName, cellName, viewName) returns a 
      %  handle to a new generator when the values are valid, [] otherwise.
      %
      %  Example: 
      %    gen = db.createSchematicGenerator('mylib', 'mycell', 'schematic');
      %   
      % See also SchematicGenerator.   

      if numel(varargin)>=3 && ischar(varargin{1}) && ischar(varargin{2}) ...
          && ischar(varargin{3})

        if numel(varargin) > 3 && islogical(varargin{4})
          schGenJavaObj = javaMethod('createSchematicGenerator', this.javaObj, ...
                               varargin{1}, varargin{2}, varargin{3}, varargin{4});
        else
          schGenJavaObj = javaMethod('createSchematicGenerator', this.javaObj, ...
                               varargin{1}, varargin{2}, varargin{3}, false);
        end

        obj = SchematicGenerator.init(schGenJavaObj);
      else
        error('Provided parameters libName/cellName/viewName are not character arrays');
      end
    end

    function obj = createRectSymbolGenerator(this, libName, cellName, viewName, width, height)
      % CREATERECTSYMBOLGENERATOR  Create a new symbol generator 
      %  db.CREATERECTSYMBOLGENERATOR(libName, cellName, viewName, width, height) 
      %  returns a handle to a new generator when the values are valid,
      %  [] otherwise.
      %  The parameters width and length define the inner box of the symbol.
      %
      %  Example: 
      %    gen = db.createRectSymbolGenerator('mylib', 'mycell', 'symbol', 1.0, 2.0);
      %   
      % See also RectangularSymbolGenerator.   

      if ischar(libName) && ischar(cellName) && ischar(viewName) 

        if isreal(width) && isreal(height) && numel(width)==1 && numel(height)==1

          schGenJavaObj = javaMethod(                                   ...
                      'createRectangularSymbolGenerator'              , ...
                      this.javaObj                                    , ...
                      javaObject('java.lang.String'    , libName)     , ...
                      javaObject('java.lang.String'    , cellName)    , ...
                      javaObject('java.lang.String'    , viewName)    , ...
                      javaObject('java.math.BigDecimal', width)       , ...
                      javaObject('java.math.BigDecimal', height));   

          obj = RectangularSymbolGenerator.init(schGenJavaObj);             
        else
          error('Provided parameters width or length are not real');
        end
      else
        error('Provided parameters libName/cellName/viewName are not character arrays');
      end
    end

    function nl = createNetlist(this)
      % CREATENETLIST Create a new, empty netlist
      %  db.CREATENETLIST() returns a handle to a new netlist.
      %
      %  Examples: 
      %    nl = db.createNetlist();
      %   
      % See also DatabaseNetlist.   

      javaNetlist = javaMethod('createNetlist', this.javaObj);

      nl = DatabaseNetlist.init(javaNetlist);
    end

    function tf = gui_show(this)
      % GUI_SHOW Show the GUI of the simulator
      %  db.GUI_SHOW () opens the GUI of the EDP assistant

      tf = handleJavaString(...
        javaMethod('getSimDir', this.javaObj));
    end

    function eng = lut(this, libName, cellName)
      % LUT Get the LUT of a device
      %  eng = instance.LUT(<libname>, <cellname>) returns the
      %
      % See also ExtractionEngine.

      if ischar(libName) && ischar(cellName)
        javaEngine = javaMethod('getExtractionEngine',...
           this.javaObj, ...
           javaObject('java.lang.String', libName), ...
           javaObject('java.lang.String', cellName));

        eng = ExtractionEngine.init(javaEngine);
      else
        error(['Provided parameters ''libName'' and/or ''cellName''' ...
          'are not character arrays']);
      end
    end

    function obj = createTest(this, varargin)
      % CREATETEST  Create a new Test
      %  db.CREATETEST(varargin) returns a handle
      %  to a new test when the values are valid, [] otherwise.
      %  There are two different ways to create a test. The first method is
      %  to create a test for a given schematic. There you must
      %  provide libName/cellName/viewName as a reference:
      %
      %    test = db.createTest('libName','mylib',...
      %                         'cellName', 'mytb',...
      %                         'viewName', 'schematic');
      % 
      %  Another option is to provide a structrual representation by
      %  reference, so for instance a schematic
      %  
      %   struct = db.getView('mylib', 'mytb', 'schematic');
      %   test = db.createTest('design', struct);
      % 
      %  or a netlist
      %
      %   struct = db.createNestlist();
      %   ;
      %   ; add various elements to the netlist
      %   ;
      %   test = db.createTest('design', struct);
      %   
      % See also Test, DatabaseStructural.   
      
      p = inputParser;

      simulators = {'spectre'};

      addParameter(p,'simulator', simulators{1} ,...
                     @(x) any(validatestring(x, simulators)));

      addParameter(p,'libName' , '' ,@ischar);
      addParameter(p,'cellName', '' ,@ischar);
      addParameter(p,'viewName', '' ,@ischar);

      addParameter(p,'design',[]);

      parse(p, varargin{:});

      if isa(p.Results.design, 'DatabaseStructural')

        javaTest = javaMethod('createTest', this.javaObj, ...
                               p.Results.simulator, ...
                               p.Results.design.javaObj);

        obj = Test.init(javaTest);
      else
        javaTest = javaMethod('createTest', this.javaObj, ...
                               p.Results.simulator, ...
                               p.Results.libName, ...
                               p.Results.cellName, ...
                               p.Results.viewName);
        obj = Test.init(javaTest);
      end
    end

    function container = createTestContainer(this)
      % CREATETESTCONTAINER  Create a new TestContainer
      %  db.CREATETESTCONTAINER() returns a handle
      %  to a new container
      %
      %  Examples: 
      %    container = db.CREATETESTCONTAINER();
      %   
      % See also TestContainer.
      javaContainerTest = javaMethod('createTestContainer', this.javaObj);
      container = TestContainer.init(javaContainerTest);
    end
  end
 
  methods (Static)
    function obj = initdb(jDatabase)
      if isjava(jDatabase) && ...
         javaMethod('isInstanceOf',...
            'edlab.eda.edp.core.database.Database', jDatabase)

        obj = Database(jDatabase);
      else
        obj=[];
      end
    end

    function obj = init(varargin)
      % INIT  Initialize a new database (main entry point for an EDP).
      %  Database.INIT(varargin) returns a handle to a DATABSE when 
      %  all parameters are valid, [] otherwise.
      % 
      %  Keyword Parameters: 
      %   - workDir     : Path to working directory (character array).
      %                    When session is established, the corresponding 
      %                    'cds.lib' and '.cdsinit' must be located in this
      %                    direcory. When this parameter is not provided, pwd
      %                    is used.
      %   - techFiles   : Paths to EDP technology files 
      %                   (cell array of character arrays).   
      %   - workLibs    : Names of Virtuoso libraries that are accessible in 
      %                   the EDP (cell array of character arrays).
      %   - evalLib     : Name of evaluation libaray in Virtuoso
      %                   (character array). When this parameter is not 
      %                   provided, 'evalLib' is sued.
      %   - simDir      : Path to simulation directory (character array).
      %                   The netlists and simulation results are stored 
      %                   in this directory. When this parameter is not 
      %                   provided, '/tmp' is used.      
      %   - interactive : Establish an interactive session (bool).
      %                   When this parameter is not provided, false is used.
      %   - background  : Establish a background session (bool).
      %                   When this parameter is not provided, false is used.
      %
      %   Example: 
      %     db = Database.init(   ...
      %           'workDir'     , '~/eda-workspace', ...
      %           'techFiles'   , {'~/tech1.xml', '/opt/tech2.xml'}, ...
      %           'workLibs'    , {'edp_dsgn', 'edp_tb'}, ...
      %           'evalLib'     , 'evalLib', ...      
      %           'simDir'      , '/tmp', ...         
      %           'interactive' ,  false, ...    
      %           'background'  ,  true);

      p = inputParser();

      addParameter(p,'workDir', pwd(), @(x) ischar(x) && isfolder(x));
      addParameter(p,'techFiles', {},  @iscellstr);
      addParameter(p,'workLibs', {},  @iscellstr);
      addParameter(p,'predict_db', {},  @iscellstr);
      addParameter(p,'evalLib', 'evalLib' , @ischar);     
      addParameter(p,'simDir', '/tmp' , @ischar);       
      addParameter(p,'interactive', false, @islogical);
      addParameter(p,'background', false, @islogical);

      parse(p, varargin{:});

      jWorkDir = javaObject('java.lang.String', p.Results.workDir);

      jTechFiles = javaArray('java.lang.String', numel(p.Results.techFiles));

      for i = 1:numel(p.Results.techFiles)
        jTechFiles(i) = javaObject('java.lang.String', p.Results.techFiles{i});
      end

      jWorkLibs = javaArray('java.lang.String', numel(p.Results.workLibs));

      for i = 1:numel(p.Results.workLibs)
        jWorkLibs(i) = javaObject('java.lang.String', p.Results.workLibs{i});
      end

      jPredict = javaArray('java.lang.String', numel(p.Results.predict_db));

      for i = 1:numel(p.Results.predict_db)
        jPredict(i) = javaObject('java.lang.String', p.Results.predict_db{i});
      end

      jEvalLib = javaObject('java.lang.String', p.Results.evalLib);

      jSimDir = javaObject('java.lang.String', p.Results.simDir);

      jInteractive = p.Results.interactive;
      jBackground = p.Results.background;

      jDatabase = javaMethod('init',...
                             'edlab.eda.edp.core.database.Database', ...
                             jWorkDir, ...
                             jTechFiles, ...
                             jWorkLibs, ...
                             jPredict, ...
                             jEvalLib, ...
                             jSimDir, ...
                             10, ...
                             jInteractive, ...
                             jBackground);

      if isjava(jDatabase) && ...
         javaMethod('isInstanceOf',...
            'edlab.eda.edp.core.database.Database', jDatabase)

        obj = Database(jDatabase);
      else
        obj=[];
      end
    end

    function obj = initFromJSON(varargin)
      % INITFROMJSON  Initialize a Database from a JSON file
      %  db = Database.INITFROMJSON(varargin) initializes a database from
      %  a JSON file
      %  When no parameter is provided to the method, the default
      %  file name '.edpinit.json' is used in the current working directory.
      %  The direct path can be provided to the function as well.
      %  
      %  Example
      %  >> db1 = Database.initFromJSON()
      %  >> db1 = Database.initFromJSON('myinit.json')
      %

      if nargin==0
        obj  = Database.initdb(...
          javaMethod('init', 'edlab.eda.edp.core.database.Database'));
      elseif numel(varargin{:})==1 && ischar(varargin{1})
        obj  =  Database.initdb(javaMethod('init', ...
                          'edlab.eda.edp.core.database.Database',...
                          javaObject('java.lang.String',varargin{1})));
      else
        error('Invalid initialization of Database');
      end
    end

    function retval = quitAll()
      % QUITALL  Quits all databases that are created in the current JVM
      %  This function is called automatically when Matlab/Octave is shutdown.
      retval = javaMethod('quitAll', 'edlab.eda.edp.core.database.Database');
    end

    function tf = isRunningDatabaseAvailable()
      % ISRUNNINGDATABASEAVAILABLE Identify if a database is available
      tf = javaMethod('isRunningDatabaseAvailable', ...
       'edlab.eda.edp.core.database.Database');
    end

    function db = getRunningDatabase()
      % GETRUNNINGDATABASE Get a running database
        javaDatabase = javaMethod('getRunningDatabase', ...
         'edlab.eda.edp.core.database.Database');
        db = Database.init(javaDatabase);
    end
  end
end