classdef ACFrequencySweepAnalysis < Analysis
  % ACFREQUENCYSWEEPANALYSIS Reference of an AC frequency sweep analysis

  % Copyright 2023 Reutlingen University, Electronics & Drives

  methods (Access = private)
    function this = ACFrequencySweepAnalysis(javaObj)
      this.javaObj = javaObj;
    end
  end

  methods (Access = public)

    function values = getValues(this)
      % GETVALUES Get the frequencies that are used for simulation
      %  analysis.GETVALUES() returns a vector of all frequencies that 
      %  are used for simulation
      %
      % Example:
      %  values = analysis.getValues()
      %
      %  values =
      %
      %      [1 10 100]

      javaArray = javaMethod('getValues', this.javaObj);

      values = zeros(1, numel(javaArray));

      for i=1:numel(javaArray)
        values(i) = javaMethod('doubleValue', javaArray(i));
      end
    end

    function values = setValues(this, values)
      % SETVALUES Specify the frequencies that are used for simulation
      %  analysis.SETVALUES(freq) returns the new values when valid, 
      %  [] otherwise.
      %  One must provide an array of numbers as parameter.
      %
      % Example:
      %  values = analysis.setValues([1 10 100])
      %
      %      [1 10 100]

      if isreal(values)

        javaValuesArray = javaArray('java.math.BigDecimal',...
          numel(values));

        for i=1:numel(javaValuesArray)
          javaValuesArray(i) = javaObject('java.math.BigDecimal', ...
            values(i));
        end

        javaValuesArray = javaMethod('setValues', this.javaObj,...
          javaValuesArray);

        values = zeros(1, numel(javaValuesArray));
  
        for i=1:numel(javaValuesArray)
          values(i) = javaMethod('doubleValue', javaValuesArray(i));
        end
      else
        error('No array of values is provided');
      end
    end
  end

  methods (Static)
    function obj = init(javaObj)
      %INIT Do not call this function by yourself.
      if  isjava(javaObj) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.simulation.analysis.ACFrequencySweepAnalysis', javaObj)
        obj = ACFrequencySweepAnalysis(javaObj);
      else
        obj=[];
      end
    end
  end
end