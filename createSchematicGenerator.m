function obj = createSchematicGenerator(workingDir, evalLib, libName, cellName, viewName)
  % CREATESCHEMATICGENERATOR Create a new schematic generator
  %  SchematicGenerator.CREATESCHEMATICGENERATOR(workingDir, evalLib, libName, cellName, viewName)
  %  creates and returns a handle to a new schematic generator. 
  %  The parameters (type=character array) are:
  %    workingDir : working directory where the Skill socket is started
  %    evalLib    : library in Virtuoso where symbols can be evaluated
  %    libName    : library name of resulting schematic       
  %    cellName   : cell name of resulting schematic       
  %    viewName   : view name of resulting schematic  
  %      
  % See also SchematicGenerator.
  if ischar(workingDir) && ischar(evalLib) && ischar(libName) ...
     && ischar(cellName) && ischar(viewName)

    javaSchematicGenerator = ...
      javaMethod(...
      'createSchematicGenerator',...
      'edlab.eda.edp.core.direct.DirectCoreAccess' ,...
      javaObject('java.lang.String', workingDir)   ,...
      javaObject('java.lang.String', evalLib)      ,...
      javaObject('java.lang.String', libName)      ,...
      javaObject('java.lang.String', cellName)     ,...
      javaObject('java.lang.String', viewName));

    obj = SchematicGenerator.init(javaSchematicGenerator);
  else
    error('Type of provided parameters is invalid');
  end
end