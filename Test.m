classdef Test < ExpertDesignPlanWrapper
  % TEST Reference of a Test in the Expert Design Plan
  %   A Test is the main interface for analog circuit simulation.
  %   You can create a test in the Database with the method createTest
  %
  % See also Database.  
  
  methods (Access = private)
    function this = Test(javaObj)
      this.javaObj = javaObj;
    end
  end

  methods (Access = public)

    function db = getDatabase(this)
      % GETDATABASE Get the database where the test was created

      javaDatabase = javaMethod('getDatabase', this.javaObj);

      db = Database.initdb(javaDatabase);
    end

    function instanceNames = getInstanceNames(this)
      % GETINSTANCENAMES Get a cell array of all toplevel instance names
      %  instanceNames = test.GETINSTANCENAMES() returns a cell array of all
      %  instance names.

      javaArray = javaMethod('getInstanceNames', this.javaObj);
      instanceNames = cell(1, numel(javaArray));

      for i=1:numel(javaArray)
        instanceNames{i} = handleJavaString(javaArray(i));
      end
    end
  
    function instance = getInstance(this, name)
      % GETINSTANCE Get the reference to an instance in the toplevel testbench.
      %  instance = test.GETINSTANCE(name) returns the reference to
      %  an instance if available, [] otherwise.

      instance = [];

      if ischar(name)

        javaInstance = javaMethod('getInstance', this.javaObj, name);

        if ~isempty(javaInstance)
          instance = Instance.init(javaInstance);
        end

      else
         error('Provided parameter name is not a character array');
      end
    end

    function instances = getInstances(this)
      % GETINSTANCES Get the reference to all toplevel instances in the testbench.
      %  net = test.GETINSTANCES() returns all cell array with references to
      %  all toplevel instances in the testbench.

      javaArray = javaMethod('getInstances', this.javaObj);

      instances = cell(1, numel(javaArray));

      for i=1:numel(javaArray)
        instances{i} = Instance.init(javaArray(i));
      end
    end

    function modelFileNames = getModelFileNames(this)
      % GETMODELFILENAMES Get a cell array of all model file names
      %  modelFileNames = test.GETMODELFILENAMES() returns a cell array of all
      %  model file names.
      %
      % See also ModelFile.

      javaArray = javaMethod('getModelFileNames', this.javaObj);

      modelFileNames = cell(1, numel(javaArray));

      for i=1:numel(javaArray)
        modelFileNames{i} = handleJavaString(javaArray(i));
      end
    end

    function modelFiles = getModelFiles(this)
      % GETMODELFILES Get a cell array of all model files
      %  modelFileNames = test.GETMODELFILENAMES() returns a cell array of all
      %  model files.
      %
      % See also ModelFile.

      javaArray = javaMethod('getModelFiles', this.javaObj);

      modelFiles  = cell(1, numel(javaArray));

      for i=1:numel(javaArray)
        modelFiles{i} = ModelFile.init(javaArray(i));
      end
    end

    function modelFile = getModelFile(this, name)
      % GETMODELFILE Get the model file with a given name
      %  modelFile = test.GETMODELFILE(name) the model file, when a model file
      %  with the given name exists, [] otherwise
      %
      % See also ModelFile.

      if ischar(name)
        modelFile = ModelFile.init(javaMethod('getModelFile', ...
         this.javaObj , javaObject('java.lang.String', name)));
      else
        error('Provided parameter ''name'' is not a character array');
      end
    end

    function netNames = getNetNames(this)
      %  GETNETNAMES Get a cell array of all toplevel nets
      %   netNames = test.GETNETNAMES() returns a cell array of all
      %   toplevel nets.

      javaArray = javaMethod('getNetNames', this.javaObj);
      netNames = cell(1, numel(javaArray));

      for i=1:numel(javaArray)
        netNames{i} = handleJavaString(javaArray(i));
      end
    end

    function net = getNet(this, name)
      % GETNET Get the reference to a net in the toplevel testbench.
      %  net = test.GETNET(name) returns the reference to
      %  a net if available, [] otherwise.
      %
      % See also Net.

      net = [];

      if ischar(name)

        javaNet = javaMethod('getNet', this.javaObj, name);

        if ~isempty(javaNet)
          net = Net.init(javaNet);
        end

      else
         error('Provided parameter name is not a character array');
      end
    end

    function nets = getNets(this)
      % GETNETS Get the reference to all toplevel net in the testbench.
      %  net = test.GETNETS() returns all cell array with references to
      %  all toplevel net in the testbench.
      %
      % See also Net.

      javaArray = javaMethod('getNets', this.javaObj);
      nets = cell(1, numel(javaArray));

      for i=1:numel(javaArray)
        nets{i} = Net.init(javaArray(i));
      end
    end

    function tf = save(this, electrical)
      % SAVE Save an electrical property during simulation
      %  TF = test.SAVE(electrical) returns true when the electrical is saved,
      %  false otherwise.
      %  The parameter electrical can be a Net, Terminal or OperatingPoint.
      %
      %  See also Net, Terminal, OperatingPoint.

      if isa(electrical,'Net') || isa(electrical,'Terminal') ...
                               || isa(electrical,'OperatingPoint')

        tf = javaMethod('save', this.javaObj, electrical.javaObj);

      else
         error('Provided parameter is not an electrical property');
      end
    end

    function tf = dontSave(this, electrical)
      % DONTSAVE Dont save an an electrical during simulation
      %  TF = test.DONTSAVE(electrical) returns always true.
      %
      % See also Net, Terminal, OperatingPoint.

      if isa(electrical,'Net') || isa(electrical,'Terminal') ...
                               || isa(electrical,'OperatingPoint')

        tf = javaMethod('dontSave', this.javaObj, electrical.javaObj);

      else
         error('Provided parameter is not an electrical');
      end
    end

    function tf = setInitalCondition(this, electrical, value)
      %SETINITIALCONDITION Set an initial condition for simulation
      %   TF = test.SETINITIALCONDITION(electrical,value) returns true when 
      %   the initial condition is added, false otherwise.
      %
      % See also Net, Terminal, OperatingPoint.

      if isa(electrical,'Net') || isa(electrical,'Terminal') ...
          || isa(electrical,'OperatingPoint')

        if numel(value)==1 && (isnumeric(value) || isempty(value))

          tf = javaMethod('setInitalCondition', ...
                          this.javaObj, ...
                          electrical.javaObj, ... 
                          javaObject('java.math.BigDecimal', value));

        else
          error('Provided parameter value is not a numeric value');
        end
      else
         error('Provided parameter electrical is not an electrical');
      end
    end

    function tf = setNodeset(this, electrical, value)
      %SETNODESET Set an initial condition for simulation
      %   TF = test.SETNODESET(electrical,value) returns true when 
      %   the nodeset is added, false otherwise.
      %
      % See also Net, Terminal, OperatingPoint.

      if isa(electrical,'Net') || isa(electrical,'Terminal') ...
          || isa(electrical,'OperatingPoint')

        if numel(value)==1 && (isnumeric(value) || isempty(value))

          tf = javaMethod('setNodeset', ...
                          this.javaObj, ...
                          electrical.javaObj, ... 
                          javaObject('java.math.BigDecimal', value));
        else
          error('Provided parameter value is not a numeric value');
        end
      else
         error('Provided parameter electrical is not an electrical');
      end
    end

    function analysis = getAnalysis(this, title)
      % GETANALYSIS Get a analysis
      %  test.GETANALYSIS(title) returns the analyis with a given title.
      %

      if ischar(title)
        javaAnalysis =  javaMethod('getAnalysis', this.javaObj, ...
          javaObject('java.lang.String', title));

        analysis = buildAnalysis(javaAnalysis);
      else
        error('Provided parameter ''name'' is not a character array');
      end
    end

    function tf = isAnalysis(this, analysis)
      % ISANALYSIS Identify if a analysis is a member of the test
      %  test.ISANALYSIS(analysis) returns true when the analysis is 
      %  member of the test, false otherwise
      %

      if ischar(analysis)

        tf =  javaMethod('isAnalysis', this.javaObj, ...
          javaObject('java.lang.String', analysis));

      elseif isa(analysis,'Analysis')
        tf =  javaMethod('isAnalysis', this.javaObj, analysis.javaObj);
      else
        error('Provided parameter ''analysis'' is neither a character array of an analysis');
      end
    end 

    function analyses = getAnalyses(this)
      % GETANALYSIS Get a cell array of all registered analyses
      %  test.GETANALYSES() returns a cell array of all registered analyses
      %

      javaAnalyses =  javaMethod('getAnalyses', this.javaObj);

      analyses = cell(numel(javaAnalyses),1);

      for i=1:numel(javaAnalyses)
        analyses{i} = buildAnalysis(javaAnalyses(i));
      end
    end


    function analysis = addAnalysis(this, type, title)
      % ADDANALYSIS Create a new analysis in the test
      %   analysis = test.ADDANALYSIS(type, title) returns a reference to 
      %   the analysis
      %   The parameter 'title' must be a unique character array to identify
      %   the test.
      %
      %   Valid Analyses are:
      %     dcop    : Operating Point Analysis
      %     dcps    : DC Parameter Sweep Analysis
      %     dcts    : DC Temperature Sweep Analysis
      %     acfs    : AC Frequency Sweep Analysis
      %     acps    : AC Parameter Sweep Analysis
      %     acts    : AC Temperature Sweep Analysis
      %     xfps    : Transfer Function Parameter Sweep
      %     xffs    : Transfer Function Frequency Sweep
      %     xfts    : Transfer Function Temperature Sweep
      %     stbp    : Stability Parameter Sweep
      %     stbf    : Stability Frequency Sweep
      %     stts    : Stability Temperature Sweep
      %     nsps    : Noise Parameter Sweep
      %     nsfs    : Noise Frequency Sweep
      %     nsts    : Noise Temperature Sweep
      %     tran    : Transient
      %     dcmatch : DC Match Analysis
      %     altp    : Alter Parameter Analysis
      %

      if ischar(type) && ischar(title)

        javaObject = javaMethod('addAnalysis', this.javaObj, type, title);

        analysis = buildAnalysis(javaObject);
      else
         error('Provided parameters type and title are not character arrays');
      end
    end

    function sdb = simulate(this)
      % SIMULATE Run a simulation in the test
      %  sdb = test.SIMULATE()
      %  The method returns a simulation databasebase which contains the
      %  simulation results
      %
      % See also SimulationResults.

      javaSimulationResultsArray = javaMethod('simulate', this.javaObj);

      sdb = cell(1,numel(javaSimulationResultsArray));

      for i = 1:numel(javaSimulationResultsArray)
        sdb{i} = SimulationResults.init(javaSimulationResultsArray(i));
      end

      if numel(sdb) == 1
        sdb=sdb{1};
      end
    end

    function options = getOptions(this)
      % GETOPTIONS Get the options for simulation
      %  options = test.GETOPTIONS() 

      options = SpectreOptions.init(javaMethod('getOptions', this.javaObj));
    end

    function options = getStatisticalOptions(this)
      % GETSTATISTICALOPTIONS Get the statistical options for simulation
      %  options = test.GETSTATISTICALOPTIONS() 

      options = StatisticalOptions.init(...
        javaMethod('getStatisticalOptions', this.javaObj));
    end

    function tf = isStatisticalVariationActive(this)
      % ISSTATISTICALVARIATIONAVTIVE Identify if statistical variation is active

      tf = javaMethod('isStatisticalVariationActive', this.javaObj);
    end
  end

  methods (Static)
    function obj = init(javaObj)
      %INIT Do not call this function by yourself.
      if  isjava(javaObj) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.simulation.test.Test', javaObj)
        obj = Test(javaObj);
      else
        obj=[];
      end
    end
  end
end