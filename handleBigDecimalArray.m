function retval = handleBigDecimalArray(array)
  % HANDLEBIGDECIMALARRAY This function transforms a Java String into 
  %                  a MATLAB / Octave character array
  if isOctave()
    retval = javaMethod('toString', javaString);
  else
    retval = transpose(javaMethod('toCharArray',javaString));
  end
end