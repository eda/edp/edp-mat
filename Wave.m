classdef Wave < ExpertDesignPlanWrapper
  % WAVE Reference of a waveform

  % Copyright 2023 Reutlingen University, Electronics & Drives

  methods (Access = private)
    function this = Wave(javaObj)
      this.javaObj = javaObj;
    end
  end

  methods (Access = public)

    function name = getName(this)
      % GETNAME  Get the name of the waveform

      name = handleJavaString(javaMethod('getName', this.javaObj));
    end

    function values = getX(this)
      % GETX  Get the x-values of the waveform

      javaValues = javaMethod('getX', this.javaObj);

      values = zeros(1,numel(javaValues));

      for i=1:numel(javaValues)
        values(i) = javaValues(i); 
      end
    end

    function values = getY(this)
      % GETY  Get the y-values of the waveform

      javaValues = javaMethod('getY', this.javaObj);

      values = zeros(1,numel(javaValues));


      if javaMethod('isInstanceOf','edlab.eda.ardb.RealWaveform', this.javaObj)

        for i=1:numel(javaValues)
          values(i) = javaValues(i);
        end

      else

        for i=1:numel(javaValues)
          values(i) = complex(javaMethod('getReal', javaValues(i)), ....
                              javaMethod('getImaginary', javaValues(i)));
        end
      end
    end

    function wave = uplus(this)
      % UPLUS Unary plus
      %  wave.uplus() changes the sign of the y values of the waveform
      
      javaWave = javaMethod('uplus', this.javaobj);

      wave = Wave.init(javaWave);
    end

    function wave = uminus(this)
      % UMINUS
      
      javaWave = javaMethod('uminus', this.javaobj);

      wave = Wave.init(javaWave);
    end

    function wave = plus(wave1, wave2)
      % PLUS Add waveforms
      %  wave1+wave2 returns the sum of waveforms
      %  At least one parameter (wave1 or wave2) of the sum must be a waveform
      %  The second parameter can be a wave, a real value or a complex value

      if isa(wave1, 'Wave') || isa(wave2, 'Wave')
        
        if isa(wave1, 'Wave')

          if isa(wave2, 'Wave')
            javaWave = javaMethod('add', wave1.javaObj, wave2.javaObj);
          elseif isreal(wave2) && numel(wave2)==1
            javaWave = javaMethod('add', wave1.javaObj, wave2);
          elseif isnumeric(wave2) && numel(wave2)==1
            javaWave = javaMethod('add', wave1.javaObj, ...
              javaObject('org.apache.commons.math3.complex.Complex', ...
            real(wave2), imag(wave2)));
          else
            error('''wave2'' is neither a wave, real number or complex number');
          end

        elseif isa(wave2, 'Wave')

          if isreal(wave1) && numel(wave1)==1

            javaWave = javaMethod('add', ...
              javaMethod('createConstantWave', ...
                wave2.javaObj, wave1), wave2.javaObj);

          elseif isnumeric(wave1) && numel(wave1)==1
            javaWave = javaMethod('add', ...
              javaMethod('createConstantWave', ...
                wave2.javaObj, ...
                javaObject('org.apache.commons.math3.complex.Complex', ...
                  real(wave1), imag(wave1))), wave2.javaObj);
          else
            error('''wave1'' is neither a real number or complex number');
          end
        end
      else
        error('Neither one of the parameters is a wave');
      end

      wave = Wave.init(javaWave);
    end

    function wave = minus(wave1, wave2)
      % MINUS Subtract waveforms
      %  wave1-wave2 returns the difference of waveforms
      %  At least one parameter (wave1 or wave2) of the difference must be a 
      %  waveform.
      %  The second parameter can be a wave, a real value or a complex value

      if isa(wave1, 'Wave') || isa(wave2, 'Wave')
        
        if isa(wave1, 'Wave')

          if isa(wave2, 'Wave')
            javaWave = javaMethod('subtract', wave1.javaObj, wave2.javaObj);
          elseif isreal(wave2) && numel(wave2)==1
            javaWave = javaMethod('subtract', wave1.javaObj, wave2);
          elseif isnumeric(wave2) && numel(wave2)==1
            javaWave = javaMethod('subtract', wave1.javaObj, ...
              javaObject('org.apache.commons.math3.complex.Complex', ...
            real(wave2), imag(wave2)));
          else
            error('''wave2'' is neither a wave, real number or complex number');
          end

        elseif isa(wave2, 'Wave')

          if isreal(wave1) && numel(wave1)==1

            javaWave = javaMethod('subtract', ...
              javaMethod('createConstantWave', ...
                wave2.javaObj, wave1), wave2.javaObj);

          elseif isnumeric(wave1) && numel(wave1)==1
            javaWave = javaMethod('subtract', ...
              javaMethod('createConstantWave', ...
                wave2.javaObj, ...
                javaObject('org.apache.commons.math3.complex.Complex', ...
                  real(wave1), imag(wave1))), wave2.javaObj);
          else
            error('''wave1'' is neither a real number or complex number');
          end
        end
      else
        error('Neither one of the parameters is a wave');
      end

      wave = Wave.init(javaWave);
    end

    function wave = mtimes(wave1, wave2)
      % MTIMES Multiply waveforms
      %  wave1*wave2 returns the product of waveforms
      %  At least one parameter (wave1 or wave2) of the product must be a 
      %  waveform.
      %  The second parameter can be a wave, a real value or a complex value

      if isa(wave1, 'Wave') || isa(wave2, 'Wave')
        
        if isa(wave1, 'Wave')

          if isa(wave2, 'Wave')
            javaWave = javaMethod('multiply', wave1.javaObj, wave2.javaObj);
          elseif isreal(wave2) && numel(wave2)==1
            javaWave = javaMethod('multiply', wave1.javaObj, wave2);
          elseif isnumeric(wave2) && numel(wave2)==1
            javaWave = javaMethod('multiply', wave1.javaObj, ...
              javaObject('org.apache.commons.math3.complex.Complex', ...
            real(wave2), imag(wave2)));
          else
            error('''wave2'' is neither a wave, real number or complex number');
          end

        elseif isa(wave2, 'Wave')

          if isreal(wave1) && numel(wave1)==1

            javaWave = javaMethod('multiply', ...
              javaMethod('createConstantWave', ...
                wave2.javaObj, wave1), wave2.javaObj);

          elseif isnumeric(wave1) && numel(wave1)==1
            javaWave = javaMethod('multiply', ...
              javaMethod('createConstantWave', ...
                wave2.javaObj, ...
                javaObject('org.apache.commons.math3.complex.Complex', ...
                  real(wave1), imag(wave1))), wave2.javaObj);
          else
            error('''wave1'' is neither a real number or complex number');
          end
        end
      else
        error('Neither one of the parameters is a wave');
      end

      wave = Wave.init(javaWave);
    end

    function wave = mrdivide(wave1, wave2)
      % MRDIVIDE Divide waveforms
      %  wave1/wave2 returns the quotient of waveforms
      %  At least one parameter (wave1 or wave2) of the quotient must be a 
      %  waveform.
      %  The second parameter can be a wave, a real value or a complex value

      if isa(wave1, 'Wave') || isa(wave2, 'Wave')
        
        if isa(wave1, 'Wave')

          if isa(wave2, 'Wave')
            javaWave = javaMethod('divide', wave1.javaObj, wave2.javaObj);
          elseif isreal(wave2) && numel(wave2)==1
            javaWave = javaMethod('divide', wave1.javaObj, wave2);
          elseif isnumeric(wave2) && numel(wave2)==1
            javaWave = javaMethod('divide', wave1.javaObj, ...
              javaObject('org.apache.commons.math3.complex.Complex', ...
            real(wave2), imag(wave2)));
          else
            error('''wave2'' is neither a wave, real number or complex number');
          end

        elseif isa(wave2, 'Wave')

          if isreal(wave1) && numel(wave1)==1

            javaWave = javaMethod('divide', ...
              javaMethod('createConstantWave', ...
                wave2.javaObj, wave1), wave2.javaObj);

          elseif isnumeric(wave1) && numel(wave1)==1
            javaWave = javaMethod('divide', ...
              javaMethod('createConstantWave', ...
                wave2.javaObj, ...
                javaObject('org.apache.commons.math3.complex.Complex', ...
                  real(wave1), imag(wave1))), wave2.javaObj);
          else
            error('''wave1'' is neither a real number or complex number');
          end
        end
      else
        error('Neither one of the parameters is a wave');
      end

      wave = Wave.init(javaWave);
    end

    function wave = abs(this)
      % ABS  Get the absolute value of the waveform
      wave = Wave.init(javaMethod('abs', this.javaObj));
    end

    function wave = db10(this)
      % DB10  Get 'db10' of the wave
      %  wave.DB10() returns the waveform where the y-values
      wave = Wave.init(javaMethod('db10', this.javaObj));
    end

    function wave = db20(this)
      % DB20  Get 'db20' of the wave
      wave = Wave.init(javaMethod('db20', this.javaObj));
    end

    function val = getValue(this, pos)
      % GETVALUE  

      if isreal(pos) && numel(pos)==1
        val = ResultsDatabase.handleJavaValue(...
          javaMethod('getValue', this.javaObj, pos));
      else
        error('Real value must provided as position');
      end
    end

    function wave = real(this)
      % REAL  Get the real part of the waveform
      wave = Wave.init(javaMethod('real', this.javaObj));
    end

    function wave = imag(this)
      % IMAG  Get the imaginary part of the waveform
      wave = Wave.init(javaMethod('imag', this.javaObj));
    end

    function wave = phaseDeg(this)
      % IMAG  Get the phase (degree) of the waveform
      wave = Wave.init(javaMethod('phaseDeg', this.javaObj));
    end

    function wave = resample(this, reference)
      % RESAMPLE  Resample the waveform

      if isa(reference,'Wave') && javaMethod('isInstanceOf', ...
        'edlab.eda.ardb.RealWaveform', reference.javaObj)
        
        javaWave = javaMethod('resample', this.javaObj, reference.javaObj);

      elseif isreal(reference)

        javaValuesArray = javaArray('java.math.BigDecimal', numel(reference));

        for i=1:numel(reference)
          javaValuesArray(i) = ...
            javaObject('java.math.BigDecimal', reference(i));
        end

        javaWave = javaMethod('resample', this.javaObj, javaValuesArray);
        
      else
        error('Cannot resample with the provided reference');
      end
      
      wave=Wave.init(javaWave);
    end

    function retval = cross(this, varargin)
      % CROSS  Calculate the intersection of the wave with a constant value
      %  wave.CROSS(value) calculates the intersection of the waveform
      %  with a constant value 'val'.  
      %  wave.CROSS(val,n) calculates the intersection of the waveform
      %  with a constant value 'val' at the n-th occurrence ('n')
      %
      %  Examples: 
      %   wave = wave.cross(0.0)
      %   value = wave.cross(0.0, 3)
      %  

      if numel(varargin)==1 && isnumeric(varargin{1}) && numel(varargin{1})==1
        javaRetval= javaMethod('cross', this.javaObj, varargin{1});
      elseif numel(varargin)==2 && isnumeric(varargin{1}) && ...
        numel(varargin{1})==1 && isnumeric(varargin{2}) && numel(varargin{2})==1
        javaRetval= javaMethod('cross', this.javaObj, varargin{1}, ...
          round(varargin{2}));
      else
        error('Provided parameters are incorrect');
      end

      if javaMethod('isInstanceOf',...
          'edlab.eda.ardb.RealWaveform', javaRetval)
          retval = Wave.init(javaRetval);
      else
        retval = ResultsDatabase.handleJavaValue(javaRetval);
      end
    end


    function wave = clip(this, left, right)
      % CLIP  Clip the waveform
      %  wave.CLIP(<left>,<right>) clips a waveform

      if isreal(left) && numel(left)==1 && isreal(right) && numel(right)==1
        javaWave = javaMethod('clip', this.javaObj, left, right);
        wave = Wave.init(javaWave);
      else
        error('Real values must provided as left and right position');
      end
    end

    function wave = derive(this)
      % DERIVE  Derive the waveform

      if javaMethod('isInstanceOf','edlab.eda.ardb.RealWaveform', ...
        this.javaObj)
        javaWave = javaMethod('derive', this.javaObj);
        wave = Wave.init(javaWave);
      else
        error('Cannot derive a complex waveform');
      end
    end

    function val = xmin(this)
      % XMIN  Get the minal x-value of the waveform
      val = javaMethod('getValue', javaMethod('xmin', this.javaObj));
    end

    function val = xmax(this)
      % XMAX  Get the maximal x-value of the waveform
      val = javaMethod('getValue', javaMethod('xmax', this.javaObj));
    end

    function val = ymin(this)
      % YMIN  Get the minal y-value of the waveform
      val = javaMethod('getValue', javaMethod('ymin', this.javaObj));
    end

    function val = ymax(this)
      % YMAX  Get the maximal y-value of the waveform
      val = javaMethod('getValue', javaMethod('ymax', this.javaObj));
    end

    function f = plot(this)
      % PLOT  Generate a plot of the waveform
      
      f = figure();

      plot(this.getX(),this.getY());
      xlabel(handleJavaString(javaMethod('getUnitX', this.javaObj)));
      ylabel(handleJavaString(javaMethod('getUnitY', this.javaObj)));
      title(this.getName());
    end
  end

  methods (Static)
    function obj = init(javaObj)
      %INIT Do not call this function by yourself.
      if  isjava(javaObj) && ...
             javaMethod('isInstanceOf','edlab.eda.ardb.RealWaveform', ...
                        javaObj) ...
          || javaMethod('isInstanceOf','edlab.eda.ardb.ComplexWaveform', ...
                         javaObj)

        obj = Wave(javaObj);
      else
        obj=[];
      end
    end
  end
end