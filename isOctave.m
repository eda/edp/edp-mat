function tf = isOctave()
  %ISOCTAVE Identifies whether this script is run in GNU Octave
  tf = exist("OCTAVE_VERSION", "builtin") > 0;
end