classdef SimulationResults < ExpertDesignPlanWrapper
  % SIMULATIONRESULTS Reference of a container of simulation results
  
  methods (Access = private)
    function this = SimulationResults(javaObj)
      this.javaObj = javaObj;
    end
  end

  methods (Access = public)

    function resultDatabase = getResults(this, analysis)
      % GETRESULTS  Get the results from an analysis
      %  srdb.GETRESULTS(analysis) returns the simulation results from a
      %  analysis. The parameter 'analysis' must be either a character array 
      %  which corresponds to the title of the simulation or a handle
      %  of an analysis.
      %
      % See also Analysis.

      resultDatabase = [];

      if isa(analysis, 'Analysis')

        javaResultDatabase = javaMethod('getResults', this.javaObj, ...
          analysis.javaObj);
      
      elseif ischar(analysis)
        javaResultDatabase = javaMethod('get', this.javaObj, ...
          javaObject('java.lang.String', analysis));
      else
        error('No valid analysis is provided.')
      end

      if ~isempty(javaResultDatabase)
        resultDatabase = ResultsDatabase.init(javaResultDatabase);
      end
    end

    function retval = get(this, name)
      % GET  Get the results from an expression
      %  srdb.GET(name) returns an evaluated expression
      %  The parameter 'name' must be a character array 
      %  which corresponds to the name of the expression

      retval = [];

      if ischar(name)

        javaValue = javaMethod('getExpression', this.javaObj, ...
          javaObject('java.lang.String', name));

        if isa(javaValue, 'java.math.BigDecimal')
          retval = javaMethod('doubleValue', javaValue);
        end
      else
        error('Provided parameter is no a character array')
      end
    end

    function test = getTest(this)
      % GETEST  Get the test that generated this results database
      %  srdb.GETTEST() returns the test that generated this results database
      %
      % See also Test.

      test = Test.init(javaMethod('getTest', this.javaObj));

    end
  end

  methods (Static)
    function obj = init(javaObj)
      %INIT Do not call this function by yourself.
      if  isjava(javaObj) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.simulation.test.SimulationResults', javaObj)
        obj = SimulationResults(javaObj);
      else
        obj=[];
      end
    end
  end
end