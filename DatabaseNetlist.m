classdef DatabaseNetlist < DatabaseStructural
  % DATABASENETLIST Reference of a netlist
  %  A DATABASENETLIST is a datastructure where new instances can be added 
  %  an removed without the need to specify the position of the devices
  %  explicitly.
  %  A DATABASENETLIST can be creates from an existing DatabaseSchematic using 
  %  the method convert
  %  An empty DATABASENETLIST can be created from the Database using the 
  %  command createNetlist.
  %
  % See also Database, DatabaseSchematic.   

  % Copyright 2023 Reutlingen University, Electronics & Drives
  
  methods (Access = private)
    function this = DatabaseNetlist(javaObj)
      this.javaObj = javaObj;
    end
  end

  methods (Access = public)
    function tf = removeInstance(this, inst)
      % REMOVEINSTANCE Removes an instance from the the netlist

      if isa(inst, 'DatabaseInstance')
        tf = javaMethod('removeInstance', this.javaObj, inst.javaObj);
      elseif ischar(inst)
        tf = javaMethod('removeInstance', this.javaObj, ...
          javaObject('java.lang.String', inst));
      else
        tf = false;
      end
    end
    
    function inst = addInstance(this, name, libName, cellName, conns)
      % ADDINSTANCE Adds an instance to the the netlist

      if ischar(name) && ischar(libName) && ischar(cellName) && ...
          iscellstr(conns)

        javaPins = javaArray('java.lang.String', numel(conns));

        for i = 1:numel(conns)
          javaPins(i) = javaObject('java.lang.String',conns{i});
        end

        javaInstance = javaMethod('addInstance', ...
                                  this.javaObj, ...
                                  javaObject('java.lang.String', name), ...
                                  javaObject('java.lang.String', libName), ...
                                  javaObject('java.lang.String', cellName), ...
                                  javaPins);

        if javaMethod('isInstanceOf',...
            'edlab.eda.edp.core.database.DatabasePrimitiveInstance', ...
            javaInstance)

          inst = DatabaseInstance.init(javaInstance);

        elseif javaMethod('isInstanceOf',...
            'edlab.eda.edp.core.database.DatabaseNormalInstance', ...
            javaInstance)

          inst = DatabaseInstance.init(javaInstance);

        else
          inst = [];
        end

      else
        errors('Incorrect parameters');
        inst = [];
      end
    end
  end

  methods (Static)
    function obj = init(javaObj)
      %INIT Do not call this function by yourself.
      if  isjava(javaObj) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.core.database.DatabaseNetlist', javaObj)
        obj = DatabaseNetlist(javaObj);
      else
        obj=[];
      end
    end
  end
end