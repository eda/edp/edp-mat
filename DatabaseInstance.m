classdef DatabaseInstance < ExpertDesignPlanWrapper
  % DATABASEINSTANCE Reference of a database instance in the Expert Design Plan

  % Copyright 2023 Reutlingen University, Electronics & Drives
  
  methods (Access = private)
    function this = DatabaseInstance(javaObj)
      this.javaObj = javaObj;
    end
  end

  methods (Access = public)

    function name = getMasterLibraryName(this)
      %GETMASTERLIBRARYNAME Get the name of the master library
      name = handleJavaString(javaMethod('getMasterLibraryName', this.javaObj));
    end

    function name = getMasterCellName(this)
      %GETMASTERCELLNAME Get the name of the master cell
      name = handleJavaString(javaMethod('getMasterCellName', this.javaObj));
    end

    function name = getDeviceType(this)
      %GETDEVICETYPE Get the type of the device
      name = handleJavaString(javaMethod('getDeviceType', this.javaObj));
    end

    function name = getInstanceName(this)
      %GETINSTANCENAME Get the instance name of the instance

      name = handleJavaString(javaMethod('getInstanceName', this.javaObj));
    end

    function db = getDatabase(this)
      % GETDATABASE Get the database where the instance was created

      javaDatabase = javaMethod('getDatabase', this.javaObj);

      db = Database.initdb(javaDatabase);
    end

    function nets = getNets(this)
      % GETNETS Get a all nets of the instance that are connected to the pins
      %  inst.GETNETS() returns a cell array of all nets as 
      %  character arrays.
      %
      %  Example: 
      %  >> nets = inst.getNets()
      %
      %  nets =
      %
      %      {'I', 'O', 'VDD', 'VSS'}

      javaNetsArray = javaMethod('getNets', this.javaObj);

      nets = cell(1,numel(javaNetsArray));

      for i=1:numel(javaNetsArray)
        nets{i} = handleJavaString(javaNetsArray(i));
      end
    end

    function parameter = getParameter(this, name)
      %GETPARAMETER Get the reference to a parameter of the instance
      %   parameter = instance.GETPARAMETER(name) returns the
      %   reference to a parameter if available, [] otherwise.

      parameter = [];

      if ischar(name)

        javaParameter = javaMethod('getParameter', this.javaObj, name);

        if ~isempty(javaParameter)
          parameter = DatabaseParameter.init(javaParameter);
        end

      else
         error('Provided parameter name is not a character array');
      end
    end

    function property = getProperty(this, name)
      % GETPROPERTY Get a property of the instance
      %  property = instance.GETPROPERTY(name) returns the
      %  value of a property if available, [] otherwise.

      property = [];

      if ischar(name)

        javaBigDecimal = javaMethod('getProperty', this.javaObj, ...
          javaObject('java.lang.String', name));

        if ~isempty(javaBigDecimal)
          property = javaMethod('doubleValue', javaBigDecimal);
        end

      else
         error('Provided parameter ''name'' is not a character array');
      end
    end

    function property = getTechProperty(this, name)
      % GETTECHPROPERTY Get the technology property of the instance
      %  property = instance.GETTECHPROPERTY(name, key) returns the
      %  value of a property if available, [] otherwise.

      property = [];

      if ischar(name)

        javaBigDecimal = javaMethod('getTechProperty', this.javaObj, ...
          javaObject('java.lang.String', name));

        if ~isempty(javaBigDecimal)
          property = javaMethod('doubleValue', javaBigDecimal);
        end
      else
         error(['Provided parameters ''name'' and/or ''key'' ' ...
          'are not a character arrays']);
      end
    end

    function property = setProperty(this, name, value)
      % SETPROPERTY Set a property of the instance
      %  property = instance.SETPROPERTY(name, value) will set the property 
      %  with the identifier 'name' to the value 'value'
      %   
      %  Example:
      %   property = inst.SETPROPERTY('gmid', 10.0) 
      %

      property = [];

      if ischar(name)

        if isreal(value) && numel(value) ==1

          javaBigDecimal = javaMethod('setProperty', this.javaObj, ...
            javaObject('java.lang.String', name), ...
            javaObject('java.math.BigDecimal', value));

          if ~isempty(javaBigDecimal)
            property = javaMethod('doubleValue', javaBigDecimal);
          end
        else
           error('Provided parameter ''value'' is not a real value');
        end
      else
         error('Provided parameter ''name'' is not a character array');
      end
    end

    function eng = getExtractionEngine(this)
      % GETEXTRACTIONENGINE Get the extraction engine of the device
      %  eng = instance.GETEXTRACTIONENGINE(name) returns the
      %  extraction engine when available, [] otherwise.
      %  The method returns a handle to a database of LUTs.
      %
      % See also ExtractionEngine.

      eng = [];

      javaEngine = javaMethod('getExtractionEngine', this.javaObj);

      if javaMethod('isInstanceOf', ...
        'edlab.eda.predict.extraction.ExtractionEngine', javaEngine)

        javaEnvironment = javaMethod('getEnvironment', javaEngine);

        eng = ExtractionEngine.init(javaEnvironment, javaEngine);
      end
    end

    function tf = hasExtractionEngine(this)
      % HASEXTRACTIONENGINE Identify if an extraction engine is available
      %
      % See also ExtractionEngine.

      tf = javaMethod('hasExtractionEngine', this.javaObj);
    end

    function tf = hasProperty(this, name)
      % HASPROPERTY Identify if a particular property is available

      if ischar(name)
        tf = javaMethod('hasProperty', this.javaObj, ...
         javaObject('java.lang.String', name));
      else
        error('Provided parameter ''name'' is not a character array');
      end
    end

    function eng = lut(this)
      % LUT Get the LUT engine of the device
      %  eng = instance.LUT() returns the
      %  extraction engine when available, [] otherwise.
      %  The method returns a handle to a database of LUTs.
      %
      % See also ExtractionEngine.

      eng = [];

      javaEngine = javaMethod('getExtractionEngine', this.javaObj);

      if javaMethod('isInstanceOf', ...
        'edlab.eda.predict.extraction.ExtractionEngine', javaEngine)

        javaEnvironment = javaMethod('getEnvironment', javaEngine);

        eng = ExtractionEngine.init(javaEnvironment, javaEngine);
      end
    end

    function structural = getStructural(this)
      % GETSTRUCTURAL Get the structural representation (netlist or schematic)
      %  where the instance is defined

      javaView = javaMethod('getView', this.javaObj);

      if isjava(javaView) && ...
        javaMethod('isInstanceOf',...
            'edlab.eda.edp.core.database.DatabaseSchematic', javaView)

        structural = DatabaseSchematic.init(javaView);
      elseif isjava(javaView) && ...
        javaMethod('isInstanceOf',...
            'edlab.eda.edp.core.database.DatabaseNetlist', javaView)
        
        structural = DatabaseNetlist.init(javaView);
      end
    end

    function switchList = getSwitchList(this)
      % GETSWITCHLIST Get the switch list of the instance
      %  inst.GETSWITCHLIST() returns a cell array of switch view names as
      %  character arrays.
      %
      %  Example: 
      %  >> switchList = inst.getSwitchList()
      %
      %  switchList =
      %
      %      {'schematic', 'veriloga'}


      switchListArray = javaMethod('getSwitchList', this.javaObj);
      switchList = cell(1, numel(switchListArray));

      for i=1:numel(switchListArray)
        switchList{i} = handleJavaString(switchListArray(i));
      end
    end

    function retval = setSwitchList(this, switchList)
      % SETSWITCHLIST Set the switch list of the instance
      %  inst.SETSWITCHLIST(switchList) sets the switch list of the instance.
      %  You must provide a cell array consisting of character arrays as
      %  parameter. The method returns the new set switch list.
      %
      %  Example: 
      %  >> switchList = inst.setSwitchList({'schematic', 'veriloga'})
      %
      %  switchList =
      %
      %      {'schematic', 'veriloga'}


      if iscellstr(switchList)

        switchListArray = javaArray('java.lang.String', numel(switchList));

        for i=1:numel(switchListArray)
          switchListArray(i) = javaObject('java.lang.String', switchList{i});
        end

        switchListArray = javaMethod('setSwitchList', ...
          this.javaObj, switchListArray);

        retval = cell(1, numel(switchListArray));

        for i=1:numel(switchListArray)
          retval{i} = handleJavaString(switchListArray(i));
        end

      else
        error('Provided parameter is not a cell array of character arrays');
      end
    end

    function retval = get(this, name)
      % GET  Get the value of a parameter
      %  inst.GET(paramName) returns the value of a parameter
      %
      %  Example: 
      %  >> value = inst.get('width')
      %
      %  value = 5e-6
      %      
      retval = [];

      if ischar(name)
        javaValue = javaMethod('get', this.javaObj,  ...
          javaObject('java.lang.String', name));

        if isa(javaValue, 'java.math.BigDecimal')
          retval = javaMethod('doubleValue', javaValue);
        elseif isa(javaValue, 'java.lang.String')
          retval = handleJavaString(javaValue);
        elseif isempty(javaValue)
          retval = [];
        end
      else
        error('Provided parameter ''name'' is not a character array');
      end
    end

    function retval = set(this, name, value)
      % SET  Set the value of a parameter in the instance
      %   value = instance.SET(name, value) sets the value of the parameter
      %   'name' to the 'value' and returns the new set value
      %
      %  Example: 
      %  >> value = inst.set('width', 5e-6)
      %
      %   w = 5e-6
      %  >> value = inst.set('width', 'notvalid')
      %
      %   w = 5e-6
      
      retval = [];

      javaValue = [];


      if ischar(name)
        if numel(value)==1 && isnumeric(value) && numel(value)==1

          javaValue = javaMethod('set', ...
                                  this.javaObj, ...
                                  javaObject('java.lang.String', name), ...
                                  javaObject('java.math.BigDecimal', value));

        elseif ischar(value)
          javaValue = javaMethod('set', ...
                                 this.javaObj, ...
                                 javaObject('java.lang.String', name), ...
                                 javaObject('java.lang.String', value));
        elseif isempty(value)
          javaValue = javaMethod('set', ...
                                 this.javaObj, ...
                                 javaObject('java.lang.String', name), ...
                                 value);
        end

        if isa(javaValue, 'java.math.BigDecimal')
          retval = javaMethod('doubleValue', javaValue);
        elseif isa(javaValue, 'java.lang.String')
          retval = handleJavaString(javaValue);
        end
      else
        error('Provided parameter ''name'' is not a character array');
      end
    end

    function retval = getMin(this, name)
      % GETMIN  Get the lower bound of a parameter

      retval = [];

      if ischar(name)
        javaValue = javaMethod('getMin', this.javaObj,  ...
          javaObject('java.lang.String', name));

        if isa(javaValue, 'java.math.BigDecimal')
          retval = javaMethod('doubleValue', javaValue);
        elseif isa(javaValue, 'java.lang.String')
          retval = handleJavaString(javaValue);
        elseif isempty(javaValue)
          retval = [];
        end
      else
        error('Provided parameter ''name'' is not a character array');
      end
    end

    function retval = getMax(this, name)
      % GETMAX  Get the upper bound of a parameter

      retval = [];

      if ischar(name)
        javaValue = javaMethod('getMax', this.javaObj,  ...
          javaObject('java.lang.String', name));

        if isa(javaValue, 'java.math.BigDecimal')
          retval = javaMethod('doubleValue', javaValue);
        elseif isa(javaValue, 'java.lang.String')
          retval = handleJavaString(javaValue);
        elseif isempty(javaValue)
          retval = [];
        end
      else
        error('Provided parameter ''name'' is not a character array');
      end
    end

    function retval = getGrid(this, name)
      % GETGRID Get the grid of a parameter

      retval = [];

      if ischar(name)
        javaValue = javaMethod('getMax', this.javaObj,  ...
          javaObject('java.lang.String', name));

        if isa(javaValue, 'java.math.BigDecimal')
          retval = javaMethod('doubleValue', javaValue);
        elseif isa(javaValue, 'java.lang.String')
          retval = handleJavaString(javaValue);
        elseif isempty(javaValue)
          retval = [];
        end
      else
        error('Provided parameter ''name'' is not a character array');
      end
    end

    function tf = pullParameters(this, inst)
      % PULLPARAMETERS Pull all parameters another instance.
      %
      %  Example: 
      %  >> tf = inst1.PULLPARAMETERS(inst2)
      %
      %  tf =
      %
      %      true

      if isa(inst, 'DatabaseInstance')
        tf = javaMethod('pullParameters', this.javaObj, inst.javaObj);
      else
        error('Provided parameter is not an instance.')
      end
    end


    function tf = pushParameters(this, inst)
      % PUSHPARAMETERS Push all parameters to another instance.
      %
      %  Example: 
      %  >> tf = inst2.PUSHPARAMETERS(inst1)
      %
      %  tf =
      %
      %      true

      if isa(inst, 'DatabaseInstance')
        tf = javaMethod('pushParameters', this.javaObj, inst.javaObj);
      else
        error('Provided parameter is not an instance.')
      end
    end

    function retval = show(this)
      % SHOW Show the instance in Virtuoso

      retval = javaMethod('show', this.javaObj);
    end

    function retval = getGroups(this)
      % GETGROUPS Get all matching groups
      %  struct.GETGROUPS() returns a cell array of all matching groups
      %  in the structural
      %
      %  Example: 
      %  >> groups = struct.getGroups()
      %

      javaGroups = javaMethod('getGroupsAsArray', this.javaObj);

      retval = cell(numel(javaGroups), 1);

      for i=1:numel(retval)
        retval{i} = MatchingParametersGroup.init(javaGroups(i));
      end
    end

    function retval = descend(this)
      % DESCEND Descend in hierarchy
      %

      javaStruct = javaMethod('descend', this.javaObj);
      retval=[];

      if isjava(javaObj) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.core.database.DatabaseSchematic', javaStruct)
        retval = DatabaseSchematic.init();
      elseif isjava(javaObj) && ...
          javaMethod('isInstanceOf',...
            'edlab.eda.edp.core.database.DatabaseNetlist', javaStruct)
        retval = DatabaseNetlist.init();
      end
    end
  end

  methods (Static)
    function obj = init(javaObj)
      %INIT Do not call this function by yourself.
      if  isjava(javaObj) && ...
             javaMethod('isInstanceOf',...
              'edlab.eda.edp.core.database.DatabaseNormalInstance', javaObj) ...
          || javaMethod('isInstanceOf',...
              'edlab.eda.edp.core.database.DatabasePrimitiveInstance', javaObj)
        obj = DatabaseInstance(javaObj);
      else
        obj=[];
      end
    end
  end
end