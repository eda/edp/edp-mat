classdef DatabaseStructural < ExpertDesignPlanWrapper
  % DATABASESTRUCTURAL Reference of a database structural view
  %  A structural view is either a schematic (from the remote design
  %  environemnt) or a netlist.
  %
  % See also DatabaseSchematic, Datbasenetlist.  

  % Copyright 2023 Reutlingen University, Electronics & Drives

  methods (Access = public)

    function instance = getInstance(this, name)
      % GETINSTANCE Get a handle to an instance in the structural representation.
      %  schematic.GETVIEWNAME(name) returns a handle to the instance, when
      %  name (character array) is 
      %
      %  Parameter:
      %  
      %  Example: 
      %  >> instance = schematic.getInstance('R0')

      instance = [];

      if ischar(name)

        javaInstance = javaMethod('getInstance', this.javaObj, ...
          javaObject('java.lang.String', name));
  
        if ~isempty(javaInstance)
          instance = DatabaseInstance.init(javaInstance);
        end

      else
        error('No charcter array is provided as parameter');
      end
    end

    function db = getDatabase(this)
      % GETDATABASE Get the database where the structural was created

      javaDatabase = javaMethod('getDatabase', this.javaObj);

      db = Database.initdb(javaDatabase);
    end

    function tf = hasInstance(this, instance)
      % HASINSTANCE Check if an instance is within the structural description
      %  db.ISINSTANCENAME(<name>) returns true when an instance with a 
      %  given name is in the structural description, false otherwise.
      %  db.ISINSTANCENAME(<instance>) returns true when an instance with a 
      %  given handle is in the structural description, false otherwise.
      %
      %  Example: 
      %  >> tf = db.hasInstance('MN0')
      %
      %

      if ischar(instance)
        tf = javaMethod('hasInstance', this.javaObj, ...
         javaObject('java.lang.String',instance));
      elseif isa(instance, 'DatabaseInstance')
        tf = javaMethod('hasInstance', this.javaObj, instance.javaObj);
      else
        tf = false;
      end
    end

    function retval = getInstanceNames(this)
      % GETINSTANCENAMES Get names of all instance names
      %  db.GETINSTANCENAMES() returns a cell array of character arrays of all
      %  instance names.
      %
      %  Example: 
      %  >> instances = db.getInstanceNames()
      %
      %  instances =
      %
      %      {'MN1', 'MN2'}

      javaInstances = javaMethod('getInstanceNames', this.javaObj);

      retval = cell(numel(javaInstances), 1);

      for i=1:numel(retval)
        retval{i} = handleJavaString(javaInstances(i));
      end
    end

    function retval = getInstances(this)
      % GETINSTANCES Get all instances
      %  db.GETINSTANCES() returns a cell array of all instances in the
      %  structural
      %
      %  Example: 
      %  >> instances = db.getInstances()
      %

      javaInstances = javaMethod('getInstances', this.javaObj);

      retval = cell(numel(javaInstances), 1);

      for i=1:numel(retval)
        retval{i} = DatabaseInstance.init(javaInstances(i));
      end
    end

    function retval = getGroups(this)
      % GETGROUPS Get all matching groups
      %  struct.GETGROUPS() returns a cell array of all matching groups
      %  in the structural
      %
      %  Example: 
      %  >> groups = struct.getGroups()
      %

      javaGroups = javaMethod('getGroupsAsArray', this.javaObj);

      retval = cell(numel(javaGroups), 1);

      for i=1:numel(retval)
        retval{i} = MatchingParametersGroup.init(javaGroups(i));
      end
    end

    function netNames = getNetNames(this)
      % GETNETNAMES Get names of all nets
      %  db.GETNETNAMES() returns a cell array of character array of all
      %  net names.
      %
      %  Example: 
      %  >> netNames = db.getNetNames()
      %
      %  netNames =
      %
      %      {'A', 'B'}

      javaNetNames = javaMethod('getNetNames', this.javaObj);

      netNames = cell(numel(javaNetNames), 1);

      for i=1:numel(retval)
        netNames{i} = handleJavaString(javaNetNames(i));
      end
    end

    function pins = getPins(this)
      % GETPINS Get names of all nets
      %  db.GETPINS() returns a cell array of character array of all
      %  pins.
      %
      %  Example: 
      %  >> pins = db.getPins()
      %
      %  pins =
      %
      %      {'A', 'B'}

      javaPins = javaMethod('getPins', this.javaObj);

      pins = cell(numel(javaPins), 1);

      for i=1:numel(javaPins)
        pins{i} = handleJavaString(javaPins(i));
      end
    end

    function tf = pullParameters(this, structural)
      % PULLPARAMETERS Pull all parameters of all instances from another 
      %  structural representation.
      %
      %  Example: 
      %  >> tf = sch1.PULLPARAMETERS(sch2)
      %
      %  tf =
      %
      %      true

      if isa(structural, 'DatabaseStructural')
        tf = javaMethod('pullParameters', this.javaObj, structural.javaObj);
      else
        error('Provided parameter is not a structural representation.')
      end
    end

    function tf = pushParameters(this, structural)
      % PUSHPARAMETERS Pull all parameters of all instances from another 
      %  structural representation.
      %
      %  Example: 
      %  >> tf = sch2.PUSHPARAMETERS(sch1)
      %
      %  tf =
      %
      %      true

      if isa(structural, 'DatabaseStructural')
        tf = javaMethod('pushParameters', this.javaObj, structural.javaObj);
      else
        error('Provided parameter is not a structural representation.')
      end
    end

    function group = createMatchingGroup(this)
      % CREATEMATCHINGGROUP 
      %
      %  Example: 
      %  >> group = sch.createMatchingGroup()
      %

      group = MatchingParametersGroup.init(...
        javaMethod('createMatchingGroup', this.javaObj));
    end

    function nl = convert(this)
      % CONVERT Convert the structural representation to a netlist
      %  str.CONVERT() returns a netlist 
      %
      %  Example: 
      %  >> nl = str.convert()
      %
      % See also DatabaseNetlist.

      javaNetlist = javaMethod('convert', this.javaObj);

      nl = DatabaseNetlist.init(javaNetlist);
    end
  end
end