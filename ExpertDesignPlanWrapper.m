classdef ExpertDesignPlanWrapper
  % EXPERTDESIGNPLANWRAPPER Wrapper
  %  All classes in the Expert Design Toolbox are derived from this class

  % Copyright 2023 Reutlingen University, Electronics & Drives
  
  properties (Access = public)
    javaObj
  end
  
  methods (Access = public)

    function tf = eq(this, obj)
      % EQ Check if an EDP object matches with another EDP object
      if isa(obj, 'ExpertDesignPlanWrapper')
        tf = javaMethod('equals', this.javaObj, obj.javaObj);
      else
        tf  = false;
      end
    end

    function tf = ne(this, obj)
      %NE Check if an EDP object does not match with another EDP object
      tf = ~this.eq(obj);
    end

    function disp(this, verbose)
      % DISP Display the current status of the object
      %  obj.DISP()

      if nargin==2
        disp(handleJavaString(...
          javaMethod('toString', this.javaObj, verbose)));
      else
        disp(handleJavaString(...
          javaMethod('toString', this.javaObj)));
      end
    end
  end
end